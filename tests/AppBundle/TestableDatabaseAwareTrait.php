<?php

namespace tests\AppBundle;

use Symfony\Component\Process\Process;

/**
 * @property \AppKernel $kernel
 */
trait TestableDatabaseAwareTrait
{
    private function dropTestsDatabase()
    {
        $process = new Process(implode(' ', [
            escapeshellarg(self::$kernel->getContainer()->getParameter('console_path')),
            'doctrine:database:drop',
            '--if-exists --force --env=test -vvv',
        ]));
        $process->mustRun();
        $this->assertSame(0, $process->getExitCode());
    }

    private function createTestsDatabase()
    {
        $process = new Process(implode(' ', [
            escapeshellarg(self::$kernel->getContainer()->getParameter('console_path')),
            'doctrine:database:create',
            '--if-not-exists --env=test -vvv',
        ]));
        $process->mustRun();
        $this->assertSame(0, $process->getExitCode());

        $this->updateTestsDatabaseSchema();
    }

    private function updateTestsDatabaseSchema()
    {
        $process = new Process(implode(' ', [
            escapeshellarg(self::$kernel->getContainer()->getParameter('console_path')),
            'doctrine:schema:update',
            '--force --env=test -vvv',
        ]));
        $process->mustRun();
        $this->assertSame(0, $process->getExitCode());
    }

    private function importCountries()
    {
        $process = new Process(implode(' ', [
            escapeshellarg(self::$kernel->getContainer()->getParameter('console_path')),
            'kinopoisk:find-countries',
            '--env=test -vvv',
        ]));
        $process->mustRun();
        $this->assertSame(0, $process->getExitCode());

        $process = new Process(implode(' ', [
            escapeshellarg(self::$kernel->getContainer()->getParameter('console_path')),
            'kinofit:import-countries-kinopoisk-csv',
            '--env=test -vvv',
        ]));
        $process->mustRun();
        $this->assertSame(0, $process->getExitCode());
    }

    private function importGenres()
    {
        $process = new Process(implode(' ', [
            escapeshellarg(self::$kernel->getContainer()->getParameter('console_path')),
            'kinopoisk:find-genres',
            '--env=test -vvv',
        ]));
        $process->mustRun();
        $this->assertSame(0, $process->getExitCode());

        $process = new Process(implode(' ', [
            escapeshellarg(self::$kernel->getContainer()->getParameter('console_path')),
            'kinofit:import-genres-kinopoisk-csv',
            '--env=test -vvv',
        ]));
        $process->mustRun();
        $this->assertSame(0, $process->getExitCode());
    }
}
