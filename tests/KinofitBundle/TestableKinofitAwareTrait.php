<?php

namespace tests\KinofitBundle;

use League\Flysystem\Filesystem;

/**
 * @target ContainerAwareTrait
 */
trait TestableKinofitAwareTrait
{
    protected function cleanTestableMoviesPostersFilesystem(): void
    {
        /** @var Filesystem $postersFilesystem */
        $postersFilesystem = $this->getMoviesPostersFilesystem();
        $list              = $postersFilesystem->listContents();
        foreach ($list as $finfo) {
            if ($finfo['type'] !== 'file') {
                continue;
            }
            $this->assertTrue($postersFilesystem->delete($finfo['path']));
        }
    }
}
