<?php

namespace KinofitBundle\Command;

use AppBundle\ContainerAwareTrait;
use KinofitBundle\Entity\File;
use KinofitBundle\Entity\Movie;
use QA\SoftMocks;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Tester\CommandTester;
use Tests\AppBundle\TestableAwareTrait;
use Tests\AppBundle\TestableDatabaseAwareTrait;
use Tests\KinofitBundle\TestableKinofitAwareTrait;
use Tests\KinopoiskBundle\TestableKinopoiskAwareTrait;

class KinofitImportMoviesPostersCommandTest extends KernelTestCase
{
    use TestableAwareTrait;
    use TestableDatabaseAwareTrait;
    use TestableKinofitAwareTrait;
    use TestableKinopoiskAwareTrait;
    use ContainerAwareTrait;

    public function setUp()
    {
        parent::setUp();
        self::bootKernel();
        SoftMocks::restoreAll();
        $this->cleanTestsKinopoiskDirectories();
    }

    public function tearDown()
    {
        SoftMocks::restoreAll();
        parent::tearDown();
    }

    /**
     * @covers \KinofitImportMoviesPostersCommand::execute()
     */
    public function testExecute()
    {
        $this->dropTestsDatabase();
        $this->createTestsDatabase();
        $this->cleanTestableMoviesPostersFilesystem();

        $optionKinopoiskId = 609618;
        $optionPosterUrl   = 'https://st.kp.yandex.net/images/film_big/'.$optionKinopoiskId.'.jpg';

        /** @var \AppKernel $kernel */
        $kernel            = self::$kernel;
        $em                = $kernel->getContainer()->get('doctrine.orm.entity_manager');
        $fileRepository    = $em->getRepository(File::class);
        $postersFilesystem = $this->getMoviesPostersFilesystem();

        $movie = new Movie();
        $movie->setKinopoiskId($optionKinopoiskId);
        $em->persist($movie);
        $em->flush($movie);

        $application = new Application($kernel);
        $application->add(new KinofitImportMoviesPostersCommand());

        $command       = $application->get(KinofitImportMoviesPostersCommand::NAME);
        $commandTester = new CommandTester($command);
        $commandTester->execute(
            [
                'command'                                                   => $command->getName(),
                '--'.KinofitImportMoviesPostersCommand::OPTION_KINOPOISK_ID => $optionKinopoiskId,
                '--'.KinofitImportMoviesPostersCommand::OPTION_POSTER_URL   => $optionPosterUrl,
                '--env'                                                     => 'test',
            ],
            ['verbosity' => OutputInterface::VERBOSITY_DEBUG]
        );

        $this->assertSame(0, $commandTester->getStatusCode());
        $this->assertTrue($postersFilesystem->has($optionKinopoiskId.'.jpg'));

        /** @var File $poster */
        $poster = $fileRepository->findOneBy([
            'filesystem' => $kernel->getContainer()->getParameter('flysystem.filesystem.movies_posters.adapter'),
            'path'       => null,
            'basename'   => $optionKinopoiskId.'.jpg',
        ]);
        $this->assertNotEmpty($poster);

        $em->refresh($movie);
        $this->assertSame($movie->getPoster()->getId(), $poster->getId());
    }
}
