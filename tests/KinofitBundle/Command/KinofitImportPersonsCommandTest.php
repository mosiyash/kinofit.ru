<?php

namespace KinofitBundle\Command;

use KinofitBundle\Entity\Person;
use QA\SoftMocks;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Tester\CommandTester;
use tests\AppBundle\TestableAwareTrait;
use tests\AppBundle\TestableDatabaseAwareTrait;
use tests\KinopoiskBundle\TestableKinopoiskAwareTrait;

class KinofitImportPersonsCommandTest extends KernelTestCase
{
    use TestableAwareTrait;
    use TestableDatabaseAwareTrait;
    use TestableKinopoiskAwareTrait;

    public function setUp()
    {
        parent::setUp();
        self::bootKernel();
        SoftMocks   ::restoreAll();
        $this->cleanTestsKinopoiskDirectories();
    }

    public function tearDown()
    {
        SoftMocks::restoreAll();
        parent::tearDown();
    }

    /**
     * @covers \KinofitImportMoviesCommand::execute()
     */
    public function testExecute()
    {
        $this->dropTestsDatabase();
        $this->createTestsDatabase();

        /** @var \AppKernel $kernel */
        $kernel = self::$kernel;
        $em     = $kernel->getContainer()->get('doctrine.orm.entity_manager');

        $optionKinopoiskId  = 1096253;
        $optionJsonFilepath = $kernel->getContainer()->getParameter('kernel.cache_dir').'/kinopoisk/parse-persons/'.$optionKinopoiskId.'.json';

        if (!file_exists(dirname($optionJsonFilepath)) && !mkdir(dirname($optionJsonFilepath), 0777, true)) {
            throw new \RuntimeException(sprintf(
                'Couldn\'t make directory %s',
                dirname($optionJsonFilepath)
            ));
        }

        if (!file_put_contents($optionJsonFilepath, '[{"kinopoisk_id": 1096253,"firstname_en": "Jason","firstname_ru": "Джейсон","lastname_en": "Sudeikis","lastname_ru": "Судейкис","patronymic_en": null,"patronymic_ru": null}]')) {
            throw new \RuntimeException(sprintf(
                'Couldn\'t write contents to the file %s',
                $optionJsonFilepath
            ));
        }

        $application = new Application(self::$kernel);
        $application->add(new KinofitImportPersonsCommand());

        $command       = $application->get(KinofitImportPersonsCommand::NAME);
        $commandTester = new CommandTester($command);
        $commandTester->execute(
            [
                'command'                                              => $command->getName(),
                '--'.KinofitImportPersonsCommand::OPTION_KINOPOISK_ID  => $optionKinopoiskId,
                '--'.KinofitImportPersonsCommand::OPTION_JSON_FILEPATH => $optionJsonFilepath,
                '--env'                                                => 'test',
            ],
            ['verbosity' => OutputInterface::VERBOSITY_DEBUG]
        );

        $this->assertSame(0, $commandTester->getStatusCode());
        $this->assertFileNotExists($optionJsonFilepath);

        /** @var Person $person */
        $person = $em->getRepository(Person::class)->findOneByKinopoiskId($optionKinopoiskId);

        $this->assertSame(Person::class, get_class($person));
        $this->assertSame('Jason', $person->getTranslations()->get('en')->getFirstname());
        $this->assertSame('Sudeikis', $person->getTranslations()->get('en')->getLastname());
        $this->assertSame(null, $person->getTranslations()->get('en')->getPatronymic());
        $this->assertSame('Джейсон', $person->getTranslations()->get('ru')->getFirstname());
        $this->assertSame('Судейкис', $person->getTranslations()->get('ru')->getLastname());
        $this->assertSame(null, $person->getTranslations()->get('ru')->getPatronymic());
    }
}
