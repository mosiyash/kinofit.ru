<?php

namespace KinofitBundle\Command;

use KinofitBundle\Entity\Movie;
use QA\SoftMocks;
use QueueBundle\Entity\CommandQueue;
use QueueBundle\Repository\CommandQueueRepository;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Tester\CommandTester;
use Tests\AppBundle\TestableAwareTrait;
use Tests\AppBundle\TestableDatabaseAwareTrait;
use Tests\KinopoiskBundle\TestableKinopoiskAwareTrait;

class KinofitImportMoviesCommandTest extends KernelTestCase
{
    use TestableAwareTrait;
    use TestableDatabaseAwareTrait;
    use TestableKinopoiskAwareTrait;

    public function setUp()
    {
        parent::setUp();
        self::bootKernel();
        SoftMocks::restoreAll();
        $this->cleanTestsKinopoiskDirectories();
    }

    public function tearDown()
    {
        SoftMocks::restoreAll();
        parent::tearDown();
    }

    /**
     * @covers \KinofitImportMoviesCommand::execute()
     */
    public function testExecuteToImportSmallFile()
    {
        $this->dropTestsDatabase();
        $this->createTestsDatabase();

        /** @var \AppKernel $kernel */
        $kernel = self::$kernel;
        $em     = $kernel->getContainer()->get('doctrine.orm.entity_manager');

        $optionKinopoiskId  = 316376;
        $optionJsonFilepath = $kernel->getContainer()->getParameter('kernel.cache_dir').'/kinopoisk/find-movies/'.$optionKinopoiskId.'.json';

        if (!file_exists(dirname($optionJsonFilepath)) && !mkdir(dirname($optionJsonFilepath), 0777, true)) {
            throw new \RuntimeException(sprintf(
                'Couldn\'t make directory %s',
                dirname($optionJsonFilepath)
            ));
        }

        if (!file_put_contents($optionJsonFilepath, '[{"kinopoisk_id": 316376,"title_ru": "На Западном фронте без перемен","title_origin": "All Quiet on the Western Front"}]')) {
            throw new \RuntimeException(sprintf(
                'Couldn\'t write contents to the file %s',
                $optionJsonFilepath
            ));
        }

        $application = new Application(self::$kernel);
        $application->add(new KinofitImportMoviesCommand());

        $command       = $application->get(KinofitImportMoviesCommand::NAME);
        $commandTester = new CommandTester($command);
        $commandTester->execute(
            [
                'command'                                             => $command->getName(),
                '--'.KinofitImportMoviesCommand::OPTION_KINOPOISK_ID  => $optionKinopoiskId,
                '--'.KinofitImportMoviesCommand::OPTION_JSON_FILEPATH => $optionJsonFilepath,
                '--env'                                               => 'test',
            ],
            ['verbosity' => OutputInterface::VERBOSITY_DEBUG]
        );

        $this->assertSame(0, $commandTester->getStatusCode());
        $this->assertFileNotExists($optionJsonFilepath);

        /** @var Movie $movie */
        $movie = $em->getRepository(Movie::class)->findOneByKinopoiskId($optionKinopoiskId);

        $this->assertSame(Movie::class, get_class($movie));
        $this->assertSame('All Quiet on the Western Front', $movie->getTitleOrigin());
        $this->assertSame('На Западном фронте без перемен', $movie->getTranslations()->get('ru')->getTitle());
    }

    /**
     * @covers \KinofitImportMoviesCommand::execute()
     */
    public function testExecuteToImportFullableFile()
    {
        $this->dropTestsDatabase();
        $this->createTestsDatabase();
        $this->importCountries();
        $this->importGenres();

        /** @var \AppKernel $kernel */
        $kernel = self::$kernel;
        $em     = $kernel->getContainer()->get('doctrine.orm.entity_manager');

        $optionKinopoiskId  = 922167;
        $optionJsonFilepath = $kernel->getContainer()->getParameter('kernel.cache_dir').'/kinopoisk/parse-movies/'.$optionKinopoiskId.'.json';

        if (!file_exists(dirname($optionJsonFilepath)) && !mkdir(dirname($optionJsonFilepath), 0777, true)) {
            throw new \RuntimeException(sprintf(
                'Couldn\'t make directory %s',
                dirname($optionJsonFilepath)
            ));
        }

        $jsonString = '[
            {
                "kinopoisk_id":922167,
                "kinopoisk_description":"В Готэме происходит неожиданное: некий еретик «убивает» Бэтмена, свидетелем чего становится Бэтвумен. Однако команде Летучей мыши горевать некогда, ведь зло не дремлет. Теперь оставшимся напарникам Бэтмена Найтвингу (Дик Грейсон) и Робину (сын Бэтмена Дэмиан Уэйн) предстоит занять его пост и защищать жителей темного города.",
                "title_ru":"Бэтмен: Дурная кровь",
                "title_origin":"Batman: Bad Blood",
                "kinopoisk_rating":6.379,
                "imdb_rating":6.8,
                "kinopoisk_flag_tv":false,
                "kinopoisk_flag_video":true,
                "year":2016,
                "countries":[1],
                "tagline":null,
                "directors":[544388],
                "screenwriters":[702832,75829,326028],
                "producers":[1959873,1686257,75856],
                "operators":[],
                "composers":[656585],
                "painters":[],
                "editors":[3969937],
                "genres":[14,2,3,10],
                "age_limit":13
            }
        ]';

        if (!file_put_contents($optionJsonFilepath, $jsonString)) {
            throw new \RuntimeException(sprintf(
                'Couldn\'t write contents to the file %s',
                $optionJsonFilepath
            ));
        }

        $application = new Application(self::$kernel);
        $application->add(new KinofitImportMoviesCommand());

        $command       = $application->get(KinofitImportMoviesCommand::NAME);
        $commandTester = new CommandTester($command);
        $commandTester->execute(
            [
                'command'                                             => $command->getName(),
                '--'.KinofitImportMoviesCommand::OPTION_KINOPOISK_ID  => $optionKinopoiskId,
                '--'.KinofitImportMoviesCommand::OPTION_JSON_FILEPATH => $optionJsonFilepath,
                '--env'                                               => 'test',
            ],
            ['verbosity' => OutputInterface::VERBOSITY_DEBUG]
        );

        $this->assertSame(0, $commandTester->getStatusCode());
        $this->assertFileNotExists($optionJsonFilepath);

        /** @var Movie $movie */
        $movie = $em->getRepository(Movie::class)->findOneByKinopoiskId($optionKinopoiskId);

        $this->assertSame(Movie::class, get_class($movie));
        $this->assertSame('В Готэме происходит неожиданное: некий еретик «убивает» Бэтмена, свидетелем чего становится Бэтвумен. Однако команде Летучей мыши горевать некогда, ведь зло не дремлет. Теперь оставшимся напарникам Бэтмена Найтвингу (Дик Грейсон) и Робину (сын Бэтмена Дэмиан Уэйн) предстоит занять его пост и защищать жителей темного города.', $movie->getKinopoiskDescription());
        $this->assertSame('Бэтмен: Дурная кровь', $movie->getTranslations()->get('ru')->getTitle());
        $this->assertSame('Batman: Bad Blood', $movie->getTitleOrigin());
        $this->assertSame(6.379, $movie->getKinopoiskRating());
        $this->assertSame(6.8, $movie->getImdbRating());
        $this->assertSame(false, $movie->getKinopoiskFlagTv());
        $this->assertSame(true, $movie->getKinopoiskFlagVideo());
        $this->assertSame(2016, $movie->getYear());

        $this->assertSame(1, count($movie->getCountries()));
        $this->assertSame(1, $movie->getCountries()->get(0)->getKinopoiskId());

        $this->assertSame(null, $movie->getTagline());

        $this->assertSame(1, count($movie->getDirectors()));
        $this->assertSame(544388, $movie->getDirectors()->get(0)->getKinopoiskId());

        $this->assertSame(3, count($movie->getScreenwriters()));
        $this->assertSame(702832, $movie->getScreenwriters()->get(0)->getKinopoiskId());
        $this->assertSame(75829, $movie->getScreenwriters()->get(1)->getKinopoiskId());
        $this->assertSame(326028, $movie->getScreenwriters()->get(2)->getKinopoiskId());

        $this->assertSame(3, count($movie->getProducers()));
        $this->assertSame(1959873, $movie->getProducers()->get(0)->getKinopoiskId());
        $this->assertSame(1686257, $movie->getProducers()->get(1)->getKinopoiskId());
        $this->assertSame(75856, $movie->getProducers()->get(2)->getKinopoiskId());

        $this->assertSame(0, count($movie->getOperators()));

        $this->assertSame(1, count($movie->getComposers()));
        $this->assertSame(656585, $movie->getComposers()->get(0)->getKinopoiskId());

        $this->assertSame(0, count($movie->getPainters()));

        $this->assertSame(1, count($movie->getEditors()));
        $this->assertSame(3969937, $movie->getEditors()->get(0)->getKinopoiskId());

        $this->assertSame(4, count($movie->getGenres()));
        $this->assertSame(14, $movie->getGenres()->get(0)->getKinopoiskId());
        $this->assertSame(2, $movie->getGenres()->get(1)->getKinopoiskId());
        $this->assertSame(3, $movie->getGenres()->get(2)->getKinopoiskId());
        $this->assertSame(10, $movie->getGenres()->get(3)->getKinopoiskId());

        $this->assertSame(13, $movie->getAgeLimit());

        $newPersons = [
            544388,
            702832,
            75829,
            326028,
            1959873,
            1686257,
            75856,
            656585,
            3969937,
        ];

        foreach ($newPersons as $kinopoiskId) {
            /** @var CommandQueueRepository $queueRepo */
            $queueRepo = $em->getRepository(CommandQueue::class);
            $queue     = $queueRepo->findOneBy([
                'queue'     => 'kinopoisk-parse-persons',
                'command'   => 'kinopoisk:parse-persons',
                'arguments' => implode(' ', [
                    escapeshellarg('--kinopoisk-id').'='.escapeshellarg($kinopoiskId),
                    escapeshellarg('--env').'='.escapeshellarg('test'),
                    escapeshellarg('-vvv'),
                ]),
            ]);
            $this->assertNotNull($queue);
        }
    }
}
