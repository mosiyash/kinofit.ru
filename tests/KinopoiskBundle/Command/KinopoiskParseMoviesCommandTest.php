<?php

namespace KinopoiskBundle\Tests\Command;

require_once \AppMocks::rewrite(\AppMocks::findFilepathByNameInBundle('GuzzleHttpAwareTrait.php', 'KinopoiskBundle'));

use KinofitBundle\Command\KinofitImportMoviesCommand;
use KinofitBundle\Command\KinofitImportMoviesPostersCommand;
use KinopoiskBundle\Command\GuzzleHttpAwareTrait;
use KinopoiskBundle\Command\KinopoiskParseMoviesCommand;
use KinopoiskBundle\EntitySchema\MovieMeta;
use QA\SoftMocks;
use QueueBundle\Entity\CommandQueue;
use QueueBundle\Repository\CommandQueueRepository;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Tester\CommandTester;
use Symfony\Component\DomCrawler\Crawler;
use Symfony\Component\Finder\Finder;
use Tests\AppBundle\TestableAwareTrait;
use tests\AppBundle\TestableDatabaseAwareTrait;
use tests\KinopoiskBundle\TestableKinopoiskAwareTrait;

class KinopoiskParseMoviesCommandTest extends KernelTestCase
{
    use TestableAwareTrait;
    use TestableDatabaseAwareTrait;
    use TestableKinopoiskAwareTrait;

    public function setUp()
    {
        parent::setUp();
        self::bootKernel();
        SoftMocks::restoreAll();
        $this->cleanTestsKinopoiskDirectories();
    }

    public function tearDown()
    {
        SoftMocks::restoreAll();
        parent::tearDown();
    }

    public function providerExtractMovieInfo()
    {
        return [
            [
                1005003,
                'Они любят друг друга. Юные и беззаботные. Собираются пожениться. Но Анна Гарина из бедной семьи, а Олег Бузаев сын директора мясокомбината. Его отец даже слышать не желает о таком союзе. Да и Мать Анны категорически не хочет, чтобы Анна попала в эту семью. И родители делают все, чтобы разъединить молодых людей. Отец Олега объявляет своего сына умершим, а мать Анны лихорадочно выдает дочку замуж и отправляет жить в другой город.',
                'Злая судьба',
                null,
                null,
                null,
                true,
                false,
                2016,
                [2],
                null,
                [3488519],
                [],
                [],
                [],
                [],
                [],
                [],
                [7],
                12,
            ],
            [
                609618,
                'Фильм расскажет о том, как началось знаменитое противостояние птичек и свинок, персонажей популярной компьютерной игры, а также раскроет некоторые секреты любимых героев.',
                'Angry Birds в кино',
                'Angry Birds',
                6.395,
                6.3,
                false,
                false,
                2016,
                [1, 7],
                'Что такие сердитые?',
                [2759959, 1084285],
                [542598, 1692500, 2711684],
                [1692500, 1046996, 2711684],
                [],
                [611384],
                [3612134],
                [1669810, 149992],
                [14, 3, 6, 11],
                6,
            ],
            [
                922167,
                'В Готэме происходит неожиданное: некий еретик «убивает» Бэтмена, свидетелем чего становится Бэтвумен. Однако команде Летучей мыши горевать некогда, ведь зло не дремлет. Теперь оставшимся напарникам Бэтмена  Найтвингу (Дик Грейсон) и Робину (сын Бэтмена  Дэмиан Уэйн)  предстоит занять его пост и защищать жителей темного города.',
                'Бэтмен: Дурная кровь',
                'Batman: Bad Blood',
                6.379,
                6.8,
                false,
                true,
                2016,
                [1],
                null,
                [544388],
                [702832, 75829, 326028],
                [1959873, 1686257, 75856],
                [],
                [656585],
                [],
                [3969937],
                [14, 2, 3, 10],
                13,
            ],
        ];
    }

    /**
     * @covers \KinopoiskParseMoviesCommand::extractMovieInfo
     * @dataProvider providerExtractMovieInfo
     *
     * @param int         $kinopoiskId
     * @param string|null $kinopoiskDescription
     * @param string      $titleRu
     * @param string|null $titleOrigin
     * @param float|null  $kinopoiskRating
     * @param float|null  $imdbRating
     * @param bool        $kinopoiskFlagTv
     * @param bool        $kinopoiskFlagVideo
     * @param int         $year
     * @param array       $countries
     * @param string|null $tagline
     * @param array       $directors
     * @param array       $screenwriters
     * @param array       $producers
     * @param array       $operators
     * @param array       $composers
     * @param array       $painters
     * @param array       $editors
     * @param array       $genres
     * @param int|null    $ageLimit
     */
    public function testExtractMovieInfo(
        int $kinopoiskId,
        string $kinopoiskDescription = null,
        string $titleRu,
        string $titleOrigin = null,
        float $kinopoiskRating = null,
        float $imdbRating = null,
        bool $kinopoiskFlagTv,
        bool $kinopoiskFlagVideo,
        int $year,
        array $countries,
        string $tagline = null,
        array $directors,
        array $screenwriters,
        array $producers,
        array $operators,
        array $composers,
        array $painters,
        array $editors,
        array $genres,
        int $ageLimit = null
    ) {
        self::bootKernel();
        $application = new Application(self::$kernel);
        $application->add(new KinopoiskParseMoviesCommand());

        $command = $application->get(KinopoiskParseMoviesCommand::NAME);
        $crawler = new Crawler($this->loadFixture($kinopoiskId));
        $data    = $this->invokeMethod($command, 'extractMovieInfo', [$crawler]);

        $this->assertSame($kinopoiskId, $data[MovieMeta::KEY_KINOPOISK_ID]);
        $this->assertSame($kinopoiskDescription, $data[MovieMeta::KEY_KINOPOISK_DESCRIPTION]);
        $this->assertSame($titleRu, $data[MovieMeta::KEY_TITLE_RU]);
        $this->assertSame($titleOrigin, $data[MovieMeta::KEY_TITLE_ORIGIN]);
        $this->assertSame($kinopoiskRating, $data[MovieMeta::KEY_KINOPOISK_RATING]);
        $this->assertSame($imdbRating, $data[MovieMeta::KEY_IMDB_RATING]);
        $this->assertSame($kinopoiskFlagTv, $data[MovieMeta::KEY_KINOPOISK_FLAG_TV]);
        $this->assertSame($kinopoiskFlagVideo, $data[MovieMeta::KEY_KINOPOISK_FLAG_VIDEO]);
        $this->assertSame($year, $data[MovieMeta::KEY_YEAR]);
        $this->assertSame($countries, $data[MovieMeta::KEY_COUNTRIES]);
        $this->assertSame($tagline, $data[MovieMeta::KEY_TAGLINE]);
        $this->assertSame($directors, $data[MovieMeta::KEY_DIRECTORS]);
        $this->assertSame($screenwriters, $data[MovieMeta::KEY_SCREENWRITERS]);
        $this->assertSame($producers, $data[MovieMeta::KEY_PRODUCERS]);
        $this->assertSame($operators, $data[MovieMeta::KEY_OPERATORS]);
        $this->assertSame($composers, $data[MovieMeta::KEY_COMPOSERS]);
        $this->assertSame($painters, $data[MovieMeta::KEY_PAINTERS]);
        $this->assertSame($editors, $data[MovieMeta::KEY_EDITORS]);
        $this->assertSame($genres, $data[MovieMeta::KEY_GENRES]);
        $this->assertSame($ageLimit, $data[MovieMeta::KEY_AGE_LIMIT]);
    }

    /**
     * @param $id
     *
     * @return string
     */
    private function loadFixture($id): string
    {
        return file_get_contents(self::$kernel->getRootDir().'/../tests/KinopoiskBundle/Resources/fixtures/KinopoiskParseMoviesCommand/'.$id.'.html');
    }

    /**
     * @covers \KinopoiskParseMoviesCommand::execute()
     */
    public function testExecute()
    {
        $this->dropTestsDatabase();
        $this->createTestsDatabase();

        /** @var \AppKernel $kernel */
        $kernel = self::$kernel;
        $em     = $kernel->getContainer()->get('doctrine.orm.entity_manager');

        /** @var CommandQueueRepository $queueRepository */
        $queueRepository = $em->getRepository(CommandQueue::class);

        SoftMocks::redefineMethod(
            GuzzleHttpAwareTrait::class,
            'createRequest',
            '',
            \AppMocks::getRedefinedMethod('KinopoiskBundle', 'GuzzleHttpAwareTrait', 'createRequest', '02')
        );

        $application = new Application($kernel);
        $application->add(new KinopoiskParseMoviesCommand());

        $command       = $application->get(KinopoiskParseMoviesCommand::NAME);
        $commandTester = new CommandTester($command);
        $commandTester->execute(
            [
                'command'        => $command->getName(),
                '--kinopoisk-id' => 609618,
                '--env'          => 'test',
            ],
            ['verbosity' => OutputInterface::VERBOSITY_DEBUG]
        );

        $this->assertSame(0, $commandTester->getStatusCode());

        $finder = Finder::create()
            ->in($kernel->getContainer()->getParameter('kernel.cache_dir').'/kinopoisk')
            ->name('609618.json');

        $this->assertSame(1, $finder->count());

        foreach ($finder as $file) {
            $queue0 = $queueRepository->findOneBy([
                'queue'     => KinofitImportMoviesCommand::QUEUE_NAME,
                'command'   => KinofitImportMoviesCommand::NAME,
                'arguments' => implode(' ', [
                    escapeshellarg('--'.KinofitImportMoviesCommand::OPTION_KINOPOISK_ID).'='.escapeshellarg(609618),
                    escapeshellarg('--'.KinofitImportMoviesCommand::OPTION_JSON_FILEPATH).'='.escapeshellarg($file->getPathname()),
                    escapeshellarg('--env').'='.escapeshellarg('test'),
                    escapeshellarg('-vvv'),
                ]),
            ]);
            $this->assertNotNull($queue0);

            $queue1 = $queueRepository->findOneBy([
                'queue'     => KinofitImportMoviesPostersCommand::QUEUE_NAME,
                'command'   => KinofitImportMoviesPostersCommand::NAME,
                'arguments' => implode(' ', [
                    escapeshellarg('--'.KinofitImportMoviesPostersCommand::OPTION_KINOPOISK_ID).'='.escapeshellarg(609618),
                    escapeshellarg('--'.KinofitImportMoviesPostersCommand::OPTION_POSTER_URL).'='.escapeshellarg('https://st.kp.yandex.net/images/film_big/609618.jpg'),
                    escapeshellarg('--env').'='.escapeshellarg('test'),
                    escapeshellarg('-vvv'),
                ]),
            ]);
            $this->assertNotNull($queue1);
        }
    }
}
