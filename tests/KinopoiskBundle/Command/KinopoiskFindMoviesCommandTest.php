<?php

namespace KinopoiskBundle\Command;

require_once \AppMocks::rewrite(\AppMocks::findFilepathByNameInBundle('GuzzleHttpAwareTrait.php', 'KinopoiskBundle'));

use KinofitBundle\Command\KinofitImportMoviesCommand;
use QA\SoftMocks;
use QueueBundle\Entity\CommandQueue;
use QueueBundle\Repository\CommandQueueRepository;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Tester\CommandTester;
use Symfony\Component\Finder\Finder;
use Tests\AppBundle\TestableAwareTrait;
use Tests\AppBundle\TestableDatabaseAwareTrait;
use Tests\KinopoiskBundle\TestableKinopoiskAwareTrait;

class KinopoiskFindMoviesCommandTest extends KernelTestCase
{
    use TestableAwareTrait;
    use TestableDatabaseAwareTrait;
    use TestableKinopoiskAwareTrait;

    public function setUp()
    {
        parent::setUp();
        self::bootKernel();

        $this->cleanTestsKinopoiskDirectories();
    }

    public function tearDown()
    {
        SoftMocks::restoreAll();

        parent::tearDown();
    }

    /**
     * @covers \KinopoiskFindMoviesCommand::execute()
     */
    public function testExecute()
    {
        $this->dropTestsDatabase();
        $this->createTestsDatabase();

        /** @var \AppKernel $kernel */
        $kernel = self::$kernel;
        $em     = $kernel->getContainer()->get('doctrine.orm.entity_manager');

        SoftMocks::redefineMethod(
            GuzzleHttpAwareTrait::class,
            'createRequest',
            '',
            \AppMocks::getRedefinedMethod('KinopoiskBundle', 'GuzzleHttpAwareTrait', 'createRequest', '00')
        );

        $application = new Application(self::$kernel);
        $application->add(new KinopoiskFindMoviesCommand());

        $command       = $application->get(KinopoiskFindMoviesCommand::NAME);
        $commandTester = new CommandTester($command);
        $commandTester->execute(
            [
                'command'          => $command->getName(),
                '--navigator-page' => 1,
                '--env'            => 'test',
            ],
            ['verbosity' => OutputInterface::VERBOSITY_DEBUG]
        );

        $this->assertSame(0, $commandTester->getStatusCode());

        $finder = Finder::create()
            ->in($kernel->getContainer()->getParameter('kernel.cache_dir').'/kinopoisk')
            ->name('*.json');

        $this->assertSame(3, $finder->count());

        $conformity = [
            '316376.json' => '[{"kinopoisk_id": 316376,"title_ru": "На Западном фронте без перемен","title_origin": "All Quiet on the Western Front"}]',
            '502322.json' => '[{"kinopoisk_id": 502322,"title_ru": "Все в жизни бывает","title_origin": "Koochie Koochie Hota Hai"}]',
            '882309.json' => '[{"kinopoisk_id": 882309,"title_ru": "В центре внимания","title_origin": "Limelight"}]',
        ];

        foreach ($finder as $file) {
            $this->assertJsonStringEqualsJsonString(
                $conformity[basename($file->getPathname())],
                file_get_contents($file->getPathname())
            );

            $kinopoiskId = preg_replace('/[^\d]+/i', '', basename($file->getPathname()));
            $this->assertRegExp('/^\d+$/i', $kinopoiskId);

            /** @var CommandQueueRepository $repo */
            $repo = $em->getRepository(CommandQueue::class);

            $queue0 = $repo->findOneBy([
                'queue'     => KinofitImportMoviesCommand::QUEUE_NAME,
                'command'   => KinofitImportMoviesCommand::NAME,
                'arguments' => implode(' ', [
                    escapeshellarg('--'.KinofitImportMoviesCommand::OPTION_KINOPOISK_ID).'='.escapeshellarg($kinopoiskId),
                    escapeshellarg('--'.KinofitImportMoviesCommand::OPTION_JSON_FILEPATH).'='.escapeshellarg($file->getPathname()),
                    escapeshellarg('--env').'='.escapeshellarg($kernel->getContainer()->get('kernel')->getEnvironment()),
                    escapeshellarg('-vvv'),
                ]),
            ]);
            $this->assertNotNull($queue0);

            $queue1 = $repo->findOneBy([
                'queue'     => KinopoiskParseMoviesCommand::QUEUE_NAME,
                'command'   => KinopoiskParseMoviesCommand::NAME,
                'arguments' => implode(' ', [
                    escapeshellarg('--'.KinopoiskParseMoviesCommand::OPTION_KINOPOISK_ID).'='.escapeshellarg($kinopoiskId),
                    escapeshellarg('--env').'='.escapeshellarg($kernel->getContainer()->get('kernel')->getEnvironment()),
                    escapeshellarg('-vvv'),
                ]),
            ]);
            $this->assertNotNull($queue1);
        }
    }
}
