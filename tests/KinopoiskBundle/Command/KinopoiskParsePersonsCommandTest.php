<?php

namespace KinopoiskBundle\Tests\Command;

require_once \AppMocks::rewrite(\AppMocks::findFilepathByNameInBundle('GuzzleHttpAwareTrait.php', 'KinopoiskBundle'));

use KinofitBundle\Command\KinofitImportPersonsCommand;
use KinopoiskBundle\Command\GuzzleHttpAwareTrait;
use KinopoiskBundle\Command\KinopoiskParsePersonsCommand;
use KinopoiskBundle\EntitySchema\PersonMeta;
use QA\SoftMocks;
use QueueBundle\Entity\CommandQueue;
use QueueBundle\Repository\CommandQueueRepository;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Tester\CommandTester;
use Symfony\Component\DomCrawler\Crawler;
use Symfony\Component\Finder\Finder;
use Tests\AppBundle\TestableAwareTrait;
use Tests\AppBundle\TestableDatabaseAwareTrait;
use Tests\KinopoiskBundle\TestableKinopoiskAwareTrait;

class KinopoiskParsePersonsCommandTest extends KernelTestCase
{
    use TestableAwareTrait;
    use TestableDatabaseAwareTrait;
    use TestableKinopoiskAwareTrait;

    public function setUp()
    {
        parent::setUp();
        self::bootKernel();
        SoftMocks::restoreAll();

        $this->cleanTestsKinopoiskDirectories();
    }

    public function tearDown()
    {
        SoftMocks::restoreAll();

        parent::tearDown();
    }

    /**
     * @covers \KinopoiskParsePersonsCommand::extractPersonInfo
     */
    public function testExtractPersonInfo()
    {
        $application = new Application(self::$kernel);
        $application->add(new KinopoiskParsePersonsCommand());

        $command = $application->get(KinopoiskParsePersonsCommand::NAME);
        $crawler = new Crawler($this->loadFixture(1096253));
        $data    = $this->invokeMethod($command, 'extractPersonInfo', [$crawler]);

        $this->assertSame(1096253, $data[PersonMeta::KEY_KINOPOISK_ID]);
        $this->assertSame('Jason', $data[PersonMeta::KEY_FIRSTNAME_EN]);
        $this->assertSame('Джейсон', $data[PersonMeta::KEY_FIRSTNAME_RU]);
        $this->assertSame('Sudeikis', $data[PersonMeta::KEY_LASTNAME_EN]);
        $this->assertSame('Судейкис', $data[PersonMeta::KEY_LASTNAME_RU]);
        $this->assertSame(null, $data[PersonMeta::KEY_PATRONYMIC_EN]);
        $this->assertSame(null, $data[PersonMeta::KEY_PATRONYMIC_RU]);
    }

    /**
     * @covers \KinopoiskParsePersonsCommand::execute()
     */
    public function testExecute()
    {
        $this->dropTestsDatabase();
        $this->createTestsDatabase();

        /** @var \AppKernel $kernel */
        $kernel = self::$kernel;
        $em     = $kernel->getContainer()->get('doctrine.orm.entity_manager');

        /** @var CommandQueueRepository $queueRepository */
        $queueRepository = $em->getRepository(CommandQueue::class);

        SoftMocks::redefineMethod(
            GuzzleHttpAwareTrait::class,
            'createRequest',
            '',
            \AppMocks::getRedefinedMethod('KinopoiskBundle', 'GuzzleHttpAwareTrait', 'createRequest', '01')
        );

        $application = new Application(self::$kernel);
        $application->add(new KinopoiskParsePersonsCommand());

        $command       = $application->get(KinopoiskParsePersonsCommand::NAME);
        $commandTester = new CommandTester($command);
        $commandTester->execute(
            [
                'command'        => $command->getName(),
                '--kinopoisk-id' => 1,
                '--env'          => 'test',
            ],
            ['verbosity' => OutputInterface::VERBOSITY_DEBUG]
        );

        $this->assertSame(0, $commandTester->getStatusCode());

        $finder = Finder::create()
            ->in($kernel->getContainer()->getParameter('kernel.cache_dir').'/kinopoisk')
            ->name('*.json');

        $this->assertSame(1, $finder->count());

        foreach ($finder as $file) {
            $this->assertJsonStringEqualsJsonString(
                '[{"kinopoisk_id": 1096253,"firstname_en": "Jason","firstname_ru": "Джейсон","lastname_en": "Sudeikis","lastname_ru": "Судейкис","patronymic_en": null,"patronymic_ru": null}]',
                file_get_contents($file->getPathname())
            );

            $queue = $queueRepository->findOneBy([
                'queue'     => KinofitImportPersonsCommand::QUEUE_NAME,
                'command'   => KinofitImportPersonsCommand::NAME,
                'arguments' => implode(' ', [
                    escapeshellarg('--'.KinofitImportPersonsCommand::OPTION_KINOPOISK_ID).'='.escapeshellarg(1096253),
                    escapeshellarg('--'.KinofitImportPersonsCommand::OPTION_JSON_FILEPATH).'='.escapeshellarg($file->getPathname()),
                    escapeshellarg('--env').'='.escapeshellarg('test'),
                    escapeshellarg('-vvv'),
                ]),
            ]);
            $this->assertNotNull($queue);
        }
    }

    /**
     * @param $id
     *
     * @return string
     */
    private function loadFixture($id): string
    {
        return file_get_contents(self::$kernel->getRootDir().'/../tests/KinopoiskBundle/Resources/fixtures/KinopoiskParsePersonsCommand/'.$id.'.html');
    }
}
