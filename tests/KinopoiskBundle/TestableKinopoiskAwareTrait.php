<?php

namespace tests\KinopoiskBundle;

use Symfony\Component\Process\Process;

/**
 * @property \AppKernel $kernel
 */
trait TestableKinopoiskAwareTrait
{
    private function cleanTestsKinopoiskDirectories()
    {
        /** @var \AppKernel $kernel */
        $kernel  = self::$kernel;
        $process = new Process('rm -rf '.escapeshellarg($kernel->getCacheDir().'/kinopoisk'));
        $process->mustRun();
        $this->assertSame(0, $process->getExitCode());
        $this->assertFalse(file_exists($kernel->getCacheDir().'/kinopoisk'));
    }
}
