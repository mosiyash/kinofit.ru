<?php

/** @var \KinopoiskBundle\Command\KinopoiskParsePersonsCommand $this */
$body = new \GuzzleHttp\Psr7\Stream(fopen('php://temp', 'r+'));
$body->write(file_get_contents($this->getContainer()->getParameter('project_dir').'/tests/KinopoiskBundle/Resources/fixtures/KinopoiskParsePersonsCommand/1.html'));
$response = new \GuzzleHttp\Psr7\Response();
$response = $response->withBody($body);

return $response;
