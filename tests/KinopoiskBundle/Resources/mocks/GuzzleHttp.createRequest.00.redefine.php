<?php

/** @var \KinopoiskBundle\Command\KinopoiskFindMoviesCommand $this */
$body = new \GuzzleHttp\Psr7\Stream(fopen('php://temp', 'r+'));
$body->write(file_get_contents($this->getContainer()->getParameter('project_dir').'/tests/KinopoiskBundle/Resources/fixtures/KinopoiskFindMoviesCommand/navigator-page.html'));
$response = new \GuzzleHttp\Psr7\Response();
$response = $response->withBody($body);

return $response;
