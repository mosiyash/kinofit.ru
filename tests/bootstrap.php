<?php

require_once dirname(__DIR__).'/vendor/badoo/soft-mocks/src/bootstrap.php';
require_once dirname(__DIR__).'/app/AppMocks.php';

AppMocks::setMocksCachePath(dirname(__DIR__).'/var/cache/test/mocks');

use Composer\Autoload\ClassLoader;
use Doctrine\Common\Annotations\AnnotationRegistry;

/** @var ClassLoader $loader */
$loader = require __DIR__.'/../vendor/autoload.php';

AnnotationRegistry::registerLoader([$loader, 'loadClass']);

return $loader;
