<?php

namespace tests\AccountBundle\Controller;

use AccountBundle\Entity\Account;
use AccountBundle\Repository\AccountRepository;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\DomCrawler\Crawler;
use Tests\AppBundle\TestableAwareTrait;
use Tests\AppBundle\TestableDatabaseAwareTrait;

class DefaultControllerTest extends WebTestCase
{
    use TestableAwareTrait;
    use TestableDatabaseAwareTrait;

    public function setUp()
    {
        parent::setUp();
        self::bootKernel();
    }

    public function tearDown()
    {
        parent::tearDown();
    }

    /**
     * @covers \DefaultController::loginAction()
     */
    public function testLoginAction()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/login');

        $this->assertContains('Hello World', $client->getResponse()->getContent());
    }

    /**
     * @covers \DefaultController::registerAction()
     */
    public function testRegisterAction()
    {
        $this->dropTestsDatabase();
        $this->createTestsDatabase();

        /** @var \AppKernel $kernel */
        $kernel = self::$kernel;

        $em           = $kernel->getContainer()->get('doctrine.orm.entity_manager');
        $client       = static::createClient();
        $formSelector = 'form[name=registration]';

        /** @var AccountRepository $accountRepository */
        $accountRepository = $em->getRepository(Account::class);

        $crawler                        = $client->request('GET', '/register');
        $form                           = $crawler->filter($formSelector)->form();
        $form['registration[email]']    = '';
        $form['registration[username]'] = '';
        $form['registration[password]'] = '';

        $crawler = $client->submit($form);
        $this->assertFieldErrorIsPresented($crawler, $formSelector, 'registration[email]');
        $this->assertFieldErrorIsPresented($crawler, $formSelector, 'registration[username]');
        $this->assertFieldErrorIsPresented($crawler, $formSelector, 'registration[password]');
        $this->assertNull($accountRepository->findOneBy(['email' => 'test@test.loc', 'username' => 'test']));
        $this->assertSame(0, count($accountRepository->findAll()));

        $form                           = $crawler->filter($formSelector)->form();
        $form['registration[email]']    = 'test@test.loc';
        $form['registration[username]'] = 'test';
        $form['registration[password]'] = 'test';

        $crawler = $client->submit($form);
        $this->assertGlobalFormErrorsIsNotPresented($crawler, $formSelector);

        $this->assertNotNull($accountRepository->findOneBy(['email' => 'test@test.loc', 'username' => 'test']));
        $this->assertSame(1, count($accountRepository->findAll()));

        $crawler                        = $client->request('GET', '/register');
        $form                           = $crawler->filter($formSelector)->form();
        $form['registration[email]']    = 'test@test.loc';
        $form['registration[username]'] = 'test';
        $form['registration[password]'] = 'test';

        $crawler = $client->submit($form);
        $this->assertGlobalFormErrorIsPresented($crawler, $formSelector, 'Username or email already exists');
        $this->assertNotNull($accountRepository->findOneBy(['email' => 'test@test.loc', 'username' => 'test']));
        $this->assertSame(1, count($accountRepository->findAll()));
    }

    /**
     * @param Crawler     $crawler
     * @param string      $formSelector
     * @param string      $fieldName
     * @param string|null $errorText
     */
    private function assertFieldErrorIsPresented(Crawler $crawler, string $formSelector, string $fieldName, string $errorText = null): void
    {
        $form  = $crawler->filter($formSelector)->eq(0);
        $field = $form->filter('*[name="'.$fieldName.'"]')->eq(0);
        $this->assertCount(1, $field->parents()->eq(0)->filter('small.error'));
        if ($errorText) {
            $this->assertContains($errorText, $field->parents()->eq(0)->filter('small.error')->text());
        }
    }

    /**
     * @param Crawler     $crawler
     * @param string      $formSelector
     * @param string|null $errorText
     */
    private function assertGlobalFormErrorIsPresented(Crawler $crawler, string $formSelector, string $errorText = null): void
    {
        $form   = $crawler->filter($formSelector)->eq(0);
        $errors = $crawler->filter('.form-global-error li');
        $this->assertGreaterThan(0, $errors->count());
        if ($errorText) {
            $this->assertContains($errorText, $errors->text());
        }
    }

    /**
     * @param Crawler $crawler
     * @param string  $formSelector
     */
    private function assertGlobalFormErrorsIsNotPresented(Crawler $crawler, string $formSelector): void
    {
        $form   = $crawler->filter($formSelector)->eq(0);
        $errors = $crawler->filter('.form-global-error li');
        $this->assertSame(0, $errors->count());
    }
}
