<?php

use Eloquent\Pathogen\FileSystem\FileSystemPath;
use QA\SoftMocks;
use Symfony\Component\Finder\Finder;

class AppMocks extends SoftMocks
{
    public static function bootstrap()
    {
        // self::setMocksCachePath(dirname(__DIR__).'/var/cache/test/mocks');
    }

    /**
     * @param string $name
     * @param string $bundle
     *
     * @throws RuntimeException
     *
     * @return string
     */
    public static function findFilepathByNameInBundle(string $name, string $bundle): string
    {
        $finder = Finder::create()
            ->in(dirname(__DIR__).'/src/'.$bundle)
            ->in(dirname(__DIR__).'/tests/'.$bundle)
            ->name($name);

        foreach ($finder as $file) {
            return FileSystemPath::fromString($file->getPathname())->normalize();
        }

        throw new RuntimeException(sprintf(
            'File %s not found in bundle %s (searched in %s and %s)',
            $name,
            $bundle,
            dirname(__DIR__).'/src/'.$bundle,
            dirname(__DIR__).'/tests/'.$bundle
        ));
    }

    /**
     * @param string $bundle
     * @param string $class
     * @param string $method
     * @param string $variant
     *
     * @throws RuntimeException
     *
     * @return string
     */
    public static function getRedefinedMethod(string $bundle, string $class, string $method, string $variant = '00'): string
    {
        $finder = Finder::create()
            ->in(dirname(__DIR__).'/tests/'.$bundle)
            ->name($class.'.'.$method.'.'.$variant.'.redefine.php');

        foreach ($finder as $file) {
            $contents = file_get_contents($file->getPathname());
            if (!$contents) {
                throw new RuntimeException(sprintf(
                    'Couldn\'t get contents from the file %s',
                    $file->getPathname()
                ));
            }
            // https://regex101.com/r/uQ5yul/2
            $contents = preg_replace('/^<\?php\s*/isu', '', $contents);

            return $contents;
        }

        throw new RuntimeException(sprintf(
            'File %s is not found in the bundle %s (searched in %s)',
            $class.'.'.$method.'.'.$variant.'.redefine.php',
            $bundle,
            dirname(__DIR__).'/tests/'.$bundle
        ));
    }

    /**
     * @param array  $in
     * @param string $namePattern
     */
    public static function rewriteFiles(array $in, string $namePattern): void
    {
        $files = [];

        foreach ($in as $dir) {
            $directory = new RecursiveDirectoryIterator($dir);
            $iterator  = new RecursiveIteratorIterator($directory);
            $regex     = new RegexIterator($iterator, $namePattern, RecursiveRegexIterator::GET_MATCH);
            foreach ($regex as $pathname => $file) {
                $files[] = $pathname;
            }
        }

        $files = array_unique($files);

        foreach ($files as $pathname) {
            self::rewrite($pathname);
        }
    }
}
