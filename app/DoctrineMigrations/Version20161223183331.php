<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20161223183331 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE queue_log (id INT AUTO_INCREMENT NOT NULL, message_id INT DEFAULT NULL, date_log DATETIME NOT NULL, log LONGTEXT NOT NULL, INDEX IDX_D5C2F5E2537A1329 (message_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE queue (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(100) NOT NULL, timeout SMALLINT NOT NULL, max_retries INT DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE queue_message (id INT AUTO_INCREMENT NOT NULL, queue_id INT DEFAULT NULL, handle VARCHAR(32) DEFAULT NULL, body LONGTEXT NOT NULL, md5 VARCHAR(32) NOT NULL, timeout NUMERIC(10, 0) DEFAULT NULL, created INT NOT NULL, priority SMALLINT NOT NULL, failed TINYINT(1) NOT NULL, num_retries INT NOT NULL, ended TINYINT(1) NOT NULL, INDEX IDX_543FAB41477B5BAE (queue_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE queue_log ADD CONSTRAINT FK_D5C2F5E2537A1329 FOREIGN KEY (message_id) REFERENCES queue_message (id)');
        $this->addSql('ALTER TABLE queue_message ADD CONSTRAINT FK_543FAB41477B5BAE FOREIGN KEY (queue_id) REFERENCES queue (id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE queue_message DROP FOREIGN KEY FK_543FAB41477B5BAE');
        $this->addSql('ALTER TABLE queue_log DROP FOREIGN KEY FK_D5C2F5E2537A1329');
        $this->addSql('DROP TABLE queue_log');
        $this->addSql('DROP TABLE queue');
        $this->addSql('DROP TABLE queue_message');
    }
}
