<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20161226115523 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE command_queue_log DROP FOREIGN KEY FK_52F3BAE9477B5BAE;');
        $this->addSql('DROP TABLE command_queue_log;');
        $this->addSql('DROP TABLE command_queue;');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE command_queue ( id INT(11) PRIMARY KEY NOT NULL AUTO_INCREMENT, queue VARCHAR(255) NOT NULL, called_at DATETIME, command VARCHAR(255) NOT NULL, arguments LONGTEXT, priority SMALLINT(6) DEFAULT \'0\' NOT NULL, failed TINYINT(1) DEFAULT \'0\' NOT NULL, num_retries INT(11) DEFAULT \'0\' NOT NULL, ended TINYINT(1) DEFAULT \'0\' NOT NULL );');
        $this->addSql('CREATE TABLE command_queue_log ( id INT(11) PRIMARY KEY NOT NULL AUTO_INCREMENT, queue_id INT(11), message LONGTEXT NOT NULL, trace LONGTEXT, CONSTRAINT FK_52F3BAE9477B5BAE FOREIGN KEY (queue_id) REFERENCES command_queue (id));');
        $this->addSql(' CREATE INDEX IDX_52F3BAE9477B5BAE ON command_queue_log (queue_id);');
    }
}
