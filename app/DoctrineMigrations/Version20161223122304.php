<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20161223122304 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE movie_translation (id INT AUTO_INCREMENT NOT NULL, translatable_id INT DEFAULT NULL, title VARCHAR(255) DEFAULT NULL, slug VARCHAR(255) DEFAULT NULL, locale VARCHAR(255) NOT NULL, INDEX IDX_9BEFC9982C2AC5D3 (translatable_id), UNIQUE INDEX movie_translation_unique_translation (translatable_id, locale), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE transmission_queue (id INT AUTO_INCREMENT NOT NULL, movie_id INT DEFAULT NULL, name VARCHAR(255) NOT NULL, size INT NOT NULL, announce VARCHAR(255) NOT NULL, announce_list LONGTEXT NOT NULL COMMENT \'(DC2Type:simple_array)\', comment VARCHAR(255) NOT NULL, encoded_hash VARCHAR(255) NOT NULL, extra_meta LONGTEXT NOT NULL COMMENT \'(DC2Type:simple_array)\', file_list LONGTEXT NOT NULL COMMENT \'(DC2Type:simple_array)\', hash VARCHAR(255) NOT NULL, info LONGTEXT NOT NULL COMMENT \'(DC2Type:simple_array)\', piece_length_exp INT NOT NULL, created_at DATETIME NOT NULL, created_by VARCHAR(255) NOT NULL, INDEX IDX_5B2663758F93B6FC (movie_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE country_translation (id INT AUTO_INCREMENT NOT NULL, translatable_id INT DEFAULT NULL, title VARCHAR(255) NOT NULL, slug VARCHAR(255) DEFAULT NULL, locale VARCHAR(255) NOT NULL, UNIQUE INDEX UNIQ_A1FE6FA42B36786B (title), INDEX IDX_A1FE6FA42C2AC5D3 (translatable_id), UNIQUE INDEX country_translation_unique_translation (translatable_id, locale), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE person (id INT AUTO_INCREMENT NOT NULL, kinopoiskId INT DEFAULT NULL, UNIQUE INDEX UNIQ_34DCD1767F6E652D (kinopoiskId), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE genre (id INT AUTO_INCREMENT NOT NULL, kinopoiskId INT DEFAULT NULL, count_movies INT DEFAULT 0 NOT NULL, count_tvseries INT DEFAULT 0 NOT NULL, UNIQUE INDEX UNIQ_835033F87F6E652D (kinopoiskId), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE genre_translation (id INT AUTO_INCREMENT NOT NULL, translatable_id INT DEFAULT NULL, title VARCHAR(255) DEFAULT NULL, slug VARCHAR(255) DEFAULT NULL, locale VARCHAR(255) NOT NULL, INDEX IDX_FF11F8A42C2AC5D3 (translatable_id), UNIQUE INDEX genre_translation_unique_translation (translatable_id, locale), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE file (id INT AUTO_INCREMENT NOT NULL, filesystem VARCHAR(255) NOT NULL, path LONGTEXT DEFAULT NULL, basename LONGTEXT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE person_translation (id INT AUTO_INCREMENT NOT NULL, translatable_id INT DEFAULT NULL, firstname VARCHAR(255) NOT NULL, lastname VARCHAR(255) NOT NULL, patronymic VARCHAR(255) DEFAULT NULL, slug VARCHAR(255) DEFAULT NULL, locale VARCHAR(255) NOT NULL, INDEX IDX_D9821AE72C2AC5D3 (translatable_id), UNIQUE INDEX person_translation_unique_translation (translatable_id, locale), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE country (id INT AUTO_INCREMENT NOT NULL, kinopoiskId INT DEFAULT NULL, count_movies INT DEFAULT 0 NOT NULL, count_tvseries INT DEFAULT 0 NOT NULL, UNIQUE INDEX UNIQ_5373C9667F6E652D (kinopoiskId), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE movie (id INT AUTO_INCREMENT NOT NULL, poster_id INT DEFAULT NULL, kinopoisk_id INT DEFAULT NULL, title_origin VARCHAR(255) DEFAULT NULL, kinopoisk_flag_tv TINYINT(1) NOT NULL, kinopoisk_flag_video TINYINT(1) NOT NULL, kinopoisk_description LONGTEXT DEFAULT NULL, kinopoisk_rating DOUBLE PRECISION DEFAULT NULL, imdb_rating DOUBLE PRECISION DEFAULT NULL, year INT DEFAULT NULL, tagline LONGTEXT DEFAULT NULL, age_limit SMALLINT DEFAULT NULL, popularity DOUBLE PRECISION DEFAULT \'0\' NOT NULL, auto_status ENUM(\'draft\', \'held\', \'published\') NOT NULL COMMENT \'(DC2Type:ContentAutoStatusType)\', UNIQUE INDEX UNIQ_1D5EF26F7078D953 (kinopoisk_id), UNIQUE INDEX UNIQ_1D5EF26F5BB66C05 (poster_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE movie_country (movie_id INT NOT NULL, country_id INT NOT NULL, INDEX IDX_73E58B488F93B6FC (movie_id), INDEX IDX_73E58B48F92F3E70 (country_id), PRIMARY KEY(movie_id, country_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE movie_director (movie_id INT NOT NULL, person_id INT NOT NULL, INDEX IDX_C266487D8F93B6FC (movie_id), INDEX IDX_C266487D217BBB47 (person_id), PRIMARY KEY(movie_id, person_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE movie_screenwriter (movie_id INT NOT NULL, person_id INT NOT NULL, INDEX IDX_4369F5698F93B6FC (movie_id), INDEX IDX_4369F569217BBB47 (person_id), PRIMARY KEY(movie_id, person_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE movie_producer (movie_id INT NOT NULL, person_id INT NOT NULL, INDEX IDX_4B92D2518F93B6FC (movie_id), INDEX IDX_4B92D251217BBB47 (person_id), PRIMARY KEY(movie_id, person_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE movie_operator (movie_id INT NOT NULL, person_id INT NOT NULL, INDEX IDX_B503C0C8F93B6FC (movie_id), INDEX IDX_B503C0C217BBB47 (person_id), PRIMARY KEY(movie_id, person_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE movie_composer (movie_id INT NOT NULL, person_id INT NOT NULL, INDEX IDX_44859D558F93B6FC (movie_id), INDEX IDX_44859D55217BBB47 (person_id), PRIMARY KEY(movie_id, person_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE movie_painter (movie_id INT NOT NULL, person_id INT NOT NULL, INDEX IDX_28984BF68F93B6FC (movie_id), INDEX IDX_28984BF6217BBB47 (person_id), PRIMARY KEY(movie_id, person_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE movie_editor (movie_id INT NOT NULL, person_id INT NOT NULL, INDEX IDX_35366CCF8F93B6FC (movie_id), INDEX IDX_35366CCF217BBB47 (person_id), PRIMARY KEY(movie_id, person_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE movie_genre (movie_id INT NOT NULL, genre_id INT NOT NULL, INDEX IDX_FD1229648F93B6FC (movie_id), INDEX IDX_FD1229644296D31F (genre_id), PRIMARY KEY(movie_id, genre_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE command_queue (id INT AUTO_INCREMENT NOT NULL, queue VARCHAR(255) NOT NULL, called_at DATETIME DEFAULT NULL, command VARCHAR(255) NOT NULL, arguments LONGTEXT DEFAULT NULL, priority SMALLINT DEFAULT 0 NOT NULL, failed TINYINT(1) DEFAULT \'0\' NOT NULL, num_retries INT DEFAULT 0 NOT NULL, ended TINYINT(1) DEFAULT \'0\' NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE command_queue_log (id INT AUTO_INCREMENT NOT NULL, queue_id INT DEFAULT NULL, message LONGTEXT NOT NULL, trace LONGTEXT DEFAULT NULL, INDEX IDX_52F3BAE9477B5BAE (queue_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE account (id INT AUTO_INCREMENT NOT NULL, vkontakte_id INT DEFAULT NULL, username VARCHAR(255) NOT NULL, email VARCHAR(255) NOT NULL, password VARCHAR(255) DEFAULT NULL, is_active TINYINT(1) NOT NULL, confirmed TINYINT(1) NOT NULL, created_at DATETIME NOT NULL, confirmed_at DATETIME DEFAULT NULL, UNIQUE INDEX UNIQ_7D3656A4F85E0677 (username), UNIQUE INDEX UNIQ_7D3656A4E7927C74 (email), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE movie_translation ADD CONSTRAINT FK_9BEFC9982C2AC5D3 FOREIGN KEY (translatable_id) REFERENCES movie (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE transmission_queue ADD CONSTRAINT FK_5B2663758F93B6FC FOREIGN KEY (movie_id) REFERENCES movie (id)');
        $this->addSql('ALTER TABLE country_translation ADD CONSTRAINT FK_A1FE6FA42C2AC5D3 FOREIGN KEY (translatable_id) REFERENCES country (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE genre_translation ADD CONSTRAINT FK_FF11F8A42C2AC5D3 FOREIGN KEY (translatable_id) REFERENCES genre (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE person_translation ADD CONSTRAINT FK_D9821AE72C2AC5D3 FOREIGN KEY (translatable_id) REFERENCES person (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE movie ADD CONSTRAINT FK_1D5EF26F5BB66C05 FOREIGN KEY (poster_id) REFERENCES file (id)');
        $this->addSql('ALTER TABLE movie_country ADD CONSTRAINT FK_73E58B488F93B6FC FOREIGN KEY (movie_id) REFERENCES movie (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE movie_country ADD CONSTRAINT FK_73E58B48F92F3E70 FOREIGN KEY (country_id) REFERENCES country (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE movie_director ADD CONSTRAINT FK_C266487D8F93B6FC FOREIGN KEY (movie_id) REFERENCES movie (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE movie_director ADD CONSTRAINT FK_C266487D217BBB47 FOREIGN KEY (person_id) REFERENCES person (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE movie_screenwriter ADD CONSTRAINT FK_4369F5698F93B6FC FOREIGN KEY (movie_id) REFERENCES movie (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE movie_screenwriter ADD CONSTRAINT FK_4369F569217BBB47 FOREIGN KEY (person_id) REFERENCES person (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE movie_producer ADD CONSTRAINT FK_4B92D2518F93B6FC FOREIGN KEY (movie_id) REFERENCES movie (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE movie_producer ADD CONSTRAINT FK_4B92D251217BBB47 FOREIGN KEY (person_id) REFERENCES person (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE movie_operator ADD CONSTRAINT FK_B503C0C8F93B6FC FOREIGN KEY (movie_id) REFERENCES movie (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE movie_operator ADD CONSTRAINT FK_B503C0C217BBB47 FOREIGN KEY (person_id) REFERENCES person (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE movie_composer ADD CONSTRAINT FK_44859D558F93B6FC FOREIGN KEY (movie_id) REFERENCES movie (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE movie_composer ADD CONSTRAINT FK_44859D55217BBB47 FOREIGN KEY (person_id) REFERENCES person (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE movie_painter ADD CONSTRAINT FK_28984BF68F93B6FC FOREIGN KEY (movie_id) REFERENCES movie (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE movie_painter ADD CONSTRAINT FK_28984BF6217BBB47 FOREIGN KEY (person_id) REFERENCES person (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE movie_editor ADD CONSTRAINT FK_35366CCF8F93B6FC FOREIGN KEY (movie_id) REFERENCES movie (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE movie_editor ADD CONSTRAINT FK_35366CCF217BBB47 FOREIGN KEY (person_id) REFERENCES person (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE movie_genre ADD CONSTRAINT FK_FD1229648F93B6FC FOREIGN KEY (movie_id) REFERENCES movie (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE movie_genre ADD CONSTRAINT FK_FD1229644296D31F FOREIGN KEY (genre_id) REFERENCES genre (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE command_queue_log ADD CONSTRAINT FK_52F3BAE9477B5BAE FOREIGN KEY (queue_id) REFERENCES command_queue (id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE person_translation DROP FOREIGN KEY FK_D9821AE72C2AC5D3');
        $this->addSql('ALTER TABLE movie_director DROP FOREIGN KEY FK_C266487D217BBB47');
        $this->addSql('ALTER TABLE movie_screenwriter DROP FOREIGN KEY FK_4369F569217BBB47');
        $this->addSql('ALTER TABLE movie_producer DROP FOREIGN KEY FK_4B92D251217BBB47');
        $this->addSql('ALTER TABLE movie_operator DROP FOREIGN KEY FK_B503C0C217BBB47');
        $this->addSql('ALTER TABLE movie_composer DROP FOREIGN KEY FK_44859D55217BBB47');
        $this->addSql('ALTER TABLE movie_painter DROP FOREIGN KEY FK_28984BF6217BBB47');
        $this->addSql('ALTER TABLE movie_editor DROP FOREIGN KEY FK_35366CCF217BBB47');
        $this->addSql('ALTER TABLE genre_translation DROP FOREIGN KEY FK_FF11F8A42C2AC5D3');
        $this->addSql('ALTER TABLE movie_genre DROP FOREIGN KEY FK_FD1229644296D31F');
        $this->addSql('ALTER TABLE movie DROP FOREIGN KEY FK_1D5EF26F5BB66C05');
        $this->addSql('ALTER TABLE country_translation DROP FOREIGN KEY FK_A1FE6FA42C2AC5D3');
        $this->addSql('ALTER TABLE movie_country DROP FOREIGN KEY FK_73E58B48F92F3E70');
        $this->addSql('ALTER TABLE movie_translation DROP FOREIGN KEY FK_9BEFC9982C2AC5D3');
        $this->addSql('ALTER TABLE transmission_queue DROP FOREIGN KEY FK_5B2663758F93B6FC');
        $this->addSql('ALTER TABLE movie_country DROP FOREIGN KEY FK_73E58B488F93B6FC');
        $this->addSql('ALTER TABLE movie_director DROP FOREIGN KEY FK_C266487D8F93B6FC');
        $this->addSql('ALTER TABLE movie_screenwriter DROP FOREIGN KEY FK_4369F5698F93B6FC');
        $this->addSql('ALTER TABLE movie_producer DROP FOREIGN KEY FK_4B92D2518F93B6FC');
        $this->addSql('ALTER TABLE movie_operator DROP FOREIGN KEY FK_B503C0C8F93B6FC');
        $this->addSql('ALTER TABLE movie_composer DROP FOREIGN KEY FK_44859D558F93B6FC');
        $this->addSql('ALTER TABLE movie_painter DROP FOREIGN KEY FK_28984BF68F93B6FC');
        $this->addSql('ALTER TABLE movie_editor DROP FOREIGN KEY FK_35366CCF8F93B6FC');
        $this->addSql('ALTER TABLE movie_genre DROP FOREIGN KEY FK_FD1229648F93B6FC');
        $this->addSql('ALTER TABLE command_queue_log DROP FOREIGN KEY FK_52F3BAE9477B5BAE');
        $this->addSql('DROP TABLE movie_translation');
        $this->addSql('DROP TABLE transmission_queue');
        $this->addSql('DROP TABLE country_translation');
        $this->addSql('DROP TABLE person');
        $this->addSql('DROP TABLE genre');
        $this->addSql('DROP TABLE genre_translation');
        $this->addSql('DROP TABLE file');
        $this->addSql('DROP TABLE person_translation');
        $this->addSql('DROP TABLE country');
        $this->addSql('DROP TABLE movie');
        $this->addSql('DROP TABLE movie_country');
        $this->addSql('DROP TABLE movie_director');
        $this->addSql('DROP TABLE movie_screenwriter');
        $this->addSql('DROP TABLE movie_producer');
        $this->addSql('DROP TABLE movie_operator');
        $this->addSql('DROP TABLE movie_composer');
        $this->addSql('DROP TABLE movie_painter');
        $this->addSql('DROP TABLE movie_editor');
        $this->addSql('DROP TABLE movie_genre');
        $this->addSql('DROP TABLE command_queue');
        $this->addSql('DROP TABLE command_queue_log');
        $this->addSql('DROP TABLE account');
    }
}
