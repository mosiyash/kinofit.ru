imports:
    - { resource: parameters.yml }
    - { resource: security.yml }
    - { resource: services.yml }
    - { resource: "@BackendBundle/Resources/config/services.yml" }
    - { resource: "@FrontendBundle/Resources/config/services.yml" }
    - { resource: "@KinopoiskBundle/Resources/config/services.yml" }
    - { resource: "@KinofitBundle/Resources/config/services.yml" }
    - { resource: "@AccountBundle/Resources/config/services.yml" }
    - { resource: "@FrontendMovieViewBundle/Resources/config/services.yml" }
    - { resource: "@FrontendMoviesListBundle/Resources/config/services.yml" }
    - { resource: "@FrontendTvSeriesListBundle/Resources/config/services.yml" }
    - { resource: "@FrontendHomepageBundle/Resources/config/services.yml" }
    - { resource: "@FrontendPersonViewBundle/Resources/config/services.yml" }
    - { resource: "@BackendMoviesListBundle/Resources/config/services.yml" }
    - { resource: "@BackendTorrentsListBundle/Resources/config/services.yml" }
    - { resource: "@TransmissionBundle/Resources/config/services.yml" }
    - { resource: "@VideoConverterBundle/Resources/config/services.yml" }
    - { resource: "@CronBundle/Resources/config/services.yml" }

# Put parameters here that don't need to change on each machine where the app is deployed
# http://symfony.com/doc/current/best_practices/configuration.html#application-related-configuration
parameters:
    locale: ru

framework:
    #esi:             ~
    translator:      { fallbacks: ["%locale%"] }
    secret:          "%secret%"
    router:
        resource: "%kernel.root_dir%/config/routing.yml"
        strict_requirements: ~
    form:            ~
    csrf_protection: ~
    validation:      { enable_annotations: true }
    #serializer:      { enable_annotations: true }
    templating:
        engines: ['twig']
    default_locale:  "%locale%"
    trusted_hosts:   ~
    trusted_proxies: ~
    session:
        # http://symfony.com/doc/current/reference/configuration/framework.html#handler-id
        handler_id:  session.handler.native_file
        save_path:   "%kernel.root_dir%/../var/sessions/%kernel.environment%"
    fragments:       ~
    http_method_override: true
    assets: ~

# Monolog Configuration
monolog:
    handlers:
        main:
            type: rotating_file
            path: "%kernel.logs_dir%/%kernel.environment%.log"
            level: notice
            max_files: 10
        syslog_handler:
            type: syslog
            level: error

# Twig Configuration
twig:
    debug:            "%kernel.debug%"
    strict_variables: "%kernel.debug%"
    globals:
        container: "@service_container"

# Doctrine Configuration
doctrine:
    dbal:
        driver:   pdo_mysql
        host:     "%database_host%"
        port:     "%database_port%"
        dbname:   "%database_name%"
        user:     "%database_user%"
        password: "%database_password%"
        charset:  utf8
        default_table_options:
            charset:  utf8
            collate: utf8_unicode_ci
#        charset: utf8mb4
#        default_table_options:
#            charset: utf8mb4
#            collate: utf8mb4_unicode_ci
        # charset:  UTF8
        # if using pdo_sqlite as your database driver:
        #   1. add the path in parameters.yml
        #     e.g. database_path: "%kernel.root_dir%/data/data.db3"
        #   2. Uncomment database_path in parameters.yml.dist
        #   3. Uncomment next line:
        #     path:     "%database_path%"
        types:
            ContentAutoStatusType: KinofitBundle\DBAL\Types\ContentAutoStatusType

    orm:
        auto_generate_proxy_classes: "%kernel.debug%"
        naming_strategy: doctrine.orm.naming_strategy.underscore
        auto_mapping: true

# Doctrine Migrations Configuration
doctrine_migrations:
    dir_name: "%kernel.root_dir%/DoctrineMigrations"
    namespace: Application\Migrations
    table_name: migration_versions
    name: Application Migrations

# Swiftmailer Configuration
swiftmailer:
    transport: "%mailer_transport%"
    host:      "%mailer_host%"
    username:  "%mailer_user%"
    password:  "%mailer_password%"
    spool:     { type: memory }

# Assetic Configuration
assetic:
    debug:          '%kernel.debug%'
    use_controller: '%kernel.debug%'
    filters:
        cssrewrite: ~
        scss: ~
    assets:
        font_awesome:
            inputs:
                - 'assets/bower/font-awesome/css/font-awesome.css'
            filters:
                - cssrewrite

# Menu Configuration
knp_menu:
    # use "twig: false" to disable the Twig extension and the TwigRenderer
    twig:
        template: KnpMenuBundle::menu.html.twig
    #  if true, enables the helper for PHP templates
    templating: false
    # the renderer to use, list is also available by default
    default_renderer: twig

# Paginator Configuration
knp_paginator:
    page_range: 5                      # default page range used in pagination control
    default_options:
        page_name: page                # page query parameter name
        sort_field_name: sort          # sort field query parameter name
        sort_direction_name: direction # sort direction query parameter name
        distinct: true                 # ensure distinct results, useful when ORM queries are using GROUP BY statements
    template:
#        pagination: KnpPaginatorBundle:Pagination:sliding.html.twig     # sliding pagination controls template
        sortable: KnpPaginatorBundle:Pagination:sortable_link.html.twig # sort link template

# Flysystem Configuration
oneup_flysystem:
    adapters:
        movies_posters:
            custom:
                service: kinofit.flysystem.movies_posters_adapter
        movies_videos:
            custom:
                service: kinofit.flysystem.movies_videos_adapter
    filesystems:
        movies_posters:
            adapter: movies_posters
            mount: movies_posters
        movies_videos:
            adapter: movies_videos
            mount: movies_videos

# Foundation Configuration
flob_foundation:
#    theme: { form: true, knp_menu: true, knp_paginator: true, pagerfanta: true }
    theme: { form: true, knp_menu: true, knp_paginator: true }
#    template:
#        form: 'YourBundle:YourFolder:formtemplate.html.twig'
#        breadcrumb: 'YourBundle:YourFolder:breadcrumbtemplate.html.twig'
#        knp_menu: 'YourBundle:YourFolder:menutemplate.html.twig'
#        knp_paginator: 'FlobFoundationBundle:Pagination:foundation_sliding.html.twig'
#        pagerfanta: 'YourPagerFantaTemplate'

# Liip Imagine Configuration
liip_imagine:
    resolvers:
        default:
            web_path: ~
    filter_sets:
        cache: ~
        movie_poster_thumb:
            data_loader: movies_posters_flysystem_data_loader
            quality: 80
            filters:
                thumbnail: { size: [136, 195], mode: inbound }
        movie_poster_thumb_large:
            data_loader: movies_posters_flysystem_data_loader
            quality: 80
            filters:
                thumbnail: { size: [220, 338], mode: inbound }
#    loaders:
#        movies_posters:
#            flysystem:
#                filesystem_service: oneup_flysystem.movies_posters_filesystem
#    data_loader: movies_posters_flysystem_data_loader

# OAuth2 Client Configuration
knpu_oauth2_client:
    clients:
        vkontakte:
            type: generic
            provider_class: J4k\OAuth2\Client\Provider\Vkontakte
            client_id: '%account.oauth.vkontakte.client_id%'
            client_secret: '%account.oauth.vkontakte.client_secret%'
            redirect_route: connect_vkontakte_check
            redirect_params: {}

heri_job_queue:
    enabled:            true
    max_messages:       1
#    process_timeout:    60
    queues:
      - kinopoisk-find-movies
      - kinopoisk-parse-movies
      - kinopoisk-parse-persons
      - kinofit-import-movies
      - kinofit-import-movies-posters
      - kinofit-import-persons
