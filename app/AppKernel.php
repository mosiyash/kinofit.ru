<?php

use FOS\RestBundle\FOSRestBundle;
use JMS\SerializerBundle\JMSSerializerBundle;
use SamJ\FractalBundle\SamJFractalBundle;
use Symfony\Component\Config\Loader\LoaderInterface;
use Symfony\Component\HttpKernel\Kernel;

class AppKernel extends Kernel
{
    public function registerBundles()
    {
        $bundles = [
            new Symfony\Bundle\FrameworkBundle\FrameworkBundle(),
            new Symfony\Bundle\SecurityBundle\SecurityBundle(),
            new Symfony\Bundle\TwigBundle\TwigBundle(),
            new Symfony\Bundle\MonologBundle\MonologBundle(),
            new Symfony\Bundle\SwiftmailerBundle\SwiftmailerBundle(),
            new Symfony\Bundle\AsseticBundle\AsseticBundle(),
            new Doctrine\Bundle\DoctrineBundle\DoctrineBundle(),
            new Doctrine\Bundle\MigrationsBundle\DoctrineMigrationsBundle(),
            new Sensio\Bundle\FrameworkExtraBundle\SensioFrameworkExtraBundle(),
            new Knp\Bundle\MenuBundle\KnpMenuBundle(),
            new Knp\DoctrineBehaviors\Bundle\DoctrineBehaviorsBundle(),
            new Knp\Bundle\PaginatorBundle\KnpPaginatorBundle(),
            new KnpU\OAuth2ClientBundle\KnpUOAuth2ClientBundle(),
            new Oneup\FlysystemBundle\OneupFlysystemBundle(),
            new Flob\Bundle\FoundationBundle\FlobFoundationBundle(),
            new Liip\ImagineBundle\LiipImagineBundle(),
            new Fresh\DoctrineEnumBundle\FreshDoctrineEnumBundle(),
            new FOSRestBundle(),
            new JMSSerializerBundle(),
            new SamJFractalBundle(),
            new Heri\Bundle\JobQueueBundle\HeriJobQueueBundle(),
            new AppBundle\AppBundle(),
            new BackendBundle\BackendBundle(),
            new FrontendBundle\FrontendBundle(),
            new KinopoiskBundle\KinopoiskBundle(),
            new KinofitBundle\KinofitBundle(),
            new AccountBundle\AccountBundle(),
            new FrontendMovieViewBundle\FrontendMovieViewBundle(),
            new FrontendMoviesListBundle\FrontendMoviesListBundle(),
            new FrontendTvSeriesListBundle\FrontendTvSeriesListBundle(),
            new FrontendHomepageBundle\FrontendHomepageBundle(),
            new FrontendPersonViewBundle\FrontendPersonViewBundle(),
            new BackendMoviesListBundle\BackendMoviesListBundle(),
            new BackendTorrentsListBundle\BackendTorrentsListBundle(),
            new TransmissionBundle\TransmissionBundle(),
            new League\Tactician\Bundle\TacticianBundle(),
            new VideoConverterBundle\VideoConverterBundle(),
            new CronBundle\CronBundle(),
        ];

        if (in_array($this->getEnvironment(), ['dev', 'test'], true)) {
            $bundles[] = new Symfony\Bundle\DebugBundle\DebugBundle();
            $bundles[] = new Symfony\Bundle\WebProfilerBundle\WebProfilerBundle();
            $bundles[] = new Sensio\Bundle\DistributionBundle\SensioDistributionBundle();
            $bundles[] = new Sensio\Bundle\GeneratorBundle\SensioGeneratorBundle();
            $bundles[] = new Doctrine\Bundle\FixturesBundle\DoctrineFixturesBundle();
        }

        return $bundles;
    }

    public function getRootDir()
    {
        return __DIR__;
    }

    public function getCacheDir()
    {
        return dirname(__DIR__).'/var/cache/'.$this->getEnvironment();
    }

    public function getLogDir()
    {
        return dirname(__DIR__).'/var/logs';
    }

    public function registerContainerConfiguration(LoaderInterface $loader)
    {
        $loader->load($this->getRootDir().'/config/config_'.$this->getEnvironment().'.yml');
    }
}
