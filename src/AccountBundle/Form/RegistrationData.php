<?php

namespace AccountBundle\Form;

use Symfony\Component\Validator\Constraints as Assert;

class RegistrationData
{
    /**
     * @var null|string
     * @Assert\NotBlank()
     * @Assert\Email()
     */
    private $email;

    /**
     * @var null|string
     * @Assert\NotBlank()
     */
    private $username;

    /**
     * @var null|string
     * @Assert\NotBlank()
     */
    private $password;

    /**
     * @return null|string
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * @param null|string $username
     *
     * @return RegistrationData
     */
    public function setUsername($username)
    {
        $this->username = $username;

        return $this;
    }

    /**
     * @return null|string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param null|string $password
     *
     * @return RegistrationData
     */
    public function setPassword($password)
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @return null|string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param null|string $email
     *
     * @return RegistrationData
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }
}
