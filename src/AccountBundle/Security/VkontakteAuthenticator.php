<?php

namespace AccountBundle\Security;

use AccountBundle\Entity\Account;
use AccountBundle\Repository\AccountRepository;
use Doctrine\ORM\EntityManager;
use J4k\OAuth2\Client\Provider\User;
use KnpU\OAuth2ClientBundle\Client\ClientRegistry;
use KnpU\OAuth2ClientBundle\Client\OAuth2Client;
use KnpU\OAuth2ClientBundle\Security\Authenticator\SocialAuthenticator;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\User\UserProviderInterface;

class VkontakteAuthenticator extends SocialAuthenticator
{
    /**
     * @var ClientRegistry
     */
    private $clientRegistry;

    /**
     * @var EntityManager
     */
    private $em;

    /**
     * @var RouterInterface
     */
    private $router;

    /**
     * VkontakteAuthenticator constructor.
     *
     * @param ClientRegistry  $clientRegistry
     * @param EntityManager   $em
     * @param RouterInterface $router
     */
    public function __construct(ClientRegistry $clientRegistry, EntityManager $em, RouterInterface $router)
    {
        $this->clientRegistry = $clientRegistry;
        $this->em             = $em;
        $this->router         = $router;
    }

    /**
     * {@inheritdoc}
     */
    public function getCredentials(Request $request)
    {
        if ($request->getPathInfo() != '/connect/vkontakte/check') {
            // don't auth
            return;
        }

        return $this->fetchAccessToken($this->getVkontakteClient());
    }

    /**
     * {@inheritdoc}
     */
    public function getUser($credentials, UserProviderInterface $accountProvider)
    {
        /** @var AccountRepository $accountRepository */
        $accountRepository = $this->em->getRepository(Account::class);

        /** @var User $vkontakteUser */
        $vkontakteUser = $this->getVkontakteClient()
            ->fetchUserFromToken($credentials);

        $email = $vkontakteUser->toArray()['email'];

        // 1) have they logged in with Vkontakte before? Easy!
        $existingUser = $accountRepository
            ->findOneBy(['vkontakteId' => $vkontakteUser->getId()]);
        if ($existingUser) {
            return $existingUser;
        }

        // 2) do we have a matching user by email?
        $account = $accountRepository
            ->findOneBy(['email' => $email]);

        // 3) Maybe you just want to "register" them by creating
        // a User object
        if ($account) {
            $account->setVkontakteId($vkontakteUser->getId());
        } else {
            $accountUsername = $vkontakteUser->getScreenName();
            if (!$accountUsername) {
                $accountUsername = $vkontakteUser->getNickname();
            }
            if (!$accountUsername) {
                $accountUsername = $vkontakteUser->getDomain();
            }

            $account = new Account();
            $account->setVkontakteId($vkontakteUser->getId());
            $account->setUsername($accountUsername);
            $account->setEmail($vkontakteUser->toArray()['email']);
        }

        $this->em->persist($account);
        $this->em->flush();

        return $account;
    }

    /**
     * @return OAuth2Client
     */
    private function getVkontakteClient()
    {
        return $this->clientRegistry->getClient('vkontakte');
    }

    /**
     * {@inheritdoc}
     */
    public function onAuthenticationFailure(Request $request, AuthenticationException $exception)
    {
        return null;
    }

    /**
     * {@inheritdoc}
     */
    public function onAuthenticationSuccess(Request $request, TokenInterface $token, $providerKey)
    {
        return new RedirectResponse($this->router->generate('homepage'));
    }

    /**
     * {@inheritdoc}
     */
    public function start(Request $request, AuthenticationException $authException = null)
    {
        return new Response();
    }
}
