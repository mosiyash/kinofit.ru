<?php

namespace AccountBundle\Controller;

use AccountBundle\Entity\Account;
use AccountBundle\Form\RegistrationType;
use AccountBundle\Repository\AccountRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{
    /**
     * @Route("/login", name="login")
     */
    public function loginAction()
    {
        $authenticationUtils = $this->get('security.authentication_utils');
        $error               = $authenticationUtils->getLastAuthenticationError();
        $lastUsername        = $authenticationUtils->getLastUsername();

        return $this->render('AccountBundle:Default:login.html.twig', [
            'last_username' => $lastUsername,
            'error'         => $error,
        ]);
    }

    /**
     * @Route("/register", name="register")
     */
    public function registerAction(Request $request)
    {
        $em = $this->get('doctrine.orm.entity_manager');

        /** @var AccountRepository $accountRepository */
        $accountRepository = $em->getRepository(Account::class);

        $form = $this->createForm(RegistrationType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $account = $accountRepository->findOneBy([
                'email'    => $form->get('email')->getData(),
                'username' => $form->get('username')->getData(),
            ]);
            if ($account) {
                $form->addError(new FormError('Username or email already exists'));
            }
            if ($form->isValid()) {
                $account = new Account();
                $account->setEmail($form->get('email')->getData());
                $account->setUsername($form->get('username')->getData());
                $account->setPassword($this->get('security.password_encoder')->encodePassword($account, $form->get('password')->getData()));

                $em->persist($account);
                $em->flush($account);
            }
        }

        return $this->render('AccountBundle:Default:register.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/connect/vkontakte", name="connect_vkontakte")
     */
    public function connectVkontakte()
    {
        return $this->get('knpu.oauth2.client.vkontakte')->redirect();
    }

    /**
     * @param Request $request
     * @Route("/connect/vkontakte/check", name="connect_vkontakte_check")
     */
    public function connectVkontakteCheck(Request $request)
    {
        $client = $this->get('knpu.oauth2.client.vkontakte');

        $user = $client->fetchUser();
        exit(dump($user));

        return new RedirectResponse($this->get('router')->generate('homepage'));
    }
}
