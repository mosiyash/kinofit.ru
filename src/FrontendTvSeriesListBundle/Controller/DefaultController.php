<?php

namespace FrontendTvSeriesListBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    /**
     * @Route("/tvseries", name="tvseries")
     */
    public function indexAction()
    {
        return $this->render('FrontendTvSeriesListBundle:Default:index.html.twig');
    }
}
