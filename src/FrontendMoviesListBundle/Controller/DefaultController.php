<?php

namespace FrontendMoviesListBundle\Controller;

use KinofitBundle\DBAL\Types\ContentAutoStatusType;
use KinofitBundle\Entity\Movie;
use KinofitBundle\Repository\MovieRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{
    /**
     * @param Request $request
     * @Route("/movies", name="movies")
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function moviesAction(Request $request)
    {
        $em        = $this->get('doctrine.orm.entity_manager');
        $paginator = $this->get('knp_paginator');

        /** @var MovieRepository $movieRepository */
        $movieRepository = $em->getRepository(Movie::class);

        $movieQueryBuilder = $movieRepository->filterBy(
            [
                'genre:id'   => $request->get('genre'),
                'movie:year' => $request->get('year'),
                'country:id' => $request->get('country'),
            ],
            $movieRepository
                ->createQueryBuilder('movie')
                ->where('movie.autoStatus = :movie_auto_status')
                ->setParameter('movie_auto_status', ContentAutoStatusType::STATUS_PUBLISHED)
        );

        $orderBy = null;
        if ($request->get('popularity')) {
            $movieQueryBuilder->orderBy('movie.popularity', 'DESC');
        } elseif ($request->get('kinopoiskRating')) {
            $movieQueryBuilder->orderBy('movie.kinopoiskRating', 'DESC');
        } elseif ($request->get('imdbRating')) {
            $movieQueryBuilder->orderBy('movie.imdbRating', 'DESC');
        }

        $movieQuery = $movieQueryBuilder->getQuery();

        $pagination = $paginator->paginate(
            $movieQuery,
            $request->query->getInt('page', 1),
            12,
            ['wrap-queries' => true]
        );

        return $this->render('FrontendMoviesListBundle:Default:movies.html.twig', [
            'pagination' => $pagination,
        ]);
    }
}
