<?php

namespace FrontendBundle\Menu;

use Knp\Menu\FactoryInterface;
use Knp\Menu\ItemInterface;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

class Builder implements ContainerAwareInterface
{
    use ContainerAwareTrait;

    public function headerMenu(FactoryInterface $factory, array $options): ItemInterface
    {
        $menu = $factory->createItem('root');
        $menu->addChild('Movies', ['route' => 'movies']);
        $menu->addChild('TV Series', ['route' => 'tvseries']);

        $community = $menu->addChild('Community');
        $community->addChild('Forum');
        $community->addChild('Orders');

        return $menu;
    }
}
