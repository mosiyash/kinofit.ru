<?php

namespace KinopoiskBundle\Command;

use KinofitBundle\Command\KinofitImportMoviesCommand;
use KinofitBundle\Command\KinofitImportMoviesPostersCommand;
use KinopoiskBundle\EntitySchema\MovieMeta;
use KinopoiskBundle\GuzzleHttp\Exception\CaptchaPresentedResponseException;
use KinopoiskBundle\GuzzleHttp\Exception\NotFoundRequestException;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Exception\RuntimeException;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DomCrawler\Crawler;

class KinopoiskParseMoviesCommand extends ContainerAwareCommand
{
    use GuzzleHttpAwareTrait;
    use KinopoiskHelper;

    const NAME       = 'kinopoisk:parse-movies';
    const QUEUE_NAME = 'kinopoisk-parse-movies';

    const OPTION_KINOPOISK_ID = 'kinopoisk-id';

    /**
     * @var string
     */
    private $filepath;

    private $headers = [
        MovieMeta::KEY_KINOPOISK_ID,
        MovieMeta::KEY_KINOPOISK_DESCRIPTION,
        MovieMeta::KEY_KINOPOISK_RATING,
        MovieMeta::KEY_IMDB_RATING,
        MovieMeta::KEY_KINOPOISK_FLAG_TV,
        MovieMeta::KEY_KINOPOISK_FLAG_VIDEO,
        MovieMeta::KEY_TITLE_RU,
        MovieMeta::KEY_TITLE_ORIGIN,
        MovieMeta::KEY_YEAR,
        MovieMeta::KEY_COUNTRIES,
        MovieMeta::KEY_TAGLINE,
        MovieMeta::KEY_DIRECTORS,
        MovieMeta::KEY_SCREENWRITERS,
        MovieMeta::KEY_PRODUCERS,
        MovieMeta::KEY_OPERATORS,
        MovieMeta::KEY_COMPOSERS,
        MovieMeta::KEY_PAINTERS,
        MovieMeta::KEY_EDITORS,
        MovieMeta::KEY_GENRES,
        MovieMeta::KEY_AGE_LIMIT,
    ];

    protected function configure()
    {
        $this
            ->setName(self::NAME)
            ->setDescription('Detailed parsing of movies on KinoPoisk.Ru')
            ->addOption(self::OPTION_KINOPOISK_ID, null, InputOption::VALUE_REQUIRED, 'KinoPoisk movie id', null)
        ;
    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        try {
            try {
                $response = $this->createRequest('GET', 'https://www.kinopoisk.ru/film/'.$input->getOption(self::OPTION_KINOPOISK_ID).'/');
            } catch (CaptchaPresentedResponseException $e) {
                $this->getContainer()->get('logger')->notice('Captcha is presented');

                return;
            } catch (NotFoundRequestException $e) {
                $this->getContainer()->get('logger')->notice('404 Not Found');

                return;
            }

            $crawler = new Crawler((string) $response->getBody());
            $data    = $this->extractMovieInfo($crawler);

            $this->saveToJsonFile($data);
            $this->addToKinofitImportMoviesQueue($data);

            $posterUrl = 'https://st.kp.yandex.net/images/film_big/'.$data[MovieMeta::KEY_KINOPOISK_ID].'.jpg';
            if ($posterUrl) {
                $this->addToKinofitImportMoviesPostersQueue($data[MovieMeta::KEY_KINOPOISK_ID], $posterUrl);
            }
        } catch (\Exception $e) {
            throw new RuntimeException($e);
        }
    }

    /**
     * @param Crawler $crawler
     *
     * @return array
     */
    private function extractMovieInfo(Crawler $crawler): array
    {
        $data = [];

        $data[MovieMeta::KEY_KINOPOISK_ID] = call_user_func(function () use ($crawler) {
            $selector = 'link[rel="canonical"]';
            $element = $crawler->filter($selector)->eq(0);

            return (int) preg_replace('#^.+?/(\d+?)/$#is', '\1', $element->attr('href'));
        });

        $data[MovieMeta::KEY_KINOPOISK_DESCRIPTION] = call_user_func(function () use ($crawler) {
            $selector = '.brand_words.film-synopsys';
            $elements = $crawler->filter($selector);
            if (!$elements->count()) {
                return null;
            }

            return $elements->eq(0)->text();
        });

        $data[MovieMeta::KEY_TITLE_RU] = call_user_func(function () use ($crawler) {
            $selector = 'h1.moviename-big';
            $element = $crawler->filter($selector)->eq(0);

            return preg_replace('#\s*\((тв|видео)\)\s*$#iu', '', $element->text());
        });

        $data[MovieMeta::KEY_TITLE_ORIGIN] = call_user_func(function () use ($crawler) {
            $selector = '#headerFilm > span[itemprop="alternativeHeadline"]';
            $elements = $crawler->filter($selector);
            if (!$elements->count()) {
                return null;
            }
            $titleOrigin = $elements->eq(0)->text();

            return ($titleOrigin === '') ? null : $titleOrigin;
        });

        $data[MovieMeta::KEY_KINOPOISK_RATING] = call_user_func(function () use ($crawler) {
            $selector = 'span.rating_ball';
            $elements = $crawler->filter($selector);
            if (!$elements->count()) {
                return null;
            }

            return (float) $elements->eq(0)->text();
        });

        $data[MovieMeta::KEY_IMDB_RATING] = call_user_func(function () use ($crawler) {
            $selector = '#block_rating > .block_2 > div:nth-child(2)';
            $elements = $crawler->filter($selector);
            if (!$elements->count()) {
                return null;
            }
            $imdb = (float) preg_replace('#^imdb:\s*([\d\.]+?)\s+.*$#i', '\1', $elements->eq(0)->text());

            return ($imdb > 0) ? $imdb : null;
        });

        $data[MovieMeta::KEY_KINOPOISK_FLAG_TV] = call_user_func(function () use ($crawler) {
            $selector = 'h1.moviename-big';
            $element = $crawler->filter($selector)->eq(0);

            return (bool) preg_match('#\s*\(тв\)\s*$#iu', $element->text());
        });

        $data[MovieMeta::KEY_KINOPOISK_FLAG_VIDEO] = call_user_func(function () use ($crawler) {
            $selector = 'h1.moviename-big';
            $element = $crawler->filter($selector)->eq(0);

            return (bool) preg_match('#\s*\(видео\)\s*$#iu', $element->text());
        });

        $data[MovieMeta::KEY_YEAR] = call_user_func(function () use ($crawler) {
            $selector = '#infoTable td.type:first-child:contains("год")';
            $elements = $crawler->filter($selector);
            if (!$elements->count()) {
                return null;
            }

            return (int) trim($elements->eq(0)->parents()->eq(0)->filter('td:nth-child(2)')->eq(0)->text());
        });

        $data[MovieMeta::KEY_COUNTRIES] = call_user_func(function () use ($crawler) {
            $selector = '#infoTable td.type:first-child:contains("страна")';
            $elements = $crawler->filter($selector);
            if (!$elements->count()) {
                return [];
            }
            $data = [];
            $elements->eq(0)->parents()->eq(0)->filter('td:nth-child(2)')->eq(0)->filter('a')->each(function (Crawler $node) use (&$data) {
                $pattern = '#^.+?%5Bcountry%5D/(\d+?)/$#';
                if (preg_match($pattern, $node->attr('href'), $matches)) {
                    $data[] = (int) $matches[1];
                }
            });

            return $data;
        });

        $data[MovieMeta::KEY_TAGLINE] = call_user_func(function () use ($crawler) {
            $selector = '#infoTable td.type:first-child:contains("слоган")';
            $elements = $crawler->filter($selector);
            if (!$elements->count()) {
                return null;
            }
            $tagline = $elements->eq(0)->parents()->eq(0)->filter('td:nth-child(2)')->eq(0)->text();
            if ($tagline === '-') {
                return null;
            }

            return mb_substr($tagline, 1, -1);
        });

        $peopleKeys = [
            MovieMeta::KEY_DIRECTORS     => 'режиссер',
            MovieMeta::KEY_SCREENWRITERS => 'сценарий',
            MovieMeta::KEY_PRODUCERS     => 'продюсер',
            MovieMeta::KEY_OPERATORS     => 'оператор',
            MovieMeta::KEY_COMPOSERS     => 'композитор',
            MovieMeta::KEY_PAINTERS      => 'художник',
            MovieMeta::KEY_EDITORS       => 'монтаж',
        ];

        foreach ($peopleKeys as $csvMetaKey => $contains) {
            $data[$csvMetaKey] = call_user_func(function () use ($crawler, $contains) {
                $selector = '#infoTable td.type:first-child:contains("'.$contains.'")';
                $elements = $crawler->filter($selector);
                if (!$elements->count()) {
                    return [];
                }
                $data = [];
                $elements->eq(0)->parents()->eq(0)->filter('td:nth-child(2)')->eq(0)->filter('a')->each(function (Crawler $node) use (&$data) {
                    $pattern = '#^.*?/name/(\d+?)/$#';
                    if (preg_match($pattern, $node->attr('href'), $matches)) {
                        $data[] = (int) $matches[1];
                    }
                });

                return $data;
            });
        }

        $data[MovieMeta::KEY_GENRES] = call_user_func(function () use ($crawler) {
            $selector = '#infoTable td.type:first-child:contains("жанр")';
            $elements = $crawler->filter($selector);
            if (!$elements->count()) {
                return [];
            }
            $data = [];
            $elements->eq(0)->parents()->eq(0)->filter('td:nth-child(2)')->eq(0)->filter('a')->each(function (Crawler $node) use (&$data) {
                $pattern = '#^.+?%5Bgenre%5D/(\d+?)/$#';
                if (preg_match($pattern, $node->attr('href'), $matches)) {
                    $data[] = (int) $matches[1];
                }
            });

            return $data;
        });

        $data[MovieMeta::KEY_AGE_LIMIT] = call_user_func(function () use ($crawler) {
            $selector = '#infoTable td.type:first-child:contains("возраст")';
            $elements = $crawler->filter($selector);
            if ($elements->count()) {
                $element = $elements->eq(0)->parents()->eq(0)->filter('td:nth-child(2)')->eq(0)->filter('div')->eq(0);

                return (int) preg_replace('#^ageLimit age(\d+?)$#', '\1', $element->attr('class'));
            }

            $selector = '#infoTable td.type:first-child:contains("рейтинг MPAA")';
            $elements = $crawler->filter($selector);
            if ($elements->count()) {
                $element = $elements->eq(0)->parents()->eq(0)->filter('td:nth-child(2)')->eq(0)->filter('a')->eq(0);

                return (int) preg_replace('#^.+?/rn/PG-(\d+)/$#', '\1', $element->attr('href'));
            }

            return null;
        });

        foreach ($data as $dataKey => $dataValue) {
            $this->getContainer()->get('logger')->debug('Set `'.$dataKey.'` to '.json_encode($dataValue, JSON_UNESCAPED_UNICODE));
        }

        return $data;
    }

    /**
     * @param array $data
     */
    private function addToKinofitImportMoviesQueue(array $data): void
    {
        $queue = $this->getContainer()->get('jobqueue');
        $queue->attach(KinofitImportMoviesCommand::QUEUE_NAME);
        $queue->push([
            'command'  => KinofitImportMoviesCommand::NAME,
            'argument' => [
                '--'.KinofitImportMoviesCommand::OPTION_KINOPOISK_ID  => $data[MovieMeta::KEY_KINOPOISK_ID],
                '--'.KinofitImportMoviesCommand::OPTION_JSON_FILEPATH => $this->getFilepath($data[MovieMeta::KEY_KINOPOISK_ID]),
                '--env'                                               => $this->getContainer()->get('kernel')->getEnvironment(),
            ],
        ]);
    }

    /**
     * @param int    $kinopoiskId
     * @param string $remoteUrl
     */
    private function addToKinofitImportMoviesPostersQueue(int $kinopoiskId, string $remoteUrl): void
    {
        $excludeRemoteUrls = [
            'https://st.kp.yandex.net/images/movies/poster_none.png',
        ];

        if (in_array($remoteUrl, $excludeRemoteUrls)) {
            return;
        }

        $queue = $this->getContainer()->get('jobqueue');
        $queue->attach(KinofitImportMoviesPostersCommand::QUEUE_NAME);
        $queue->push([
            'command'  => KinofitImportMoviesPostersCommand::NAME,
            'argument' => [
                '--'.KinofitImportMoviesPostersCommand::OPTION_KINOPOISK_ID => $kinopoiskId,
                '--'.KinofitImportMoviesPostersCommand::OPTION_POSTER_URL   => $remoteUrl,
                '--env'                                                     => $this->getContainer()->get('kernel')->getEnvironment(),
            ],
        ]);
    }

    /**
     * @param array $data
     *
     * @throws RuntimeException
     */
    private function saveToJsonFile(array $data): void
    {
        $filepath = $this->getFilepath($data[MovieMeta::KEY_KINOPOISK_ID]);
        if (!file_put_contents($filepath, json_encode([$data], JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT))) {
            throw new RuntimeException('Couldn\'t save contents to the file '.$filepath);
        }
    }

    /**
     * @return string
     */
    private function getDirectory(): string
    {
        $dir = $this->getContainer()->getParameter('kinopoisk.json_dir').'/parse-movies/'.mb_substr(time(), 0, 5).'/'.mb_substr(time(), 5, 3).'/'.mb_substr(time(), 8);
        if (!file_exists($dir)) {
            mkdir($dir, 0777, true);
        }

        return $dir;
    }

    /**
     * @param int $kinopoiskId
     *
     * @return string
     */
    private function getFilepath(int $kinopoiskId): string
    {
        return $this->getDirectory().'/'.$kinopoiskId.'.json';
    }
}
