<?php

namespace KinopoiskBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Exception\RuntimeException;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class KinopoiskFindGenresCommand extends ContainerAwareCommand
{
    const NAME = 'kinopoisk:find-genres';

    use GuzzleHttpAwareTrait;
    use KinopoiskHelper;

    protected function configure(): void
    {
        $this
            ->setName(self::NAME)
            ->setDescription('Find genres on KinoPoisk.ru')
        ;
    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     *
     * @throws RuntimeException
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        // Парсить жанры в прямом смысле слова не имеет смысла, так как их немного.
        $genres = [
            1750 => 'аниме',
            22   => 'биография',
            3    => 'боевик',
            13   => 'вестерн',
            19   => 'военный',
            17   => 'детектив',
            456  => 'детский',
            20   => 'для взрослых',
            12   => 'документальный',
            8    => 'драма',
            27   => 'игра',
            23   => 'история',
            6    => 'комедия',
            1747 => 'концерт',
            15   => 'короткометражка',
            16   => 'криминал',
            7    => 'мелодрама',
            21   => 'музыка',
            14   => 'мультфильм',
            9    => 'мюзикл',
            28   => 'новости',
            10   => 'приключения',
            25   => 'реальное ТВ',
            11   => 'семейный',
            24   => 'спорт',
            26   => 'ток-шоу',
            4    => 'триллер',
            1    => 'ужасы',
            2    => 'фантастика',
            18   => 'фильм-нуар',
            5    => 'фэнтези',
            1751 => 'церемония',
        ];

        $jsonDir = $this->getContainer()->getParameter('kinopoisk.json_dir').'/find-genres';
        if (!file_exists($jsonDir)) {
            mkdir($jsonDir, 0777, true);
        }
        $jsonFile = $jsonDir.'/genres.json';
        if (file_exists($jsonFile)) {
            unlink($jsonFile);
        }

        $data = [];

        foreach ($genres as $kinopoiskId => $titleRu) {
            $data[] = [
                'kinopoisk_id' => $kinopoiskId,
                'title_ru'     => $titleRu,
            ];
        }

        if (!file_put_contents($jsonFile, json_encode($data, JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT))) {
            throw new RuntimeException(sprintf('Couldn\'t write contents to %s', $jsonFile));
        }
    }
}
