<?php

namespace KinopoiskBundle\Command;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Psr7\Request;
use KinopoiskBundle\GuzzleHttp\Exception\CaptchaPresentedResponseException;
use KinopoiskBundle\GuzzleHttp\Exception\NotFoundRequestException;
use Psr\Http\Message\ResponseInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * @method ContainerInterface getContainer
 * @target \Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand
 */
trait GuzzleHttpAwareTrait
{
    /**
     * @return Client
     */
    protected function getClient(): Client
    {
        return $this->getContainer()->get('kinopoisk.client');
    }

    /**
     * @param string $method
     * @param string $url
     * @param array  $options
     *
     * @throws CaptchaPresentedResponseException
     * @throws NotFoundRequestException
     *
     * @return ResponseInterface
     */
    protected function createRequest(string $method, string $url, array $options = []): ResponseInterface
    {
        $this->getContainer()->get('logger')->debug('Requesting '.$url);

        $response = $this->loadFromCache($method, $url);

        if ($response) {
            $this->getContainer()->get('logger')->debug('Response loaded from cache');

            return $response;
        }

        if ($this->isUseProxy() && !array_key_exists('proxy', $options)) {
            $options['proxy']['https'] = $this->getContainer()->get('kinopoisk.proxy_header')->getRandomHttpsProxy();
        }

        try {
            $request  = new Request($method, $url);
            $response = $this->toUTF8($this->getClient()->send($request, $options));
            if ($this->isBannedByKinopoiskResponse($response)) {
                $this->getContainer()->get('logger')->warning('Banned by KinoPoisk firewall');
                if ($this->isUseProxy()) {
                    $this->banProxies($options['proxy'], $url);
                    $this->removeProxyOption($options, $url);
                }

                return $this->createRequest($method, $url, $options);
            } elseif ($this->isPresentedCaptchaByKinopoiskResponse($response)) {
                $this->getContainer()->get('logger')->warning('KinoPoisk return captcha response');
                if ($this->isUseProxy()) {
                    $this->banProxies($options['proxy'], $url);
                    $this->removeProxyOption($options, $url);
                }

                throw new CaptchaPresentedResponseException();
            }
        } catch (RequestException $e) {
            if (mb_strpos($e->getMessage(), '404 Not Found') !== false) {
                throw new NotFoundRequestException($e->getMessage(), $request);
            } else {
                $this->getContainer()->get('logger')->warning(sprintf(
                    '%s: %s at %s line %s',
                    get_class($e),
                    $e->getMessage(),
                    $e->getFile(),
                    $e->getLine()
                ));
                if ($this->isUseProxy()) {
                    $this->banProxies($options['proxy'], $url);
                    $this->removeProxyOption($options, $url);
                }

                return $this->createRequest($method, $url, $options);
            }
        }

        $this->putToCache($method, $url, $response);

        return $this->loadFromCache($method, $url);
    }

    /**
     * @param array  $options
     * @param string $url
     */
    private function removeProxyOption(array &$options, string $url): void
    {
        $urlSchema = parse_url($url, PHP_URL_SCHEME);
        $cache     = $this->getContainer()->get('cache.app');

        $cache->deleteItem(
            $this->getContainer()
                ->get('kinopoisk.proxy_header')
                ->{'get'.ucfirst($urlSchema).'ProxyCacheKey'}()
        );

        unset($options['proxy']);
    }

    /**
     * @param array  $proxies
     * @param string $url
     */
    private function banProxies(array $proxies, string $url): void
    {
        $cache     = $this->getContainer()->get('cache.app');
        $urlSchema = parse_url($url, PHP_URL_SCHEME);

        foreach ($proxies as $proxySchema => $schema) {
            if ($urlSchema === $proxySchema) {
                $this->getContainer()
                    ->get('kinopoisk.proxy_header')
                    ->banProxy($schema);

                $cacheKey = $this->getContainer()->get('kinopoisk.proxy_header')->{'get'.ucfirst($proxySchema).'ProxyCacheKey'}();
                $cache->deleteItem($cacheKey);
            }
        }
    }

    /**
     * @param string $method
     * @param string $url
     *
     * @return null|ResponseInterface
     */
    private function loadFromCache(string $method, string $url): ? ResponseInterface
    {
        $cache    = $this->getContainer()->get('kinopoisk.client.request_cache');
        $cacheKey = 'Kinopoisk.Request.'.md5($method.$url);
        $item     = $cache->getItem($cacheKey);
        $data     = $item->get();

        if (!$data) {
            return null;
        }

        $data = unserialize($data);
        \Assert\that($data)->isArray();
        \Assert\that($data)->keyExists('object');
        \Assert\that($data['object'])->isInstanceOf(ResponseInterface::class);
        \Assert\that($data)->keyExists('body');
        \Assert\that($data['body'])->string();

        /** @var ResponseInterface $response */
        $response = $data['object'];
        \Assert\that($response)->isInstanceOf(ResponseInterface::class);

        $response = $response->withBody(\GuzzleHttp\Psr7\stream_for($data['body']));

        return $response;
    }

    /**
     * @param string            $method
     * @param string            $url
     * @param ResponseInterface $response
     */
    private function putToCache(string $method, string $url, ResponseInterface $response) : void
    {
        $cache    = $this->getContainer()->get('kinopoisk.client.request_cache');
        $cacheKey = 'Kinopoisk.Request.'.md5($method.$url);
        $item     = $cache->getItem($cacheKey);

        $data = serialize([
            'object' => $response,
            'body'   => (string) $response->getBody(),
        ]);

        $item->set($data);
        $item->expiresAfter(60 * 60 * 24 * 360);
        $cache->save($item);
    }

    /**
     * @param ResponseInterface $response
     *
     * @return bool
     */
    private function isBannedByKinopoiskResponse(ResponseInterface $response): bool
    {
        $response = $this->toUTF8($response);

        return preg_match('/Если.+вы.+видите.+эту.+страницу.+значит.+с.+вашего.+IP.+адреса.+поступило.+необычно.+много.+запросов/is', (string) $response->getBody());
    }

    /**
     * @param ResponseInterface $response
     *
     * @return bool
     */
    private function isPresentedCaptchaByKinopoiskResponse(ResponseInterface $response): bool
    {
        $response = $this->toUTF8($response);

        return preg_match('/Всего.+несколько.+секунд.+и.+вы.+сможете.+продолжить.+поиск/is', (string) $response->getBody());
    }

    /**
     * @param ResponseInterface $response
     *
     * @return ResponseInterface
     */
    private function toUTF8(ResponseInterface $response): ResponseInterface
    {
        $charset = mb_detect_encoding((string) $response->getBody());

        if (mb_strtoupper($charset) !== 'UTF-8') {
            $response->withHeader('Content-Type', 'text/html; charset=UTF-8');
            $response = $response->withBody(mb_convert_encoding((string) $response->getBody(), 'UTF-8', $charset));
        }

        return $response;
    }

    /**
     * @return bool
     */
    private function isUseProxy(): bool
    {
        return (bool) $this->getContainer()->getParameter('kinopoisk.guzzle_use_proxy');
    }
}
