<?php

namespace KinopoiskBundle\Command;

use AppBundle\ContainerAwareTrait;
use KinofitBundle\Command\KinofitImportPersonsCommand;
use KinopoiskBundle\EntitySchema\PersonMeta;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Exception\RuntimeException;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DomCrawler\Crawler;

class KinopoiskParsePersonsCommand extends ContainerAwareCommand
{
    use GuzzleHttpAwareTrait;
    use KinopoiskHelper;
    use ContainerAwareTrait;

    const NAME       = 'kinopoisk:parse-persons';
    const QUEUE_NAME = 'kinopoisk-parse-persons';

    const OPTION_KINOPOISK_ID = 'kinopoisk-id';

    protected function configure()
    {
        $this
            ->setName(self::NAME)
            ->setDescription('Detailed parsing of persons on KinoPoisk.Ru')
            ->addOption(self::OPTION_KINOPOISK_ID, null, InputOption::VALUE_REQUIRED, 'KinoPoisk person id', null)
        ;
    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $url      = 'https://www.kinopoisk.ru/name/'.$input->getOption(self::OPTION_KINOPOISK_ID).'/';
        $response = $this->createRequest('GET', $url);
        $crawler  = new Crawler((string) $response->getBody());
        $data     = $this->extractPersonInfo($crawler);

        $this->saveToJsonFile($data);
        $this->addToKinofitImportPersonsQueue($data);
    }

    /**
     * @param Crawler $crawler
     *
     * @return array
     */
    private function extractPersonInfo(Crawler $crawler): array
    {
        $data = [];

        $data[PersonMeta::KEY_KINOPOISK_ID] = call_user_func(function () use ($crawler) {
            $selector = 'link[rel="canonical"]';
            $element = $crawler->filter($selector)->eq(0);

            return (int) preg_replace('#^.+?/(\d+?)/$#is', '\1', $element->attr('href'));
        });

        $data[PersonMeta::KEY_FIRSTNAME_EN] = call_user_func(function () use ($crawler) {
            $selector = '#headerPeople > span';
            $element = $crawler->filter($selector)->eq(0);

            try {
                $value = trim(preg_replace('#^\s*([^\s]+?)\s+.+?\s*$#iu', '\1', $element->text()));
            } catch (\InvalidArgumentException $e) {
                return null;
            }

            return ($value === '') ? null : $value;
        });

        $data[PersonMeta::KEY_FIRSTNAME_RU] = call_user_func(function () use ($crawler) {
            $selector = 'h1.moviename-big';
            $element = $crawler->filter($selector)->eq(0);
            $value = trim(preg_replace('#^\s*([^\s]+?)\s+.+?\s*$#iu', '\1', $element->text()));

            return ($value === '') ? null : $value;
        });

        $data[PersonMeta::KEY_LASTNAME_EN] = call_user_func(function () use ($crawler) {
            $selector = '#headerPeople > span';
            $element = $crawler->filter($selector)->eq(0);

            try {
                $parts = preg_split('/\s+/', trim($element->text()));
            } catch (\InvalidArgumentException $e) {
                return null;
            }

            $value = end($parts);

            return ($value === '') ? null : $value;
        });

        $data[PersonMeta::KEY_LASTNAME_RU] = call_user_func(function () use ($crawler) {
            $selector = 'h1.moviename-big';
            $element = $crawler->filter($selector)->eq(0);
            $parts = preg_split('/\s+/', trim($element->text()));
            $value = end($parts);

            return ($value === '') ? null : $value;
        });

        $data[PersonMeta::KEY_PATRONYMIC_EN] = null;
        $data[PersonMeta::KEY_PATRONYMIC_RU] = null;

        foreach ($data as $dataKey => $dataValue) {
            $this->getContainer()->get('logger')->debug('Set `'.$dataKey.'` to '.json_encode($dataValue, JSON_UNESCAPED_UNICODE));
        }

        return $data;
    }

    /**
     * @param array $data
     *
     * @throws RuntimeException
     */
    private function saveToJsonFile(array $data) : void
    {
        $filepath = $this->getFilepath($data[PersonMeta::KEY_KINOPOISK_ID]);
        if (!file_put_contents($filepath, json_encode([$data], JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT))) {
            throw new RuntimeException('Couldn\'t save contents to the file '.$filepath);
        }
    }

    /**
     * @return string
     */
    private function getDirectory(): string
    {
        $dir = $this->getContainer()->getParameter('kinopoisk.json_dir').'/parse-persons/'.mb_substr(time(), 0, 5).'/'.mb_substr(time(), 5, 3).'/'.mb_substr(time(), 8);
        if (!file_exists($dir)) {
            mkdir($dir, 0777, true);
        }

        return $dir;
    }

    /**
     * @param int $kinopoiskId
     *
     * @return string
     */
    private function getFilepath(int $kinopoiskId): string
    {
        return $this->getDirectory().'/'.$kinopoiskId.'.json';
    }

    /**
     * @param array $data
     */
    private function addToKinofitImportPersonsQueue(array $data): void
    {
        $queue = $this->getContainer()->get('jobqueue');
        $queue->attach(KinofitImportPersonsCommand::QUEUE_NAME);
        $queue->push([
            'command'  => KinofitImportPersonsCommand::NAME,
            'argument' => [
                '--'.KinofitImportPersonsCommand::OPTION_KINOPOISK_ID  => $data[PersonMeta::KEY_KINOPOISK_ID],
                '--'.KinofitImportPersonsCommand::OPTION_JSON_FILEPATH => $this->getFilepath($data[PersonMeta::KEY_KINOPOISK_ID]),
                '--env'                                                => $this->getContainer()->get('kernel')->getEnvironment(),
            ],
        ]);
    }
}
