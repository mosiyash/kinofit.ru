<?php

namespace KinopoiskBundle\Command;

use KinofitBundle\Command\KinofitImportMoviesCommand;
use KinopoiskBundle\EntitySchema\MovieMeta;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Exception\RuntimeException;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DomCrawler\Crawler;

class KinopoiskFindMoviesCommand extends ContainerAwareCommand
{
    use GuzzleHttpAwareTrait;
    use KinopoiskHelper;

    const NAME                  = 'kinopoisk:find-movies';
    const QUEUE_NAME            = 'kinopoisk-find-movies';
    const OPTION_NAVIGATOR_PAGE = 'navigator-page';

    protected function configure()
    {
        $this
            ->setName(self::NAME)
            ->setDescription('Find movies from Navigator of KinoPoisk.ru')
            ->addOption(self::OPTION_NAVIGATOR_PAGE, null, InputOption::VALUE_OPTIONAL, 'Page number of Navigator results on https://www.kinopoisk.ru/top/navigator/*')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $pageNum = $maxPageNum = $input->getOption(self::OPTION_NAVIGATOR_PAGE)
            ? (int) $input->getOption(self::OPTION_NAVIGATOR_PAGE)
            : 1;

        $url = str_replace(
            '/page/1/',
            '/',
            implode('', [
                'https://www.kinopoisk.ru/top/navigator/',
                'm_act%5Bnum_vote%5D/10/m_act%5Brating%5D/1:/m_act%5Bis_film%5D/',
                'on/m_act%5Bis_mult%5D/on/order/year/page/'.$pageNum.'/#results',
            ])
        );

        $response = $this->createRequest('GET', $url);
        $crawler  = new Crawler((string) $response->getBody());

        if ($pageNum === 1 && $input->getOption(self::OPTION_NAVIGATOR_PAGE) === null) {
            $maxPageNum = preg_replace(
                '/^.+?\/page\/(\d+)?(\/.*)$/is', '\1',
                $crawler->filter('div.navigator > ul.list > li:last-child > a')->eq(0)->attr('href')
            );
            \Assert\that($maxPageNum)->digit()->notEmpty();
            $maxPageNum = (int) $maxPageNum;
        }

        if ($input->getOption(self::OPTION_NAVIGATOR_PAGE) !== null) {
            $crawler->filter('#itemList > .item')->each(function (Crawler $item) {
                $data = $this->extractMovieInfo($item);
                if (is_array($data)) {
                    $this->saveToJsonFile($data);
                    $this->addToKinofitImportMoviesQueue($data);
                    $this->addToKinopoiskParseMoviesQueue($data);
                }
            });
        }

        if ($maxPageNum > $pageNum) {
            for ($navigatorPage = $maxPageNum; $navigatorPage >= 1; --$navigatorPage) {
                $this->addToSelfQueue($navigatorPage);
            }
        }
    }

    /**
     * @param Crawler $crawler
     *
     * @return array|null
     */
    private function extractMovieInfo(Crawler $crawler): ? array
    {
        $kinopoiskId = preg_replace(
            '/^.+\/(\d+?)\/$/is',
            '\1',
            $crawler->filter('.info > .name > a')->eq(0)->attr('href')
        );
        \Assert\that($kinopoiskId)->digit()->notEmpty();
        $kinopoiskId = (int) $kinopoiskId;

        $titleRuFull = $crawler->filter('.info > .name > a')->eq(0)->text();
        \Assert\that($titleRuFull)->string()->notEmpty();

        if ($this->isThatTitleHasTvSeriesFlag($titleRuFull)) {
            $this->getContainer()->get('logger')->debug('The name "'.$titleRuFull.'" has a \'TV Series\' flag. Skipped');

            return null;
        }

        $titleRu = $this->getCleanTitleRu($titleRuFull);
        \Assert\that($titleRu)->string()->notEmpty();

        $kinopoiskFlagTv = (int) $this->isThatTitleHasTvFlag($titleRuFull);
        \Assert\that($kinopoiskFlagTv)->integer()->range(0, 1);

        $kinopoiskFlagVideo = (int) $this->isThatTitleHasVideoFlag($titleRuFull);
        \Assert\that($kinopoiskFlagVideo)->integer()->range(0, 1);

        $titleOrigin = preg_replace(
            '/\s*\(\d{4}\).*$/is',
            '',
            $crawler->filter('.info > .name > span')->eq(0)->text()
        );
        if ($titleOrigin === '') {
            $titleOrigin = null;
        }
        \Assert\that($titleOrigin)->nullOr()->string();

        return [
            MovieMeta::KEY_KINOPOISK_ID => $kinopoiskId,
            MovieMeta::KEY_TITLE_RU     => $titleRu,
            MovieMeta::KEY_TITLE_ORIGIN => $titleOrigin,
        ];
    }

    /**
     * @param array $data
     *
     * @throws RuntimeException
     */
    private function saveToJsonFile(array $data) : void
    {
        $filepath = $this->getFilepath($data[MovieMeta::KEY_KINOPOISK_ID]);
        if (!file_put_contents($filepath, json_encode([$data], JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT))) {
            throw new RuntimeException('Couldn\'t save contents to the file '.$filepath);
        }
    }

    /**
     * @param array $data
     */
    private function addToKinofitImportMoviesQueue(array $data): void
    {
        $queue = $this->getContainer()->get('jobqueue');
        $queue->attach(KinofitImportMoviesCommand::QUEUE_NAME);
        $queue->push([
            'command'  => KinofitImportMoviesCommand::NAME,
            'argument' => [
                '--'.KinofitImportMoviesCommand::OPTION_KINOPOISK_ID  => $data[MovieMeta::KEY_KINOPOISK_ID],
                '--'.KinofitImportMoviesCommand::OPTION_JSON_FILEPATH => $this->getFilepath($data[MovieMeta::KEY_KINOPOISK_ID]),
                '--env'                                               => $this->getContainer()->get('kernel')->getEnvironment(),
            ],
        ]);
    }

    /**
     * @param array $data
     */
    private function addToKinopoiskParseMoviesQueue(array $data): void
    {
        $queue = $this->getContainer()->get('jobqueue');
        $queue->attach(KinopoiskParseMoviesCommand::QUEUE_NAME);
        $queue->push([
            'command'  => KinopoiskParseMoviesCommand::NAME,
            'argument' => [
                '--'.KinopoiskParseMoviesCommand::OPTION_KINOPOISK_ID => $data[MovieMeta::KEY_KINOPOISK_ID],
                '--env'                                               => $this->getContainer()->get('kernel')->getEnvironment(),
            ],
        ]);
    }

    /**
     * @param int $navigatorPage
     */
    private function addToSelfQueue(int $navigatorPage): void
    {
        $queue = $this->getContainer()->get('jobqueue');
        $queue->attach(self::QUEUE_NAME);
        $queue->push([
            'command'  => self::NAME,
            'argument' => [
                '--'.self::OPTION_NAVIGATOR_PAGE => $navigatorPage,
                '--env'                          => $this->getContainer()->get('kernel')->getEnvironment(),
            ],
        ]);
    }

    /**
     * @return string
     */
    private function getDirectory(): string
    {
        $dir = $this->getContainer()->getParameter('kinopoisk.json_dir').'/find-movies/'.mb_substr(time(), 0, 5).'/'.mb_substr(time(), 5, 3).'/'.mb_substr(time(), 8);
        if (!file_exists($dir)) {
            mkdir($dir, 0777, true);
        }

        return $dir;
    }

    /**
     * @param int $kinopoiskId
     *
     * @return string
     */
    private function getFilepath(int $kinopoiskId): string
    {
        return $this->getDirectory().'/'.$kinopoiskId.'.json';
    }
}
