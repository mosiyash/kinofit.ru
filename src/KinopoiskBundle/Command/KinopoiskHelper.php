<?php

namespace KinopoiskBundle\Command;

trait KinopoiskHelper
{
    /**
     * @param string $title
     *
     * @return string
     */
    protected function getCleanTitleRu(string $title): string
    {
        if (preg_match('/\((ТВ|видео)\)$/is', $title)) {
            $title = preg_replace('/^(.+?)\s+\((ТВ|видео)\)$/is', '\1', $title);
        }
        \Assert\that($title)->string()->notEmpty();

        return $title;
    }

    /**
     * @param string $title
     *
     * @return bool
     */
    protected function isThatTitleHasTvFlag(string $title): bool
    {
        return preg_match('/\(ТВ\)$/is', $title);
    }

    /**
     * @param string $title
     *
     * @return bool
     */
    protected function isThatTitleHasVideoFlag(string $title): bool
    {
        return preg_match('/\(видео\)$/is', $title);
    }

    /**
     * @param string $title
     *
     * @return bool
     */
    protected function isThatTitleHasTvSeriesFlag(string $title): bool
    {
        return preg_match('/\(сериал\)$/is', $title);
    }
}
