<?php

namespace KinopoiskBundle\GuzzleHttp\Exception;

class CaptchaPresentedResponseException extends \HttpResponseException
{
}
