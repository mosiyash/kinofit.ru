<?php

namespace KinopoiskBundle\GuzzleHttp\Exception;

use GuzzleHttp\Exception\RequestException;

class NotFoundRequestException extends RequestException
{
}
