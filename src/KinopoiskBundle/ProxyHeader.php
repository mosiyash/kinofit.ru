<?php

namespace KinopoiskBundle;

use GuzzleHttp\Client;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\DomCrawler\Crawler;

final class ProxyHeader
{
    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * @var array
     */
    private $proxyList = [];

    /**
     * ProxyHeader constructor.
     *
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;

        $this->updateProxyList();
    }

    /**
     * @return ContainerInterface
     */
    public function getContainer(): ContainerInterface
    {
        return $this->container;
    }

    /**
     * @return array
     */
    public function getProxyList(): array
    {
        return $this->proxyList;
    }

    /**
     * @param bool $forced
     *
     * @throws \RuntimeException
     */
    public function updateProxyList($forced = false): void
    {
        $needToUpdate      = $forced;
        $proxyListFilepath = $this->getContainer()->getParameter('kinopoisk.proxy_list_file');
        $proxyListTtl      = $this->getContainer()->getParameter('kinopoisk.proxy_list_ttl');

        if (!$needToUpdate) {
            if (!file_exists($proxyListFilepath)) {
                if (!file_exists(dirname($proxyListFilepath))) {
                    mkdir(dirname($proxyListFilepath), 0777, true);
                }
                $needToUpdate = true;
            } elseif (filemtime($proxyListFilepath) < time() - $proxyListTtl) {
                $needToUpdate = true;
            }
        }

        if ($needToUpdate) {
            $guzzle      = new Client();
            $currentPage = $countResultPages = 1;

            $proxyList = [
                'h' => [],
                's' => [],
                5   => [],
            ];

            while ($currentPage <= $countResultPages) {
                $url = sprintf(
                    'http://hideme.ru/proxy-list/?type=hs5&anon=4%s#list',
                    $currentPage > 1
                        ? '&start='.($currentPage * 64 - 64)
                        : ''
                );

                $this->getContainer()->get('logger')->debug('Requesting '.$url);

                $response = $guzzle->request('GET', $url);
                $crawler  = new Crawler((string) $response->getBody());

                if ($currentPage === 1) {
                    $countResultPages = call_user_func(function () use ($crawler) {
                        try {
                            $count = $crawler->filter('.proxy__pagination > ul > li:last-child > a')->first()->text();
                            \Assert\that($count)->digit()->notEmpty();

                            return (int) $count;
                        } catch (\InvalidArgumentException $e) {
                            return 1;
                        }
                    });
                }

                $crawler->filter('table.proxy__t > tbody > tr')->each(function (Crawler $tr, $i) use (&$proxyList) {
                    $types = array_values(
                        array_filter(
                            preg_split('/\s*,\s*/', str_replace(['HTTPS', 'HTTP', 'SOCKS5'], ['s', 'h', 5], $tr->filter('td')->eq(4)->text())),
                            function ($value) {
                                return in_array($value, ['h', 's', 5]);
                            }
                        )
                    );
                    foreach ($types as $type) {
                        $proxyList[$type][] = [
                            'ip'    => $tr->filter('td')->eq(0)->text(),
                            'port'  => $tr->filter('td')->eq(1)->text(),
                            'types' => $types,
                        ];
                    }
                });

                ++$currentPage;
            }

            $res = file_put_contents(
                $proxyListFilepath,
                "<?php\n\nreturn ".var_export($proxyList, true).";\n"
            );

            if (!$res) {
                throw new \RuntimeException(sprintf('Couldn\'t write contents to %s', $proxyListFilepath));
            }
        }

        $this->proxyList = require_once $proxyListFilepath;
    }

    /**
     * @return string
     */
    public function getHttpProxyCacheKey(): string
    {
        return 'ProxyHeader.http';
    }

    /**
     * @return string
     */
    public function getHttpsProxyCacheKey(): string
    {
        return 'ProxyHeader.https';
    }

    /**
     * @return string
     */
    public function getSocks5ProxyCacheKey(): string
    {
        return 'ProxyHeader.socks5';
    }

    /**
     * @param string $schema
     *
     * @return bool
     */
    public function isBannedProxy(string $schema): bool
    {
        //        $cache    = $this->getContainer()->get('cache.app');
//        $cacheKey = 'ProxyHeader.banned.'.md5($schema);
//        $proxy = $cache->getItem($cacheKey);

//        return $proxy->isHit();

        return false;
    }

    /**
     * @param string $schema
     */
    public function banProxy(string $schema): void
    {
        //        $cache    = $this->getContainer()->get('cache.app');
//        $cacheKey = 'ProxyHeader.banned.'.md5($schema);
//        $proxy = $cache->getItem($cacheKey);

//        $proxy->set((new \DateTime())->format(\DateTime::ISO8601));
//        $proxy->expiresAfter($this->getContainer()->getParameter('kinopoisk.proxy_header.banned_ttl'));
//        $cache->save($proxy);

//        $this->getContainer()->get('logger')->debug(sprintf('Proxy %s marked as banned', $schema));

        return;
    }

    /**
     * @return string
     */
    public function getRandomHttpProxy(): string
    {
        $cache    = $this->getContainer()->get('cache.app');
        $cacheKey = $this->getHttpProxyCacheKey();
        $proxy    = $cache->getItem($cacheKey);

        if ($proxy->isHit()) {
            $proxySchema = $proxy->get();
        } else {
            \Assert\that($this->proxyList)->keyExists('h');
            \Assert\that($this->proxyList['h'])->isArray();

            $key  = rand(0, count($this->proxyList['h']) - 1);
            $item = $this->proxyList['h'][$key];

            \Assert\that($item)->isArray()->keyExists('ip')->keyExists('port');
            \Assert\that($item['ip'])->string()->ip();
            \Assert\that($item['port'])->digit();

            $proxySchema = $item['ip'].':'.$item['port'];

            $proxy->set($proxySchema);
            $cache->save($proxy);
        }

        if ($this->isBannedProxy($proxySchema)) {
            $this->getContainer()->get('logger')->debug('ProxyHeader was select `HTTP` proxy '.$proxySchema.', but it is banned');
            $cache->deleteItem($proxy->getKey());

            return $this->getRandomHttpProxy();
        }

        $this->getContainer()->get('logger')->debug('ProxyHeader was select `HTTP` proxy: '.$proxySchema);

        return $proxySchema;
    }

    /**
     * @return string
     */
    public function getRandomHttpsProxy(): string
    {
        $cache    = $this->getContainer()->get('cache.app');
        $cacheKey = $this->getHttpsProxyCacheKey();
        $proxy    = $cache->getItem($cacheKey);

        if ($proxy->isHit()) {
            $proxySchema = $proxy->get();
        } else {
            \Assert\that($this->proxyList)->keyExists('s');
            \Assert\that($this->proxyList['s'])->isArray();

            $key  = rand(0, count($this->proxyList['s']) - 1);
            $item = $this->proxyList['s'][$key];

            \Assert\that($item)->isArray()->keyExists('ip')->keyExists('port');
            \Assert\that($item['ip'])->string()->ip();
            \Assert\that($item['port'])->digit();

            $proxySchema = $item['ip'].':'.$item['port'];

            $proxy->set($proxySchema);
            $cache->save($proxy);
        }

        if ($this->isBannedProxy($proxySchema)) {
            $this->getContainer()->get('logger')->debug('ProxyHeader was select `HTTPS` proxy '.$proxySchema.', but it is banned');
            $cache->deleteItem($proxy->getKey());

            return $this->getRandomHttpsProxy();
        }

        $this->getContainer()->get('logger')->debug('ProxyHeader was select `HTTPS` proxy: '.$proxySchema);

        return $proxySchema;
    }

    /**
     * @return string
     */
    public function getRandomSocks5Proxy(): string
    {
        $cache    = $this->getContainer()->get('cache.app');
        $cacheKey = $this->getSocks5ProxyCacheKey();
        $proxy    = $cache->getItem($cacheKey);

        if ($proxy->isHit()) {
            $proxySchema = $proxy->get();
        } else {
            \Assert\that($this->proxyList)->keyExists(5);
            \Assert\that($this->proxyList[5])->isArray();

            $key  = rand(0, count($this->proxyList[5]) - 1);
            $item = $this->proxyList[5][$key];

            \Assert\that($item)->isArray()->keyExists('ip')->keyExists('port');
            \Assert\that($item['ip'])->string()->ip();
            \Assert\that($item['port'])->digit();

            $proxySchema = $item['ip'].':'.$item['port'];

            $proxy->set($proxySchema);
            $cache->save($proxy);
        }

        if ($this->isBannedProxy($proxySchema)) {
            $this->getContainer()->get('logger')->debug('ProxyHeader was select `SOCKS5` proxy '.$proxySchema.', but it is banned');
            $cache->deleteItem($proxy->getKey());

            return $this->getRandomSocks5Proxy();
        }

        $this->getContainer()->get('logger')->debug('ProxyHeader was select `SOCKS5` proxy: '.$proxySchema);

        return $proxySchema;
    }
}
