<?php

namespace KinopoiskBundle\EntitySchema;

final class MovieMeta
{
    const KEY_KINOPOISK_ID          = 'kinopoisk_id';
    const KEY_KINOPOISK_FLAG_TV     = 'kinopoisk_flag_tv';
    const KEY_KINOPOISK_FLAG_VIDEO  = 'kinopoisk_flag_video';
    const KEY_KINOPOISK_DESCRIPTION = 'kinopoisk_description';
    const KEY_KINOPOISK_RATING      = 'kinopoisk_rating';
    const KEY_IMDB_RATING           = 'imdb_rating';
    const KEY_TITLE_ORIGIN          = 'title_origin';
    const KEY_TITLE_RU              = 'title_ru';
    const KEY_YEAR                  = 'year';
    const KEY_COUNTRIES             = 'countries';
    const KEY_TAGLINE               = 'tagline';
    const KEY_DIRECTORS             = 'directors';
    const KEY_SCREENWRITERS         = 'screenwriters';
    const KEY_PRODUCERS             = 'producers';
    const KEY_OPERATORS             = 'operators';
    const KEY_COMPOSERS             = 'composers';
    const KEY_PAINTERS              = 'painters';
    const KEY_EDITORS               = 'editors';
    const KEY_GENRES                = 'genres';
    const KEY_AGE_LIMIT             = 'age_limit';
    const KEY_ACTORS                = 'actors';
}
