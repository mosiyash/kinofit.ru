<?php

namespace KinopoiskBundle\EntitySchema;

final class PersonMeta
{
    const KEY_KINOPOISK_ID  = 'kinopoisk_id';
    const KEY_FIRSTNAME_EN  = 'firstname_en';
    const KEY_LASTNAME_EN   = 'lastname_en';
    const KEY_PATRONYMIC_EN = 'patronymic_en';
    const KEY_FIRSTNAME_RU  = 'firstname_ru';
    const KEY_LASTNAME_RU   = 'lastname_ru';
    const KEY_PATRONYMIC_RU = 'patronymic_ru';
}
