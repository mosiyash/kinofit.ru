<?php

namespace FrontendPersonViewBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    /**
     * @Route("/person/{id}", name="person")
     */
    public function indexAction()
    {
        return $this->render('FrontendPersonViewBundle:Default:person.html.twig');
    }
}
