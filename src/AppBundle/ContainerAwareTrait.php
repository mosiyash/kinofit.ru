<?php

namespace AppBundle;

use League\Flysystem\Filesystem;
use Symfony\Component\DependencyInjection\ContainerInterface;

trait ContainerAwareTrait
{
    /**
     * @throws \RuntimeException
     *
     * @return ContainerInterface
     */
    private function container(): ContainerInterface
    {
        if (method_exists($this, 'getContainer')) {
            return $this->getContainer();
        } elseif (isset(self::$kernel) && self::$kernel instanceof \AppKernel) {
            /** @var \AppKernel $kernel */
            $kernel = self::$kernel;

            return $kernel->getContainer();
        }
        throw new \RuntimeException(sprintf(
            'Container not found in %s',
            get_class($this)
        ));
    }

    /**
     * @return Filesystem
     */
    public function getMoviesPostersFilesystem(): Filesystem
    {
        return $this->container()
            ->get('oneup_flysystem.mount_manager')
            ->getFilesystem('movies_posters');
    }
}
