<?php

namespace AppBundle\Composer;

use Composer\Script\Event;

class ScriptHandler extends \Sensio\Bundle\DistributionBundle\Composer\ScriptHandler
{
    protected static $options = [
        'symfony-app-dir'        => 'app',
        'symfony-web-dir'        => 'web',
        'symfony-assets-install' => 'hard',
        'symfony-cache-warmup'   => false,
    ];

    public static function doSetDirectoriesPermissions(Event $event)
    {
        $varPath = self::getOptions($event)['symfony-var-dir'];
        $users   = self::getAccessedUsers();

        $commands = [
            sprintf('setfacl -R %s %s', '-m u:'.implode(':rwX -m u:', $users).':rwX', $varPath),
            sprintf('setfacl -dR %s %s', '-m u:'.implode(':rwX -m u:', $users).':rwX', $varPath),
        ];

        foreach ($commands as $command) {
            exec(
                $command,
                $output,
                $res
            );

            if ($res !== 0) {
                throw new \RuntimeException(sprintf('Execute of `%s` return %s. Output is: %s'), $command, $res, implode("\n", $output));
            }
        }
    }

    protected static function getAccessedUsers()
    {
        $command = "ps axo user,comm | grep -E '[a]pache|[h]ttpd|[_]www|[w]ww-data|[n]ginx' | grep -v root | head -1 | cut -d\  -f1";

        exec($command, $output, $res);

        if ($res !== 0) {
            throw new \RuntimeException(sprintf('Execute of `%s` return %s. Output is: %s'), $command, $res, implode("\n", $output));
        }

        return array_values(array_unique(array_merge(
            $output,
            [posix_getpwuid(posix_geteuid())['name']]
        )));
    }
}
