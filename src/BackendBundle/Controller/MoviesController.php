<?php

namespace BackendBundle\Controller;

use KinofitBundle\Entity\Movie;
use KinofitBundle\Repository\MovieRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class MoviesController extends Controller
{
    /**
     * @Route("/movies")
     */
    public function indexAction(Request $request)
    {
        $em        = $this->get('doctrine.orm.entity_manager');
        $paginator = $this->get('knp_paginator');

        /** @var MovieRepository $movieRepository */
        $movieRepository   = $em->getRepository(Movie::class);
        $movieQueryBuilder = $movieRepository->getBackendListQueryBuilder();
        $movieQuery        = $movieQueryBuilder->getQuery();

        $pagination = $paginator->paginate(
            $movieQuery,
            $request->query->getInt('page', 1),
            10,
            ['wrap-queries' => true]
        );

        return $this->render('BackendBundle:Movies:index.html.twig', [
            'pagination' => $pagination,
        ]);
    }
}
