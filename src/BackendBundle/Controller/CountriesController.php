<?php

namespace BackendBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class CountriesController extends Controller
{
    /**
     * @Route("/countries")
     */
    public function indexAction(Request $request)
    {
        $em = $this->get('doctrine.orm.entity_manager');

        $queryBuilder = $em->getRepository('KinofitBundle:Country')
            ->createQueryBuilder('country');

        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $queryBuilder->getQuery(),
            $request->query->getInt('page', 1)
        );

        return $this->render('BackendBundle:Countries:index.html.twig', [
            'pagination' => $pagination,
        ]);
    }
}
