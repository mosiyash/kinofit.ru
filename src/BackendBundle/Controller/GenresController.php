<?php

namespace BackendBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class GenresController extends Controller
{
    /**
     * @Route("/genres")
     */
    public function indexAction()
    {
        return $this->render('BackendBundle:Genres:index.html.twig');
    }
}
