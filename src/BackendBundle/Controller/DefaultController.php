<?php

namespace BackendBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{
    /**
     * @Route("/")
     */
    public function indexAction(Request $request)
    {
        return $this->render('BackendBundle:Default:index.html.twig');
    }

    /**
     * @param Request $request
     * @Route("/run", name="backend_run")
     * @Method()
     */
    public function runAction(Request $request)
    {
        exit(dump(':)'));
    }
}
