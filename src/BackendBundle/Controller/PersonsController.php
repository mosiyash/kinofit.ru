<?php

namespace BackendBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class PersonsController extends Controller
{
    /**
     * @Route("/persons")
     */
    public function indexAction()
    {
        return $this->render('BackendBundle:Persons:index.html.twig');
    }
}
