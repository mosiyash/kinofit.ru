<?php

namespace FrontendMovieViewBundle\Controller;

use KinofitBundle\Entity\Movie;
use KinofitBundle\Entity\MovieTranslation;
use KinofitBundle\Repository\MovieRepository;
use KinofitBundle\Repository\MovieTranslationRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class DefaultController extends Controller
{
    /**
     * @param Request $request
     * @Route("/movie/{slug}", name="movie")
     *
     * @return Response
     */
    public function indexAction(Request $request): Response
    {
        $em = $this->get('doctrine.orm.entity_manager');

        /** @var MovieRepository $movieRepository */
        $movieRepository = $em->getRepository(Movie::class);

        /** @var MovieTranslationRepository $movieTranslationRepository */
        $movieTranslationRepository = $em->getRepository(MovieTranslation::class);

        $movieTranslation = $movieTranslationRepository->findOneBy(['slug' => $request->attributes->get('slug')]);
        $movie            = $movieTranslation->getTranslatable();

        return $this->render('FrontendMovieViewBundle:Default:movie.html.twig', [
            'movie' => $movie,
        ]);
    }
}
