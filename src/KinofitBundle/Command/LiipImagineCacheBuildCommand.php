<?php

namespace KinofitBundle\Command;

use Eloquent\Pathogen\FileSystem\FileSystemPath;
use KinofitBundle\Entity\Movie;
use KinofitBundle\Repository\MovieRepository;
use Liip\ImagineBundle\Templating\ImagineExtension;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Wrep\Daemonizable\Command\EndlessContainerAwareCommand;

class LiipImagineCacheBuildCommand extends EndlessContainerAwareCommand
{
    const NAME = 'liip:imagine:cache:build';

    protected function configure()
    {
        $this
            ->setName(self::NAME)
            ->setDescription('Generate thumbs cache for project')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        exit(dump($this->getContainer()->get('liip_imagine.filter.configuration')->all()));
        $logger = $this->getContainer()->get('logger');

        /** @var ImagineExtension $imagine */
        $imagine = $this->getContainer()->get('twig')->getExtension(ImagineExtension::class);

        $em = $this->getContainer()->get('doctrine.orm.entity_manager');

        /** @var MovieRepository $movieRepository */
        $movieRepository = $em->getRepository(Movie::class);

        $movies = $movieRepository->createQueryBuilder('movie')->getQuery()->iterate();

        foreach ($movies as $result) {
            /** @var Movie $movie */
            $movie = $result[0];
            if (!$movie->getPoster()) {
                $logger->addDebug(sprintf('Movie %s has not a poster', $movie->getId()));
                continue;
            }

            $logger->addDebug(sprintf('Processing movie %s', $movie->getId()));

            $image_url = str_replace(
                ['/localhost/'],
                ['/127.0.0.1:8001/'],
                $imagine->filter(
                    $movie->getPoster()->getPath().'/'.$movie->getPoster()->getBasename(),
                    'movie_poster_thumb'
                )
            );

            $filepath = (string) FileSystemPath::fromString($this->getContainer()->getParameter('project_dir').'/web/media/cache/'.preg_replace('#^.+?/([^/]+?)/([^/]+?)$#i', '\1/\2', $image_url))->normalize();
            if (file_exists($filepath)) {
                $logger->addDebug(sprintf('Found cached file: %s', $filepath));
                continue;
            }

            $logger->addDebug(sprintf('Requesting %s...', $image_url));

            $res = file_get_contents($image_url);

            if (!$res) {
                $logger->addError(sprintf('Contents are not fetched'));
            } else {
                $logger->addDebug(sprintf('OK'));
            }
        }
    }
}
