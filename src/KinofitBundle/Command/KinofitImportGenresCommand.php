<?php

namespace KinofitBundle\Command;

use Assert\InvalidArgumentException;
use KinofitBundle\Entity\Genre;
use KinopoiskBundle\EntitySchema\MovieMeta;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Exception\RuntimeException;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Finder\Finder;

class KinofitImportGenresCommand extends ContainerAwareCommand
{
    const NAME = 'kinofit:import-genres';

    use CommandTrait;

    protected function configure()
    {
        $this
            ->setName(self::NAME)
            ->setDescription('Import kinopoisk genres')
        ;
    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     *
     * @throws RuntimeException
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $batchSize = 20;

        $em = $this->getContainer()->get('doctrine.orm.entity_manager');

        $files = Finder::create()
            ->in($this->getContainer()->getParameter('kinopoisk.json_dir').'/find-genres')
            ->name('genres.json')
            ->sortByChangedTime();

        foreach ($files as $file) {
            $this->getContainer()->get('logger')->debug('Importing '.$file->getPathname());

            $contents = file_get_contents($file->getPathname(), true);
            if (!$contents) {
                throw new RuntimeException(sprintf('Couldn\'t get contents from %s', $file->getPathname()));
            }

            $data = json_decode($contents, true);
            if (!$data) {
                throw new RuntimeException('Couldn\'t decode JSON');
            }

            foreach ($data as $row_key => $row) {
                try {
                    \Assert\that($row)->keyExists(MovieMeta::KEY_KINOPOISK_ID);
                    \Assert\that($row[MovieMeta::KEY_KINOPOISK_ID])->nullOr()->integer()->greaterThan(0);
                    \Assert\that($row)->keyExists(MovieMeta::KEY_TITLE_RU);
                    \Assert\that($row[MovieMeta::KEY_TITLE_RU])->string()->notBlank();
                } catch (InvalidArgumentException $e) {
                    $this->logException($e);
                    $this->outputException($input, $output, $e);
                    $this->getContainer()->get('logger')->notice(sprintf(
                        'Skipped processing row %s at file %s',
                        $row_key,
                        $file->getPathname()
                    ));
                    continue;
                }

                $genre = $em->getRepository('KinofitBundle:Genre')->findOneBy(['kinopoiskId' => $row[MovieMeta::KEY_KINOPOISK_ID]]);
                if (!$genre) {
                    $genre = new Genre();
                    $genre->setKinopoiskId($row[MovieMeta::KEY_KINOPOISK_ID]);
                }

                $genre->translate('ru')->setTitle($row[MovieMeta::KEY_TITLE_RU]);
                $em->persist($genre);
                $genre->mergeNewTranslations();

                if (($row_key % $batchSize) == 0 || $row_key === count($data) - 1) {
                    $em->flush();
                    $em->clear();
                }
            }
        }
    }
}
