<?php

namespace KinofitBundle\Command;

use KinofitBundle\DBAL\Types\ContentAutoStatusType;
use KinofitBundle\Entity\Movie;
use KinofitBundle\Repository\MovieRepository;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class KinofitUpdateMoviesAutoStatusCommand extends ContainerAwareCommand
{
    const NAME = 'kinofit:update-movies-auto-status';

    protected function configure()
    {
        $this
            ->setName(self::NAME)
            ->setDescription('Automatically set `status` value for movies')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $logger = $this->getContainer()->get('logger');
        $em     = $this->getContainer()->get('doctrine.orm.entity_manager');

        /** @var MovieRepository $movieRepository */
        $movieRepository = $em->getRepository(Movie::class);

        $movieQueryBuilder = $movieRepository->createQueryBuilder('movie');

        foreach ($movieQueryBuilder->getQuery()->iterate() as $result) {
            /** @var Movie $movie */
            $movie = $result[0];

            if ($movie->getAutoStatus() === ContentAutoStatusType::STATUS_PUBLISHED and $movie->getYear() === null) {
                $logger->addDebug(sprintf('Movie %s auto_status is published, but year is null', $movie->getId()));
                $movie->setAutoStatus(ContentAutoStatusType::STATUS_DRAFT);
                $em->persist($movie);
                $em->flush();
                $logger->addDebug(sprintf('Movie auto_status setted to %s', ContentAutoStatusType::STATUS_DRAFT));
            } elseif ($movie->getAutoStatus() !== ContentAutoStatusType::STATUS_PUBLISHED and $movie->getYear()) {
                $logger->addDebug(sprintf('The status of movie %s is %s, but year is presented', $movie->getId(), $movie->getAutoStatus()));
                $movie->setAutoStatus(ContentAutoStatusType::STATUS_PUBLISHED);
                $em->persist($movie);
                $em->flush();
                $logger->addDebug(sprintf('Movie auto_status setted to %s', ContentAutoStatusType::STATUS_PUBLISHED));
            }

            $em->clear(Movie::class);
        }
    }
}
