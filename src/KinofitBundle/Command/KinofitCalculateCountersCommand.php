<?php

namespace KinofitBundle\Command;

use KinofitBundle\Entity\Country;
use KinofitBundle\Entity\Genre;
use KinofitBundle\Entity\Movie;
use KinofitBundle\Repository\CountryRepository;
use KinofitBundle\Repository\GenreRepository;
use KinofitBundle\Repository\MovieRepository;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class KinofitCalculateCountersCommand extends ContainerAwareCommand
{
    const NAME = 'kinofit:calculate-counters';

    protected function configure()
    {
        $this
            ->setName(self::NAME)
            ->setDescription('Calculate static counters for movies')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $logger = $this->getContainer()->get('logger');
        $em     = $this->getContainer()->get('doctrine.orm.entity_manager');

        /** @var CountryRepository $countryRepository */
        $countryRepository = $em->getRepository(Country::class);

        /** @var Country $country */
        foreach ($countryRepository->findAll() as $country) {
            $logger->addDebug(sprintf('Processing country %s...', $country->getId()));

            $countMovies = $country->getMovies()->count();
            $country->setCountMovies($countMovies);

            $logger->addDebug(sprintf('Set %s', $countMovies));

            $em->persist($country);
            $em->flush();
        }

        /** @var GenreRepository $genreRepository */
        $genreRepository = $em->getRepository(Genre::class);

        /** @var Genre $genre */
        foreach ($genreRepository->findAll() as $genre) {
            $logger->addDebug(sprintf('Processing genre %s...', $genre->getId()));

            $countMovies = $genre->getMovies()->count();
            $genre->setCountMovies($countMovies);

            $logger->addDebug(sprintf('Set %s', $countMovies));

            $em->persist($genre);
            $em->flush();
        }

        /** @var MovieRepository $movieRepository */
        $movieRepository = $em->getRepository(Movie::class);

        /** @var Movie $movie */
        foreach ($movieRepository->findAll() as $movie) {
            $logger->addDebug(sprintf('Processing movie %s...', $movie->getId()));

            // todo: calculate popularity
            $popularity = 0;
            $movie->setPopularity($popularity);

            $logger->addDebug(sprintf('Set %s', $popularity));

            $em->persist($movie);
            $em->flush();
        }
    }
}
