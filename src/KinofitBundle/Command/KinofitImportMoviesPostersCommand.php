<?php

namespace KinofitBundle\Command;

use AppBundle\ContainerAwareTrait;
use KinofitBundle\Entity\File;
use KinofitBundle\Entity\Movie;
use KinofitBundle\Repository\FileRepository;
use KinofitBundle\Repository\MovieRepository;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Exception\RuntimeException;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class KinofitImportMoviesPostersCommand extends ContainerAwareCommand
{
    use CommandTrait;
    use ContainerAwareTrait;

    const NAME       = 'kinofit:import-movies-posters';
    const QUEUE_NAME = 'kinofit-import-movies-posters';

    const OPTION_KINOPOISK_ID = 'kinopoisk-id';
    const OPTION_POSTER_URL   = 'poster-url';

    protected function configure()
    {
        $this
            ->setName(self::NAME)
            ->setDescription('Import movies posters from JSON files')
            ->addOption(self::OPTION_KINOPOISK_ID, null, InputOption::VALUE_REQUIRED, 'Movie id on KinoPoisk.Ru')
            ->addOption(self::OPTION_POSTER_URL, null, InputOption::VALUE_REQUIRED, 'Remote poster url')
        ;
    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     *
     * @throws RuntimeException
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $logger            = $this->getContainer()->get('logger');
        $kinopoiskId       = $input->getOption(self::OPTION_KINOPOISK_ID);
        $posterUrl         = $input->getOption(self::OPTION_POSTER_URL);
        $postersFilesystem = $this->getMoviesPostersFilesystem();
        $em                = $this->getContainer()->get('doctrine.orm.entity_manager');
        $guzzle            = $this->getContainer()->get('kinopoisk.client');

        /** @var MovieRepository $movieRepository */
        $movieRepository = $em->getRepository(Movie::class);

        /** @var FileRepository $fileRepository */
        $fileRepository = $em->getRepository(File::class);

        /** @var Movie $movie */
        $movie = $movieRepository->findOneBy(['kinopoiskId' => $kinopoiskId]);
        if (!$movie) {
            return null;
        }

        $response = $guzzle->get($posterUrl);
        \Assert\that($response->getStatusCode())->integer()->same(200);
        \Assert\that($response->getHeader('Content-Type')[0])->string()->regex('#^image/.+$#i');

        if (strlen((string) $response->getBody()) === 3379) {
            $logger->addDebug('Image content length is 3370, it is dummy image');
        }

        $fsPath     = null;
        $fsBasename = $kinopoiskId.'.'.pathinfo($posterUrl, PATHINFO_EXTENSION);

        if (!$postersFilesystem->has($fsBasename)) {
            $res = $postersFilesystem->write(
                $fsBasename,
                (string) $response->getBody()
            );

            if (!$res) {
                throw new RuntimeException(sprintf(
                    'Couldn\'t write %s to %s filesystem',
                    $fsBasename,
                    'movies_posters'
                ));
            }
        }

        $poster = $fileRepository->findOneBy([
            'filesystem' => 'movies_posters',
            'path'       => $fsPath,
            'basename'   => $fsBasename,
        ]);

        if (!$poster) {
            $poster = new File();
            $poster->setFilesystem('movies_posters');
            $poster->setPath($fsPath);
            $poster->setBasename($fsBasename);
            $em->persist($poster);
        }

        $movie->setPoster($poster);
        $em->persist($movie);

        $em->flush();
    }
}
