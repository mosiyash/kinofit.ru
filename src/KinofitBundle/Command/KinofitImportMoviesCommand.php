<?php

namespace KinofitBundle\Command;

use Doctrine\Common\Inflector\Inflector;
use KinofitBundle\Entity\Movie;
use KinofitBundle\Entity\MovieTranslation;
use KinopoiskBundle\Command\KinopoiskParsePersonsCommand;
use KinopoiskBundle\EntitySchema\MovieMeta;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Exception\RuntimeException;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Finder\Finder;

class KinofitImportMoviesCommand extends ContainerAwareCommand
{
    use CommandTrait;

    const NAME       = 'kinofit:import-movies';
    const QUEUE_NAME = 'kinofit-import-movies';

    const OPTION_KINOPOISK_ID  = 'kinopoisk-id';
    const OPTION_JSON_FILEPATH = 'json-filepath';

    protected function configure()
    {
        $this
            ->setName(self::NAME)
            ->setDescription('Import movies from JSON files')
            ->addOption(self::OPTION_KINOPOISK_ID, null, InputOption::VALUE_OPTIONAL, 'Movie id on KinoPoisk.Ru')
            ->addOption(self::OPTION_JSON_FILEPATH, null, InputOption::VALUE_OPTIONAL, 'Absolute path to JSON file wich contains movie data')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        if ($input->hasOption(self::OPTION_JSON_FILEPATH)) {
            $files = Finder::create()
                ->in(dirname($input->getOption(self::OPTION_JSON_FILEPATH)))
                ->name(basename($input->getOption(self::OPTION_JSON_FILEPATH)))
                ->sortByChangedTime();
            if (!$files->count()) {
                throw new RuntimeException('Couldn\'t find the file '.escapeshellarg($input->getOption(self::OPTION_JSON_FILEPATH)));
            }
        } else {
            $files = Finder::create()
                ->in($this->getContainer()->getParameter('kinopoisk.json_dir').'/find-movies')
                ->in($this->getContainer()->getParameter('kinopoisk.json_dir').'/parse-movies')
                ->name('*.json')
                ->sortByChangedTime();
        }

        foreach ($files as $file) {
            $this->getContainer()->get('logger')->debug('Importing '.$file->getPathname());

            $data = $this->extractDataFromJsonFile($file->getPathname());
            foreach ($data as $arrayItem) {
                if ($input->getOption(self::OPTION_KINOPOISK_ID)) {
                    if ($arrayItem[MovieMeta::KEY_KINOPOISK_ID] == $input->getOption(self::OPTION_KINOPOISK_ID)) {
                        $this->importArrayItem($arrayItem);
                    }
                } else {
                    $this->importArrayItem($arrayItem);
                }
            }

            if (!unlink($file->getPathname())) {
                throw new RuntimeException('Couldn\'t unlink file '.$file->getPathname());
            }
        }
    }

    /**
     * @param string $filepath
     *
     * @throws RuntimeException
     *
     * @return array
     */
    private function extractDataFromJsonFile(string $filepath): array
    {
        $contents = file_get_contents($filepath);
        if (!$contents) {
            throw new RuntimeException('Couldn\'t get contents from the file '.$filepath);
        }

        $data = json_decode($contents, true);
        if ($data === null) {
            throw new RuntimeException('Couldn\'t get decode JSON contents from the file '.$filepath);
        } elseif (!is_array($data)) {
            throw new RuntimeException('Decoded data from the file '.$filepath.' must be an array, but '.gettype($data).' given');
        }

        return $data;
    }

    /**
     * @param array $arrayItem
     */
    private function importArrayItem(array $arrayItem): void
    {
        $keys  = $this->getMovieMeta();
        $em    = $this->getContainer()->get('doctrine.orm.entity_manager');

        /** @var Movie $movie */
        $movie = $em->getRepository('KinofitBundle:Movie')->findOneBy(['kinopoiskId' => $arrayItem[MovieMeta::KEY_KINOPOISK_ID]]);

        if (!$movie) {
            $movie = new Movie();
            $movie->setKinopoiskId($arrayItem[MovieMeta::KEY_KINOPOISK_ID]);
        }

        $translations = [];

        foreach ($keys as $constant => $key) {
            if (!array_key_exists($key, $arrayItem)) {
                continue;
            } elseif (is_array($arrayItem[$key])) {
                $this->setRelationMovieProperty($movie, $arrayItem, $key);
            } elseif (preg_match('#^(.+?)\_(ru|en)$#iu', $key, $matches)) {
                $property = $matches[1];
                $lang     = mb_strtolower($matches[2]);
                $translations[$lang][$property] = [
                    'movie' => $movie,
                    'arrayItem' => $arrayItem,
                    'key' => $key,
                ];
            } else {
                $this->setStandardMovieProperty($movie, $arrayItem, $key);
            }
        }

        foreach ($translations as $lang => $langTranslations) {
            foreach ($langTranslations as $property => $translation) {
                $this->setTranslationMovieProperty($translation['movie'], $lang, $property, $translation['arrayItem'], $translation['key']);
            }
        }

        $movie->mergeNewTranslations();
        $em->persist($movie);
        $em->flush();

        $this->getContainer()->get('logger')->debug('Saved');
    }

    /**
     * @param Movie  $movie
     * @param array  $arrayItem
     * @param string $key
     */
    private function setStandardMovieProperty(Movie $movie, array $arrayItem, string $key): void
    {
        $property = Inflector::camelize($key);
        $movie->{'set'.Inflector::ucwords($property)}($arrayItem[$key]);
        $this->getContainer()->get('logger')->debug('Set `'.$property.'` to '.json_encode($arrayItem[$key], JSON_UNESCAPED_UNICODE));
    }

    /**
     * @param Movie  $movie
     * @param string $lang
     * @param string $property
     * @param array  $arrayItem
     * @param string $key
     */
    private function setTranslationMovieProperty(Movie $movie, string $lang, string $property, array $arrayItem, string $key)
    {
        $property = Inflector::camelize($property);
        $movie->translate($lang)->{'set'.Inflector::ucwords($property)}($arrayItem[$key]);
        $this->getContainer()->get('logger')->debug('Set '.$lang.'-translation of `'.$property.'` to '.json_encode($arrayItem[$key], JSON_UNESCAPED_UNICODE));
    }

    /**
     * @param Movie  $movie
     * @param array  $arrayItem
     * @param string $key
     */
    private function setRelationMovieProperty(Movie $movie, array $arrayItem, string $key)
    {
        $em                   = $this->getContainer()->get('doctrine.orm.entity_manager');
        $property             = Inflector::camelize($key);
        $addPropertyMethod    = 'add'.Inflector::ucwords(Inflector::singularize($property));
        $removePropertyMethod = 'remove'.Inflector::ucwords(Inflector::singularize($property));
        $getPropertiesMethod  = 'get'.Inflector::ucwords($property);

        if ($property === 'countries') {
            $entityName = 'Country';
        } elseif ($property === 'genres') {
            $entityName = 'Genre';
        } else {
            $entityName = 'Person';
        }

        foreach ($movie->{$getPropertiesMethod}() as $entity) {
            $movie->{$removePropertyMethod}($entity);
        }
        foreach ($arrayItem[$key] as $kinopoiskId) {
            $entity = $em->getRepository('KinofitBundle:'.$entityName)
                ->findOneBy(['kinopoiskId' => $kinopoiskId]);

            if (!$entity) {
                $entityClass = '\\KinofitBundle\\Entity\\'.$entityName;
                $entity      = new $entityClass();
                $entity->setKinopoiskId($kinopoiskId);
                $em->persist($entity);
                $em->flush();

                if ($entityName === 'Person') {
                    $queue = $this->getContainer()->get('jobqueue');
                    $queue->attach(KinopoiskParsePersonsCommand::QUEUE_NAME);
                    $queue->push([
                        'command'  => KinopoiskParsePersonsCommand::NAME,
                        'argument' => [
                            '--'.KinopoiskParsePersonsCommand::OPTION_KINOPOISK_ID => $kinopoiskId,
                            '--env'                                                => $this->getContainer()->get('kernel')->getEnvironment(),
                        ],
                    ]);
                }
            }
            $movie->{$addPropertyMethod}($entity);
        }
        $this->getContainer()->get('logger')->debug('Set `'.$property.'` to '.json_encode($arrayItem[$key], JSON_UNESCAPED_UNICODE));
    }

    /**
     * @return array
     */
    private function getMovieMeta(): array
    {
        static $keys = [];
        if (empty($keys)) {
            $reflectionClass = new \ReflectionClass(MovieMeta::class);
            $keys            = $reflectionClass->getConstants();
        }

        return $keys;
    }
}
