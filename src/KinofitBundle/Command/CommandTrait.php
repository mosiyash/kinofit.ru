<?php

namespace KinofitBundle\Command;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * @method ContainerInterface getContainer
 */
trait CommandTrait
{
    /**
     * @param \Exception $e
     */
    protected function logException(\Exception $e): void
    {
        $this->getContainer()->get('logger')->error(sprintf(
            '%s: %s at %s line %s while running console command `%s`',
            get_class($e),
            $e->getMessage(),
            $e->getFile(),
            $e->getLine(),
            $this->getName()
        ));
    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     * @param \Exception      $e
     */
    protected function outputException(InputInterface $input, OutputInterface $output, \Exception $e): void
    {
        $io = new SymfonyStyle($input, $output);
        $io->block([
            'Stack trace:',
            $e->getTraceAsString(),
        ]);
    }
}
