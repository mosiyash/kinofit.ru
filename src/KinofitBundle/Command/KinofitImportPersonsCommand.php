<?php

namespace KinofitBundle\Command;

use Doctrine\Common\Inflector\Inflector;
use KinofitBundle\Entity\Person;
use KinopoiskBundle\Command\KinopoiskParsePersonsCommand;
use KinopoiskBundle\EntitySchema\PersonMeta;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Exception\RuntimeException;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Finder\Finder;

class KinofitImportPersonsCommand extends ContainerAwareCommand
{
    use CommandTrait;

    const NAME       = 'kinofit:import-persons';
    const QUEUE_NAME = 'kinofit-import-persons';

    const OPTION_KINOPOISK_ID  = 'kinopoisk-id';
    const OPTION_JSON_FILEPATH = 'json-filepath';

    protected function configure()
    {
        $this
            ->setName(self::NAME)
            ->setDescription('Import movies from JSON files')
            ->addOption(self::OPTION_KINOPOISK_ID, null, InputOption::VALUE_REQUIRED, 'Person id on KinoPoisk.Ru')
            ->addOption(self::OPTION_JSON_FILEPATH, null, InputOption::VALUE_REQUIRED, 'Absolute path to JSON file wich contains person data')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $files = Finder::create()
            ->in(dirname($input->getOption(self::OPTION_JSON_FILEPATH)))
            ->name(basename($input->getOption(self::OPTION_JSON_FILEPATH)))
            ->sortByChangedTime();

        if (!$files->count()) {
            throw new RuntimeException('Couldn\'t find the file '.escapeshellarg($input->getOption(self::OPTION_JSON_FILEPATH)));
        }

        foreach ($files as $file) {
            $this->getContainer()->get('logger')->debug('Importing '.$file->getPathname());

            $data = $this->extractDataFromJsonFile($file->getPathname());
            foreach ($data as $arrayItem) {
                if ($input->getOption(self::OPTION_KINOPOISK_ID)) {
                    if ($arrayItem[PersonMeta::KEY_KINOPOISK_ID] == $input->getOption(self::OPTION_KINOPOISK_ID)) {
                        $this->importArrayItem($arrayItem);
                    }
                } else {
                    $this->importArrayItem($arrayItem);
                }
            }

            if (!unlink($file->getPathname())) {
                throw new RuntimeException('Couldn\'t unlink file '.$file->getPathname());
            }
        }
    }

    /**
     * @param string $filepath
     *
     * @throws RuntimeException
     *
     * @return array
     */
    private function extractDataFromJsonFile(string $filepath): array
    {
        $contents = file_get_contents($filepath);
        if (!$contents) {
            throw new RuntimeException('Couldn\'t get contents from the file '.$filepath);
        }

        $data = json_decode($contents, true);
        if ($data === null) {
            throw new RuntimeException('Couldn\'t get decode JSON contents from the file '.$filepath);
        } elseif (!is_array($data)) {
            throw new RuntimeException('Decoded data from the file '.$filepath.' must be an array, but '.gettype($data).' given');
        }

        return $data;
    }

    /**
     * @param array $arrayItem
     */
    private function importArrayItem(array $arrayItem): void
    {
        $keys   = $this->getPersonMeta();
        $em     = $this->getContainer()->get('doctrine.orm.entity_manager');
        $person = $em->getRepository('KinofitBundle:Person')->findOneBy(['kinopoiskId' => $arrayItem[PersonMeta::KEY_KINOPOISK_ID]]);

        if (!$person) {
            $person = new Person();
            $person->setKinopoiskId($arrayItem[PersonMeta::KEY_KINOPOISK_ID]);
        }

        $translations = [];

        foreach ($keys as $constant => $key) {
            if (!array_key_exists($key, $arrayItem)) {
                continue;
            } elseif (is_array($arrayItem[$key])) {
                $this->setRelationPersonProperty($person, $arrayItem, $key);
            } elseif (preg_match('#^(.+?)\_(ru|en)$#iu', $key, $matches)) {
                $property = $matches[1];
                $lang     = mb_strtolower($matches[2]);
                $translations[$lang][$property] = [
                    'person' => $person,
                    'arrayItem' => $arrayItem,
                    'key' => $key,
                ];
            } else {
                $this->setStandardPersonProperty($person, $arrayItem, $key);
            }
        }

        foreach ($translations as $lang => $langTranslations) {
            foreach ($langTranslations as $property => $translation) {
                if ($translation['arrayItem']['firstname_'.$lang] === null
                    && $translation['arrayItem']['lastname_'.$lang] === null
                    && $translation['arrayItem']['patronymic_'.$lang] === null) {
                    // todo: remove translation with current itration lang
                    continue;
                }
                $this->setTranslationPersonProperty($translation['person'], $lang, $property, $translation['arrayItem'], $translation['key']);
            }
        }

        $person->mergeNewTranslations();
        $em->persist($person);
        $em->flush();

        $this->getContainer()->get('logger')->debug('Saved');
    }

    /**
     * @param Person $person
     * @param array  $arrayItem
     * @param string $key
     */
    private function setStandardPersonProperty(Person $person, array $arrayItem, string $key): void
    {
        $property = Inflector::camelize($key);
        $person->{'set'.Inflector::ucwords($property)}($arrayItem[$key]);
        $this->getContainer()->get('logger')->debug('Set `'.$property.'` to '.json_encode($arrayItem[$key], JSON_UNESCAPED_UNICODE));
    }

    /**
     * @param Person $person
     * @param string $lang
     * @param string $property
     * @param array  $arrayItem
     * @param string $key
     */
    private function setTranslationPersonProperty(Person $person, string $lang, string $property, array $arrayItem, string $key)
    {
        $property = Inflector::camelize($property);
        $person->translate($lang)->{'set'.Inflector::ucwords($property)}($arrayItem[$key]);
        $this->getContainer()->get('logger')->debug('Set '.$lang.'-translation of `'.$property.'` to '.json_encode($arrayItem[$key], JSON_UNESCAPED_UNICODE));
    }

    /**
     * @param Person $person
     * @param array  $arrayItem
     * @param string $key
     */
    private function setRelationPersonProperty(Person $person, array $arrayItem, string $key)
    {
        $em                   = $this->getContainer()->get('doctrine.orm.entity_manager');
        $property             = Inflector::camelize($key);
        $addPropertyMethod    = 'add'.Inflector::ucwords(Inflector::singularize($property));
        $removePropertyMethod = 'remove'.Inflector::ucwords(Inflector::singularize($property));
        $getPropertiesMethod  = 'get'.Inflector::ucwords($property);

        if ($property === 'countries') {
            $entityName = 'Country';
        } elseif ($property === 'genres') {
            $entityName = 'Genre';
        } else {
            $entityName = 'Person';
        }

        foreach ($person->{$getPropertiesMethod}() as $entity) {
            $person->{$removePropertyMethod}($entity);
        }

        foreach ($arrayItem[$key] as $kinopoiskId) {
            $entity = $em->getRepository('KinofitBundle:'.$entityName)
                ->createQueryBuilder('entity')
                ->where('entity.kinopoiskId = :kinopoisk_id')
                ->setParameter(':kinopoisk_id', $kinopoiskId)
                ->getQuery()
                ->getOneOrNullResult();

            if (!$entity) {
                $entityClass = '\\KinofitBundle\\Entity\\'.$entityName;
                $entity      = new $entityClass();
                $entity->setKinopoiskId($kinopoiskId);
                $em->persist($entity);

                if ($entityName === 'Person') {
                    $queue = $this->getContainer()->get('jobqueue');
                    $queue->attach(KinopoiskParsePersonsCommand::QUEUE_NAME);
                    $queue->push([
                        'command'  => KinopoiskParsePersonsCommand::NAME,
                        'argument' => [
                            '--'.KinopoiskParsePersonsCommand::OPTION_KINOPOISK_ID => $kinopoiskId,
                            '--env'                                                => $this->getContainer()->get('kernel')->getEnvironment(),
                        ],
                    ]);
                }
            }
            $person->{$addPropertyMethod}($entity);
        }
        $this->getContainer()->get('logger')->debug('Set `'.$property.'` to '.json_encode($arrayItem[$key], JSON_UNESCAPED_UNICODE));
    }

    /**
     * @return array
     */
    private function getPersonMeta(): array
    {
        static $keys = [];
        if (empty($keys)) {
            $reflectionClass = new \ReflectionClass(PersonMeta::class);
            $keys            = $reflectionClass->getConstants();
        }

        return $keys;
    }
}
