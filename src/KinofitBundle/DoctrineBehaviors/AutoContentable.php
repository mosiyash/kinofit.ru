<?php

namespace KinofitBundle\DoctrineBehaviors;

use Doctrine\ORM\Mapping as ORM;
use Fresh\DoctrineEnumBundle\Validator\Constraints\Enum;
use KinofitBundle\DBAL\Types\ContentAutoStatusType;

/**
 * @ORM\HasLifecycleCallbacks()
 */
trait AutoContentable
{
    /**
     * @var string
     *
     *
     * @ORM\Column(name="auto_status", type="ContentAutoStatusType", nullable=false)
     * @Enum(entity="KinofitBundle\DBAL\Types\ContentAutoStatusType")
     */
    private $autoStatus;

    /**
     * @ORM\PostLoad()
     * @ORM\PrePersist()
     */
    final public function defaultContentableValues()
    {
        if ($this->autoStatus === null) {
            $this->autoStatus = ContentAutoStatusType::STATUS_DRAFT;
        }
    }
}
