<?php

namespace KinofitBundle\Imagine\Binary\Loader;

use Liip\ImagineBundle\Binary\Loader\FlysystemLoader;
use Liip\ImagineBundle\Exception\Binary\Loader\NotLoadableException;
use Liip\ImagineBundle\Model\Binary;

class MoviesPostersFlysystemLoader extends FlysystemLoader
{
    /**
     * @var string
     */
    private $imagePlaceholderPath;

    /**
     * @return mixed
     */
    public function getImagePlaceholderPath(): string
    {
        return $this->imagePlaceholderPath;
    }

    /**
     * @param mixed $imagePlaceholderPath
     */
    public function setImagePlaceholderPath($imagePlaceholderPath): void
    {
        $this->imagePlaceholderPath = $imagePlaceholderPath;
    }

    /**
     * {@inheritdoc}
     */
    public function find($path)
    {
        if ($this->filesystem->has($path) === false) {
            if (!file_exists($this->imagePlaceholderPath)) {
                throw new NotLoadableException(sprintf(
                    'Placeholder image %s is not found.',
                    $this->imagePlaceholderPath,
                    $path
                ));
            } elseif (!is_readable($this->imagePlaceholderPath)) {
                throw new NotLoadableException(sprintf(
                    'Placeholder image %s isnot readable.',
                    $this->imagePlaceholderPath,
                    $path
                ));
            }

            $contents = file_get_contents($this->imagePlaceholderPath);

            if (!$contents) {
                throw new NotLoadableException(sprintf(
                    'Couldn\'t get contents from placeholder image %s.',
                    $this->imagePlaceholderPath,
                    $path
                ));
            }

            $mimeType = mime_content_type($this->imagePlaceholderPath);

            return new Binary(
                $contents,
                $mimeType,
                $this->extensionGuesser->guess($mimeType)
            );
        } elseif ($this->filesystem->getSize($path) < 4 * 1024) {
            $contents = file_get_contents($this->imagePlaceholderPath);

            if (!$contents) {
                throw new NotLoadableException(sprintf(
                    'Couldn\'t get contents from placeholder image %s.',
                    $this->imagePlaceholderPath,
                    $path
                ));
            }

            $mimeType = mime_content_type($this->imagePlaceholderPath);

            return new Binary(
                $contents,
                $mimeType,
                $this->extensionGuesser->guess($mimeType)
            );
        }

        return parent::find($path);
    }
}
