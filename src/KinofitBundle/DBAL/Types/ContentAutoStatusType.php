<?php

namespace KinofitBundle\DBAL\Types;

use Fresh\DoctrineEnumBundle\DBAL\Types\AbstractEnumType;

final class ContentAutoStatusType extends AbstractEnumType
{
    const STATUS_DRAFT     = 'draft';
    const STATUS_HELD      = 'held';
    const STATUS_PUBLISHED = 'published';

    protected static $choices = [
        self::STATUS_DRAFT     => 'Draft',
        self::STATUS_HELD      => 'Held',
        self::STATUS_PUBLISHED => 'Published',
    ];
}
