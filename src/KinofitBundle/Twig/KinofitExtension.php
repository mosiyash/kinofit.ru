<?php

namespace KinofitBundle\Twig;

use Doctrine\ORM\EntityManager;
use KinofitBundle\Entity\Country;
use KinofitBundle\Entity\Genre;
use KinofitBundle\Repository\CountryRepository;
use KinofitBundle\Repository\GenreRepository;

class KinofitExtension extends \Twig_Extension
{
    /**
     * @var EntityManager
     */
    private $em;

    /**
     * KinofitExtension constructor.
     *
     * @param EntityManager $em
     */
    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    /**
     * @return array
     */
    public function getFilters()
    {
        return [
            new \Twig_SimpleFilter('extract', [$this, 'extract']),
        ];
    }

    /**
     * @return array
     */
    public function getFunctions()
    {
        return [
            new \Twig_SimpleFunction('get_genres', [$this, 'getGenres']),
            new \Twig_SimpleFunction('get_genre', [$this, 'getGenre']),
            new \Twig_SimpleFunction('get_countries', [$this, 'getCountries']),
            new \Twig_SimpleFunction('get_country', [$this, 'getCountry']),
            new \Twig_SimpleFunction('extract', [$this, 'extract']),
        ];
    }

    /**
     * @return array|Genre[]
     */
    public function getGenres(): array
    {
        /** @var GenreRepository $repository */
        $repository = $this->em->getRepository(Genre::class);

        $queryBuilder = $repository->createQueryBuilder('genre')
            ->leftJoin('genre.translations', 'genre_translation')
            ->orderBy('genre_translation.title');

        return $queryBuilder->getQuery()->getResult();
    }

    /**
     * @param int $id
     *
     * @return null|Genre
     */
    public function getGenre(int $id): ? Genre
    {
        /** @var GenreRepository $repository */
        $repository = $this->em->getRepository(Genre::class);

        return $repository->find($id);
    }

    /**
     * @return array|Country[]
     */
    public function getCountries() : array
    {
        /** @var CountryRepository $repository */
        $repository = $this->em->getRepository(Country::class);

        $queryBuilder = $repository->createQueryBuilder('country')
            ->where('country.countMovies > 0');

        return $queryBuilder->getQuery()->getResult();
    }

    /**
     * @param int $id
     *
     * @return null|Country
     */
    public function getCountry(int $id): ? Country
    {
        /** @var CountryRepository $repository */
        $repository = $this->em->getRepository(Country::class);

        return $repository->find($id);
    }

    /**
     * @param array $data
     * @param array $keys
     *
     * @return array
     */
    public function extract(array $data, array $keys) : array
    {
        $result = [];
        foreach ($keys as $key) {
            if (array_key_exists($key, $data)) {
                $result[$key] = $data[$key];
            }
        }

        return $result;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'kinofit_extension';
    }
}
