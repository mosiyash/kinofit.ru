<?php

namespace KinofitBundle\DataFixtures\ORM;

use AccountBundle\Entity\Account;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

class LoadAccountData implements FixtureInterface, ContainerAwareInterface
{
    use ContainerAwareTrait;

    public function load(ObjectManager $manager)
    {
        $accountNikita = new Account();
        $accountNikita->setUsername('nikita');
        $accountNikita->setConfirmed(true);
        $accountNikita->setConfirmedAt(new \DateTime());
        $accountNikita->setCreatedAt(new \DateTime());
        $accountNikita->setEmail('nikita.mosiyash@gmail.com');
        $accountNikita->setIsActive(true);
        $accountNikita->setPassword('$2y$12$CQSxL4U8PJcX9.O/UYqG3O78YHklcdGvDufei1DMtsAVoYnHT3/fG');
        $accountNikita->setVkontakteId('1311807');

        $manager->persist($accountNikita);
        $manager->flush();
    }
}
