<?php

namespace KinofitBundle\DataFixtures\ORM;

use Assert\Assert;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use KinofitBundle\Command\KinofitImportGenresCommand;
use KinopoiskBundle\Command\KinopoiskFindGenresCommand;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Output\BufferedOutput;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

class LoadGenreData implements FixtureInterface, ContainerAwareInterface
{
    use ContainerAwareTrait;

    public function load(ObjectManager $manager)
    {
        $this->findGenres();
        $this->importGenres();
    }

    final private function findGenres()
    {
        $kernel      = $this->container->get('kernel');
        $application = new Application($kernel);
        $application->setAutoExit(false);

        $input = new ArrayInput([
            'command' => KinopoiskFindGenresCommand::NAME,
        ]);

        $output = new BufferedOutput();
        $code   = $application->run($input, $output);
        Assert::that($code)->same(0);
    }

    final private function importGenres()
    {
        $kernel      = $this->container->get('kernel');
        $application = new Application($kernel);
        $application->setAutoExit(false);

        $input = new ArrayInput([
            'command' => KinofitImportGenresCommand::NAME,
        ]);

        $output = new BufferedOutput();
        $code   = $application->run($input, $output);
        Assert::that($code)->same(0);
    }
}
