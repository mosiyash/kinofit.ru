<?php

namespace KinofitBundle\DataFixtures\ORM;

use Assert\Assert;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use KinofitBundle\Command\KinofitImportCountriesCommand;
use KinopoiskBundle\Command\KinopoiskFindCountriesCommand;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Output\BufferedOutput;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

class LoadCountryData implements FixtureInterface, ContainerAwareInterface
{
    use ContainerAwareTrait;

    public function load(ObjectManager $manager)
    {
        $this->findCountries();
        $this->importCountries();
    }

    final private function findCountries()
    {
        $kernel      = $this->container->get('kernel');
        $application = new Application($kernel);
        $application->setAutoExit(false);

        $input = new ArrayInput([
            'command' => KinopoiskFindCountriesCommand::NAME,
        ]);

        $output = new BufferedOutput();
        $code   = $application->run($input, $output);
        Assert::that($code)->same(0);
    }

    final private function importCountries()
    {
        $kernel      = $this->container->get('kernel');
        $application = new Application($kernel);
        $application->setAutoExit(false);

        $input = new ArrayInput([
            'command' => KinofitImportCountriesCommand::NAME,
        ]);

        $output = new BufferedOutput();
        $code   = $application->run($input, $output);
        Assert::that($code)->same(0);
    }
}
