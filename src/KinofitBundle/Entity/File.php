<?php

namespace KinofitBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * File.
 *
 * @ORM\Table(name="file")
 * @ORM\Entity(repositoryClass="KinofitBundle\Repository\FileRepository")
 */
class File
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="filesystem", type="string", length=255)
     */
    private $filesystem;

    /**
     * @var string
     *
     * @ORM\Column(name="path", type="text", nullable=true)
     */
    private $path;

    /**
     * @var string
     *
     * @ORM\Column(name="basename", type="text")
     */
    private $basename;

    /**
     * @var Movie
     * @ORM\OneToOne(targetEntity="KinofitBundle\Entity\Movie", mappedBy="poster")
     */
    private $movie;

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set filesystem.
     *
     * @param string $filesystem
     *
     * @return File
     */
    public function setFilesystem($filesystem)
    {
        $this->filesystem = $filesystem;

        return $this;
    }

    /**
     * Get filesystem.
     *
     * @return string
     */
    public function getFilesystem()
    {
        return $this->filesystem;
    }

    /**
     * Set basename.
     *
     * @param string $basename
     *
     * @return File
     */
    public function setBasename($basename)
    {
        $this->basename = $basename;

        return $this;
    }

    /**
     * Get basename.
     *
     * @return string
     */
    public function getBasename()
    {
        return $this->basename;
    }

    /**
     * Set movie.
     *
     * @param \KinofitBundle\Entity\Movie $movie
     *
     * @return File
     */
    public function setMovie(\KinofitBundle\Entity\Movie $movie = null)
    {
        $this->movie = $movie;

        return $this;
    }

    /**
     * Get movie.
     *
     * @return \KinofitBundle\Entity\Movie
     */
    public function getMovie()
    {
        return $this->movie;
    }

    /**
     * Set path.
     *
     * @param string $path
     *
     * @return File
     */
    public function setPath($path)
    {
        $this->path = $path;

        return $this;
    }

    /**
     * Get path.
     *
     * @return string
     */
    public function getPath()
    {
        return $this->path;
    }
}
