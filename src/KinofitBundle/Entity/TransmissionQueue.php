<?php

namespace KinofitBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * TransmissionQueue.
 *
 * @ORM\Table(name="transmission_queue")
 * @ORM\Entity(repositoryClass="KinofitBundle\Repository\TransmissionQueueRepository")
 */
class TransmissionQueue
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="KinofitBundle\Entity\Movie", inversedBy="transmissionQueues")
     */
    private $movie;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var int
     *
     * @ORM\Column(name="size", type="integer")
     */
    private $size;

    /**
     * @var string
     *
     * @ORM\Column(name="announce", type="string", length=255)
     */
    private $announce;

    /**
     * @var array
     *
     * @ORM\Column(name="announce_list", type="simple_array")
     */
    private $announceList;

    /**
     * @var string
     *
     * @ORM\Column(name="comment", type="string", length=255)
     */
    private $comment;

    /**
     * @var string
     *
     * @ORM\Column(name="encoded_hash", type="string", length=255)
     */
    private $encodedHash;

    /**
     * @var array
     *
     * @ORM\Column(name="extra_meta", type="simple_array")
     */
    private $extraMeta;

    /**
     * @var array
     *
     * @ORM\Column(name="file_list", type="simple_array")
     */
    private $fileList;

    /**
     * @var string
     *
     * @ORM\Column(name="hash", type="string", length=255)
     */
    private $hash;

    /**
     * @var array
     *
     * @ORM\Column(name="info", type="simple_array")
     */
    private $info;

    /**
     * @var int
     *
     * @ORM\Column(name="piece_length_exp", type="integer")
     */
    private $pieceLengthExp;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime")
     */
    private $createdAt;

    /**
     * @var string
     *
     * @ORM\Column(name="created_by", type="string", length=255)
     */
    private $createdBy;

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name.
     *
     * @param string $name
     *
     * @return TransmissionQueue
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name.
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set size.
     *
     * @param int $size
     *
     * @return TransmissionQueue
     */
    public function setSize($size)
    {
        $this->size = $size;

        return $this;
    }

    /**
     * Get size.
     *
     * @return int
     */
    public function getSize()
    {
        return $this->size;
    }

    /**
     * Set announce.
     *
     * @param string $announce
     *
     * @return TransmissionQueue
     */
    public function setAnnounce($announce)
    {
        $this->announce = $announce;

        return $this;
    }

    /**
     * Get announce.
     *
     * @return string
     */
    public function getAnnounce()
    {
        return $this->announce;
    }

    /**
     * Set announceList.
     *
     * @param array $announceList
     *
     * @return TransmissionQueue
     */
    public function setAnnounceList($announceList)
    {
        $this->announceList = $announceList;

        return $this;
    }

    /**
     * Get announceList.
     *
     * @return array
     */
    public function getAnnounceList()
    {
        return $this->announceList;
    }

    /**
     * Set comment.
     *
     * @param string $comment
     *
     * @return TransmissionQueue
     */
    public function setComment($comment)
    {
        $this->comment = $comment;

        return $this;
    }

    /**
     * Get comment.
     *
     * @return string
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * Set encodedHash.
     *
     * @param string $encodedHash
     *
     * @return TransmissionQueue
     */
    public function setEncodedHash($encodedHash)
    {
        $this->encodedHash = $encodedHash;

        return $this;
    }

    /**
     * Get encodedHash.
     *
     * @return string
     */
    public function getEncodedHash()
    {
        return $this->encodedHash;
    }

    /**
     * Set extraMeta.
     *
     * @param array $extraMeta
     *
     * @return TransmissionQueue
     */
    public function setExtraMeta($extraMeta)
    {
        $this->extraMeta = $extraMeta;

        return $this;
    }

    /**
     * Get extraMeta.
     *
     * @return array
     */
    public function getExtraMeta()
    {
        return $this->extraMeta;
    }

    /**
     * Set fileList.
     *
     * @param array $fileList
     *
     * @return TransmissionQueue
     */
    public function setFileList($fileList)
    {
        $this->fileList = $fileList;

        return $this;
    }

    /**
     * Get fileList.
     *
     * @return array
     */
    public function getFileList()
    {
        return $this->fileList;
    }

    /**
     * Set hash.
     *
     * @param string $hash
     *
     * @return TransmissionQueue
     */
    public function setHash($hash)
    {
        $this->hash = $hash;

        return $this;
    }

    /**
     * Get hash.
     *
     * @return string
     */
    public function getHash()
    {
        return $this->hash;
    }

    /**
     * Set info.
     *
     * @param array $info
     *
     * @return TransmissionQueue
     */
    public function setInfo($info)
    {
        $this->info = $info;

        return $this;
    }

    /**
     * Get info.
     *
     * @return array
     */
    public function getInfo()
    {
        return $this->info;
    }

    /**
     * Set pieceLengthExp.
     *
     * @param int $pieceLengthExp
     *
     * @return TransmissionQueue
     */
    public function setPieceLengthExp($pieceLengthExp)
    {
        $this->pieceLengthExp = $pieceLengthExp;

        return $this;
    }

    /**
     * Get pieceLengthExp.
     *
     * @return int
     */
    public function getPieceLengthExp()
    {
        return $this->pieceLengthExp;
    }

    /**
     * Set createdAt.
     *
     * @param \DateTime $createdAt
     *
     * @return TransmissionQueue
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt.
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set createdBy.
     *
     * @param string $createdBy
     *
     * @return TransmissionQueue
     */
    public function setCreatedBy($createdBy)
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    /**
     * Get createdBy.
     *
     * @return string
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * Set movie.
     *
     * @param \KinofitBundle\Entity\Movie $movie
     *
     * @return TransmissionQueue
     */
    public function setMovie(\KinofitBundle\Entity\Movie $movie = null)
    {
        $this->movie = $movie;

        return $this;
    }

    /**
     * Get movie.
     *
     * @return \KinofitBundle\Entity\Movie
     */
    public function getMovie()
    {
        return $this->movie;
    }
}
