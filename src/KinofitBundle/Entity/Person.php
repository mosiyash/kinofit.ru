<?php

namespace KinofitBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;

/**
 * Person.
 *
 * @ORM\Table(name="person")
 * @ORM\Entity(repositoryClass="KinofitBundle\Repository\PersonRepository")
 */
class Person
{
    use ORMBehaviors\Translatable\Translatable;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="kinopoiskId", type="integer", nullable=true, unique=true)
     */
    private $kinopoiskId;

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set kinopoiskId.
     *
     * @param int $kinopoiskId
     *
     * @return Person
     */
    public function setKinopoiskId($kinopoiskId)
    {
        $this->kinopoiskId = $kinopoiskId;

        return $this;
    }

    /**
     * Get kinopoiskId.
     *
     * @return int
     */
    public function getKinopoiskId()
    {
        return $this->kinopoiskId;
    }
    /**
     * Constructor.
     */
    public function __construct()
    {
        $this->movies = new \Doctrine\Common\Collections\ArrayCollection();
    }
}
