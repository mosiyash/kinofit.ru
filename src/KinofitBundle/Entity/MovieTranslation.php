<?php

namespace KinofitBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;

/**
 * @ORM\Table(name="movie_translation")
 * @ORM\Entity(repositoryClass="KinofitBundle\Repository\MovieTranslationRepository")
 */
class MovieTranslation
{
    use ORMBehaviors\Translatable\Translation;
    use ORMBehaviors\Sluggable\Sluggable;

    /**
     * @var string
     * @ORM\Column(name="title", type="string", length=255, nullable=true)
     */
    private $title;

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @param string $title
     *
     * @return MovieTranslation
     */
    public function setTitle(string $title): MovieTranslation
    {
        $this->title = $title;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getSluggableFields(): array
    {
        return ['title'];
    }
}
