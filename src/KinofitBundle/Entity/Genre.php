<?php

namespace KinofitBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;

/**
 * Genre.
 *
 * @ORM\Table(name="genre")
 * @ORM\Entity(repositoryClass="KinofitBundle\Repository\GenreRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Genre
{
    use ORMBehaviors\Translatable\Translatable;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="kinopoiskId", type="integer", nullable=true, unique=true)
     */
    private $kinopoiskId;

    /**
     * @var ArrayCollection
     * @ORM\ManyToMany(targetEntity="KinofitBundle\Entity\Movie", mappedBy="genres")
     */
    private $movies;

    /**
     * @var int
     * @ORM\Column(name="count_movies", type="integer", nullable=false, options={"default": 0})
     */
    private $countMovies;

    /**
     * @var int
     * @ORM\Column(name="count_tvseries", type="integer", nullable=false, options={"default": 0})
     */
    private $countTvSeries;

    /**
     * @ORM\PostLoad()
     * @ORM\PrePersist()
     */
    public function defaults()
    {
        if ($this->countMovies === null) {
            $this->countMovies = 0;
        }
        if ($this->countTvSeries === null) {
            $this->countTvSeries = 0;
        }
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set kinopoiskId.
     *
     * @param int $kinopoiskId
     *
     * @return Genre
     */
    public function setKinopoiskId($kinopoiskId)
    {
        $this->kinopoiskId = $kinopoiskId;

        return $this;
    }

    /**
     * Get kinopoiskId.
     *
     * @return int
     */
    public function getKinopoiskId()
    {
        return $this->kinopoiskId;
    }
    /**
     * Constructor.
     */
    public function __construct()
    {
        $this->movies = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add movie.
     *
     * @param \KinofitBundle\Entity\Movie $movie
     *
     * @return Genre
     */
    public function addMovie(\KinofitBundle\Entity\Movie $movie)
    {
        $this->movies[] = $movie;

        return $this;
    }

    /**
     * Remove movie.
     *
     * @param \KinofitBundle\Entity\Movie $movie
     */
    public function removeMovie(\KinofitBundle\Entity\Movie $movie)
    {
        $this->movies->removeElement($movie);
    }

    /**
     * Get movies.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getMovies()
    {
        return $this->movies;
    }

    /**
     * Set countMovies.
     *
     * @param int $countMovies
     *
     * @return Genre
     */
    public function setCountMovies($countMovies)
    {
        $this->countMovies = $countMovies;

        return $this;
    }

    /**
     * Get countMovies.
     *
     * @return int
     */
    public function getCountMovies()
    {
        return $this->countMovies;
    }

    /**
     * Set countTvSeries.
     *
     * @param int $countTvSeries
     *
     * @return Genre
     */
    public function setCountTvSeries($countTvSeries)
    {
        $this->countTvSeries = $countTvSeries;

        return $this;
    }

    /**
     * Get countTvSeries.
     *
     * @return int
     */
    public function getCountTvSeries()
    {
        return $this->countTvSeries;
    }
}
