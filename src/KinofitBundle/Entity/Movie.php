<?php

namespace KinofitBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use KinofitBundle\DBAL\Types\ContentAutoStatusType;
use KinofitBundle\DoctrineBehaviors\AutoContentable;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;

/**
 * Movie.
 *
 * @ORM\Table(name="movie")
 * @ORM\Entity(repositoryClass="KinofitBundle\Repository\MovieRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Movie
{
    use ORMBehaviors\Translatable\Translatable;
    use AutoContentable;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="kinopoisk_id", type="integer", nullable=true, unique=true)
     */
    private $kinopoiskId;

    /**
     * @var string
     *
     * @ORM\Column(name="title_origin", type="string", length=255, nullable=true)
     */
    private $titleOrigin;

    /**
     * @var bool
     * @ORM\Column(name="kinopoisk_flag_tv", type="boolean", nullable=false)
     */
    private $kinopoiskFlagTv;

    /**
     * @var bool
     * @ORM\Column(name="kinopoisk_flag_video", type="boolean", nullable=false)
     */
    private $kinopoiskFlagVideo;

    /**
     * @var string
     * @ORM\Column(name="kinopoisk_description", type="text", nullable=true)
     */
    private $kinopoiskDescription;

    /**
     * @var float
     * @ORM\Column(name="kinopoisk_rating", type="float", scale=3, nullable=true)
     */
    private $kinopoiskRating;

    /**
     * @var float
     * @ORM\Column(name="imdb_rating", type="float", scale=3, nullable=true)
     */
    private $imdbRating;

    /**
     * @var int
     * @ORM\Column(name="year", type="integer", nullable=true)
     */
    private $year;

    /**
     * @var string
     * @ORM\Column(name="tagline", type="text", nullable=true)
     */
    private $tagline;

    /**
     * @var int
     * @ORM\Column(name="age_limit", type="smallint", nullable=true)
     */
    private $ageLimit;

    /**
     * @var ArrayCollection
     * @ORM\ManyToMany(targetEntity="KinofitBundle\Entity\Country", inversedBy="movies")
     * @ORM\JoinTable(name="movie_country")
     */
    private $countries;

    /**
     * @var ArrayCollection
     * @ORM\ManyToMany(targetEntity="KinofitBundle\Entity\Person")
     * @ORM\JoinTable(name="movie_director")
     */
    private $directors;

    /**
     * @var ArrayCollection
     * @ORM\ManyToMany(targetEntity="KinofitBundle\Entity\Person")
     * @ORM\JoinTable(name="movie_screenwriter")
     */
    private $screenwriters;

    /**
     * @var ArrayCollection
     * @ORM\ManyToMany(targetEntity="KinofitBundle\Entity\Person")
     * @ORM\JoinTable(name="movie_producer")
     */
    private $producers;

    /**
     * @var ArrayCollection
     * @ORM\ManyToMany(targetEntity="KinofitBundle\Entity\Person")
     * @ORM\JoinTable(name="movie_operator")
     */
    private $operators;

    /**
     * @var ArrayCollection
     * @ORM\ManyToMany(targetEntity="KinofitBundle\Entity\Person")
     * @ORM\JoinTable(name="movie_composer")
     */
    private $composers;

    /**
     * @var ArrayCollection
     * @ORM\ManyToMany(targetEntity="KinofitBundle\Entity\Person")
     * @ORM\JoinTable(name="movie_painter")
     */
    private $painters;

    /**
     * @var ArrayCollection
     * @ORM\ManyToMany(targetEntity="KinofitBundle\Entity\Person")
     * @ORM\JoinTable(name="movie_editor")
     */
    private $editors;

    /**
     * @var ArrayCollection
     * @ORM\ManyToMany(targetEntity="KinofitBundle\Entity\Genre", inversedBy="movies")
     * @ORM\JoinTable(name="movie_genre")
     */
    private $genres;

    /**
     * @var File
     * @ORM\OneToOne(targetEntity="KinofitBundle\Entity\File", inversedBy="movie")
     * @ORM\JoinColumn(name="poster_id", referencedColumnName="id")
     */
    private $poster;

    /**
     * @var float
     * @ORM\Column(name="popularity", type="float", scale=3, nullable=false, options={"default": 0})
     */
    private $popularity;

    /**
     * @ORM\OneToMany(targetEntity="KinofitBundle\Entity\TransmissionQueue", mappedBy="movie")
     */
    private $transmissionQueues;

    /**
     * Get id.
     *
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * Set kinopoiskId.
     *
     * @param int $kinopoiskId
     *
     * @return Movie
     */
    public function setKinopoiskId($kinopoiskId): Movie
    {
        $this->kinopoiskId = $kinopoiskId;

        return $this;
    }

    /**
     * Get kinopoiskId.
     *
     * @return int
     */
    public function getKinopoiskId(): int
    {
        return $this->kinopoiskId;
    }

    /**
     * Set titleOrigin.
     *
     * @param null|string $titleOrigin
     *
     * @return Movie|null
     */
    public function setTitleOrigin(string $titleOrigin = null): ? Movie
    {
        $this->titleOrigin = $titleOrigin;

        return $this;
    }

    /**
     * Get titleOrigin.
     *
     * @return string|null
     */
    public function getTitleOrigin() : ? string
    {
        return $this->titleOrigin;
    }

    /**
     * @return bool
     */
    public function isKinopoiskFlagTv() : bool
    {
        return $this->kinopoiskFlagTv;
    }

    /**
     * @param bool $kinopoiskFlagTv
     *
     * @return Movie
     */
    public function setKinopoiskFlagTv(bool $kinopoiskFlagTv): Movie
    {
        $this->kinopoiskFlagTv = $kinopoiskFlagTv;

        return $this;
    }

    /**
     * @return bool
     */
    public function isKinopoiskFlagVideo(): bool
    {
        return $this->kinopoiskFlagVideo;
    }

    /**
     * @param bool $kinopoiskFlagVideo
     *
     * @return Movie
     */
    public function setKinopoiskFlagVideo(bool $kinopoiskFlagVideo): Movie
    {
        $this->kinopoiskFlagVideo = $kinopoiskFlagVideo;

        return $this;
    }

    /**
     * Get kinopoiskFlagTv.
     *
     * @return bool
     */
    public function getKinopoiskFlagTv()
    {
        return $this->kinopoiskFlagTv;
    }

    /**
     * Get kinopoiskFlagVideo.
     *
     * @return bool
     */
    public function getKinopoiskFlagVideo()
    {
        return $this->kinopoiskFlagVideo;
    }

    /**
     * @ORM\PostLoad()
     * @ORM\PrePersist()
     */
    public function defaultValues()
    {
        if ($this->kinopoiskFlagTv === null) {
            $this->kinopoiskFlagTv = false;
        }

        if ($this->kinopoiskFlagVideo === null) {
            $this->kinopoiskFlagVideo = false;
        }

        if ($this->popularity === null) {
            $this->popularity = 0;
        }
    }

    /**
     * Set kinopoiskDescription.
     *
     * @param string $kinopoiskDescription
     *
     * @return Movie
     */
    public function setKinopoiskDescription($kinopoiskDescription)
    {
        $this->kinopoiskDescription = $kinopoiskDescription;

        return $this;
    }

    /**
     * Get kinopoiskDescription.
     *
     * @return string
     */
    public function getKinopoiskDescription()
    {
        return $this->kinopoiskDescription;
    }

    /**
     * Set kinopoiskRating.
     *
     * @param float $kinopoiskRating
     *
     * @return Movie
     */
    public function setKinopoiskRating($kinopoiskRating)
    {
        $this->kinopoiskRating = $kinopoiskRating;

        return $this;
    }

    /**
     * Get kinopoiskRating.
     *
     * @return float
     */
    public function getKinopoiskRating()
    {
        return $this->kinopoiskRating;
    }

    /**
     * Set imdbRating.
     *
     * @param float $imdbRating
     *
     * @return Movie
     */
    public function setImdbRating($imdbRating)
    {
        $this->imdbRating = $imdbRating;

        return $this;
    }

    /**
     * Get imdbRating.
     *
     * @return float
     */
    public function getImdbRating()
    {
        return $this->imdbRating;
    }

    /**
     * Set year.
     *
     * @param int $year
     *
     * @return Movie
     */
    public function setYear($year)
    {
        $this->year = $year;

        return $this;
    }

    /**
     * Get year.
     *
     * @return int
     */
    public function getYear()
    {
        return $this->year;
    }

    /**
     * Set tagline.
     *
     * @param string $tagline
     *
     * @return Movie
     */
    public function setTagline($tagline)
    {
        $this->tagline = $tagline;

        return $this;
    }

    /**
     * Get tagline.
     *
     * @return string
     */
    public function getTagline()
    {
        return $this->tagline;
    }

    /**
     * Set ageLimit.
     *
     * @param int $ageLimit
     *
     * @return Movie
     */
    public function setAgeLimit($ageLimit)
    {
        $this->ageLimit = $ageLimit;

        return $this;
    }

    /**
     * Get ageLimit.
     *
     * @return int
     */
    public function getAgeLimit()
    {
        return $this->ageLimit;
    }

    /**
     * Add country.
     *
     * @param \KinofitBundle\Entity\Country $country
     *
     * @return Movie
     */
    public function addCountry(\KinofitBundle\Entity\Country $country)
    {
        $this->countries[] = $country;

        return $this;
    }

    /**
     * Remove country.
     *
     * @param \KinofitBundle\Entity\Country $country
     */
    public function removeCountry(\KinofitBundle\Entity\Country $country)
    {
        $this->countries->removeElement($country);
    }

    /**
     * Get countries.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCountries()
    {
        return $this->countries;
    }

    /**
     * Add director.
     *
     * @param \KinofitBundle\Entity\Person $director
     *
     * @return Movie
     */
    public function addDirector(\KinofitBundle\Entity\Person $director)
    {
        $this->directors[] = $director;

        return $this;
    }

    /**
     * Remove director.
     *
     * @param \KinofitBundle\Entity\Person $director
     */
    public function removeDirector(\KinofitBundle\Entity\Person $director)
    {
        $this->directors->removeElement($director);
    }

    /**
     * Get directors.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getDirectors()
    {
        return $this->directors;
    }

    /**
     * Add screenwriter.
     *
     * @param \KinofitBundle\Entity\Person $screenwriter
     *
     * @return Movie
     */
    public function addScreenwriter(\KinofitBundle\Entity\Person $screenwriter)
    {
        $this->screenwriters[] = $screenwriter;

        return $this;
    }

    /**
     * Remove screenwriter.
     *
     * @param \KinofitBundle\Entity\Person $screenwriter
     */
    public function removeScreenwriter(\KinofitBundle\Entity\Person $screenwriter)
    {
        $this->screenwriters->removeElement($screenwriter);
    }

    /**
     * Get screenwriters.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getScreenwriters()
    {
        return $this->screenwriters;
    }

    /**
     * Add producer.
     *
     * @param \KinofitBundle\Entity\Person $producer
     *
     * @return Movie
     */
    public function addProducer(\KinofitBundle\Entity\Person $producer)
    {
        $this->producers[] = $producer;

        return $this;
    }

    /**
     * Remove producer.
     *
     * @param \KinofitBundle\Entity\Person $producer
     */
    public function removeProducer(\KinofitBundle\Entity\Person $producer)
    {
        $this->producers->removeElement($producer);
    }

    /**
     * Get producers.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getProducers()
    {
        return $this->producers;
    }

    /**
     * Add operator.
     *
     * @param \KinofitBundle\Entity\Person $operator
     *
     * @return Movie
     */
    public function addOperator(\KinofitBundle\Entity\Person $operator)
    {
        $this->operators[] = $operator;

        return $this;
    }

    /**
     * Remove operator.
     *
     * @param \KinofitBundle\Entity\Person $operator
     */
    public function removeOperator(\KinofitBundle\Entity\Person $operator)
    {
        $this->operators->removeElement($operator);
    }

    /**
     * Get operators.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getOperators()
    {
        return $this->operators;
    }

    /**
     * Add composer.
     *
     * @param \KinofitBundle\Entity\Person $composer
     *
     * @return Movie
     */
    public function addComposer(\KinofitBundle\Entity\Person $composer)
    {
        $this->composers[] = $composer;

        return $this;
    }

    /**
     * Remove composer.
     *
     * @param \KinofitBundle\Entity\Person $composer
     */
    public function removeComposer(\KinofitBundle\Entity\Person $composer)
    {
        $this->composers->removeElement($composer);
    }

    /**
     * Get composers.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getComposers()
    {
        return $this->composers;
    }

    /**
     * Add painter.
     *
     * @param \KinofitBundle\Entity\Person $painter
     *
     * @return Movie
     */
    public function addPainter(\KinofitBundle\Entity\Person $painter)
    {
        $this->painters[] = $painter;

        return $this;
    }

    /**
     * Remove painter.
     *
     * @param \KinofitBundle\Entity\Person $painter
     */
    public function removePainter(\KinofitBundle\Entity\Person $painter)
    {
        $this->painters->removeElement($painter);
    }

    /**
     * Get painters.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPainters()
    {
        return $this->painters;
    }

    /**
     * Add editor.
     *
     * @param \KinofitBundle\Entity\Person $editor
     *
     * @return Movie
     */
    public function addEditor(\KinofitBundle\Entity\Person $editor)
    {
        $this->editors[] = $editor;

        return $this;
    }

    /**
     * Remove editor.
     *
     * @param \KinofitBundle\Entity\Person $editor
     */
    public function removeEditor(\KinofitBundle\Entity\Person $editor)
    {
        $this->editors->removeElement($editor);
    }

    /**
     * Get editors.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getEditors()
    {
        return $this->editors;
    }

    /**
     * Add genre.
     *
     * @param \KinofitBundle\Entity\Genre $genre
     *
     * @return Movie
     */
    public function addGenre(\KinofitBundle\Entity\Genre $genre)
    {
        $this->genres[] = $genre;

        return $this;
    }

    /**
     * Remove genre.
     *
     * @param \KinofitBundle\Entity\Genre $genre
     */
    public function removeGenre(\KinofitBundle\Entity\Genre $genre)
    {
        $this->genres->removeElement($genre);
    }

    /**
     * Get genres.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getGenres()
    {
        return $this->genres;
    }
    /**
     * Constructor.
     */
    public function __construct()
    {
        $this->countries          = new \Doctrine\Common\Collections\ArrayCollection();
        $this->directors          = new \Doctrine\Common\Collections\ArrayCollection();
        $this->screenwriters      = new \Doctrine\Common\Collections\ArrayCollection();
        $this->producers          = new \Doctrine\Common\Collections\ArrayCollection();
        $this->operators          = new \Doctrine\Common\Collections\ArrayCollection();
        $this->composers          = new \Doctrine\Common\Collections\ArrayCollection();
        $this->painters           = new \Doctrine\Common\Collections\ArrayCollection();
        $this->editors            = new \Doctrine\Common\Collections\ArrayCollection();
        $this->genres             = new \Doctrine\Common\Collections\ArrayCollection();
        $this->transmissionQueues = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Set poster.
     *
     * @param \KinofitBundle\Entity\File $poster
     *
     * @return Movie
     */
    public function setPoster(\KinofitBundle\Entity\File $poster = null)
    {
        $this->poster = $poster;

        return $this;
    }

    /**
     * Get poster.
     *
     * @return \KinofitBundle\Entity\File
     */
    public function getPoster()
    {
        return $this->poster;
    }

    /**
     * Set popularity.
     *
     * @param float $popularity
     *
     * @return Movie
     */
    public function setPopularity($popularity)
    {
        $this->popularity = $popularity;

        return $this;
    }

    /**
     * Get popularity.
     *
     * @return float
     */
    public function getPopularity()
    {
        return $this->popularity;
    }

    /**
     * Set autoStatus.
     *
     * @param ContentAutoStatusType $autoStatus
     *
     * @return Movie
     */
    public function setAutoStatus($autoStatus)
    {
        $this->autoStatus = $autoStatus;

        return $this;
    }

    /**
     * Get autoStatus.
     *
     * @return ContentAutoStatusType
     */
    public function getAutoStatus()
    {
        return $this->autoStatus;
    }

    /**
     * Add transmissionQueue.
     *
     * @param \KinofitBundle\Entity\TransmissionQueue $transmissionQueue
     *
     * @return Movie
     */
    public function addTransmissionQueue(\KinofitBundle\Entity\TransmissionQueue $transmissionQueue)
    {
        $this->transmissionQueues[] = $transmissionQueue;

        return $this;
    }

    /**
     * Remove transmissionQueue.
     *
     * @param \KinofitBundle\Entity\TransmissionQueue $transmissionQueue
     */
    public function removeTransmissionQueue(\KinofitBundle\Entity\TransmissionQueue $transmissionQueue)
    {
        $this->transmissionQueues->removeElement($transmissionQueue);
    }

    /**
     * Get transmissionQueues.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTransmissionQueues()
    {
        return $this->transmissionQueues;
    }
}
