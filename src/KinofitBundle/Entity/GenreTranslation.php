<?php

namespace KinofitBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;

/**
 * GenreTranslation.
 *
 * @ORM\Table(name="genre_translation")
 * @ORM\Entity(repositoryClass="KinofitBundle\Repository\GenreTranslationRepository")
 */
class GenreTranslation
{
    use ORMBehaviors\Translatable\Translation;
    use ORMBehaviors\Sluggable\Sluggable;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255, nullable=true)
     */
    private $title;

    /**
     * Set title.
     *
     * @param string $title
     *
     * @return GenreTranslation
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title.
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * {@inheritdoc}
     */
    public function getSluggableFields(): array
    {
        return ['title'];
    }
}
