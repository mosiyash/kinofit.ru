<?php

namespace KinofitBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;

/**
 * Country.
 *
 * @ORM\Table(name="country")
 * @ORM\Entity(repositoryClass="KinofitBundle\Repository\CountryRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Country
{
    use ORMBehaviors\Translatable\Translatable;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="kinopoiskId", type="integer", nullable=true, unique=true)
     */
    private $kinopoiskId;

    /**
     * @var ArrayCollection
     * @ORM\ManyToMany(targetEntity="KinofitBundle\Entity\Movie", mappedBy="countries")
     */
    private $movies;

    /**
     * @var int
     * @ORM\Column(name="count_movies", type="integer", nullable=false, options={"default": 0})
     */
    private $countMovies;

    /**
     * @var int
     * @ORM\Column(name="count_tvseries", type="integer", nullable=false, options={"default": 0})
     */
    private $countTvSeries;

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getKinopoiskId(): int
    {
        return $this->kinopoiskId;
    }

    /**
     * @param int $kinopoiskId
     *
     * @return Country
     */
    public function setKinopoiskId(int $kinopoiskId): Country
    {
        $this->kinopoiskId = $kinopoiskId;

        return $this;
    }
    /**
     * Constructor.
     */
    public function __construct()
    {
        $this->movies = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add movie.
     *
     * @param \KinofitBundle\Entity\Movie $movie
     *
     * @return Country
     */
    public function addMovie(\KinofitBundle\Entity\Movie $movie)
    {
        $this->movies[] = $movie;

        return $this;
    }

    /**
     * Remove movie.
     *
     * @param \KinofitBundle\Entity\Movie $movie
     */
    public function removeMovie(\KinofitBundle\Entity\Movie $movie)
    {
        $this->movies->removeElement($movie);
    }

    /**
     * Get movies.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getMovies()
    {
        return $this->movies;
    }

    /**
     * Set countMovies.
     *
     * @param int $countMovies
     *
     * @return Country
     */
    public function setCountMovies($countMovies)
    {
        $this->countMovies = $countMovies;

        return $this;
    }

    /**
     * Get countMovies.
     *
     * @return int
     */
    public function getCountMovies()
    {
        return $this->countMovies;
    }

    /**
     * Set countTvSeries.
     *
     * @param int $countTvSeries
     *
     * @return Country
     */
    public function setCountTvSeries($countTvSeries)
    {
        $this->countTvSeries = $countTvSeries;

        return $this;
    }

    /**
     * Get countTvSeries.
     *
     * @return int
     */
    public function getCountTvSeries()
    {
        return $this->countTvSeries;
    }

    /**
     * @ORM\PostLoad()
     * @ORM\PrePersist()
     */
    public function defaults()
    {
        if (!$this->countMovies) {
            $this->countMovies = 0;
        }
        if (!$this->countTvSeries) {
            $this->countTvSeries = 0;
        }
    }
}
