<?php

namespace VideoConverterBundle\Command;

use Assert\Assert;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class VideoConverterConvertCommand extends ContainerAwareCommand
{
    const NAME                 = 'video-converter:convert';
    const REMOTE_GLOBAL_PREFIX = 'video-converter';
    const OPTION_SOURCE_VIDEO  = 'source-video';

    /**
     * @var InputInterface
     */
    private $input;

    /**
     * @var OutputInterface
     */
    private $output;

    protected function configure(): void
    {
        $this
            ->setName(self::NAME)
            ->setDescription('...')
            ->addOption(self::OPTION_SOURCE_VIDEO, null, InputOption::VALUE_REQUIRED, 'File path to source video wich will be converted')
        ;
    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     */
    protected function initialize(InputInterface $input, OutputInterface $output): void
    {
        parent::initialize($input, $output);

        $this->input  = $input;
        $this->output = $output;

        Assert::that($input->getOption(self::OPTION_SOURCE_VIDEO))->file();
    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     */
    protected function execute(InputInterface $input, OutputInterface $output): void
    {
        $this->doConvert();
    }

    final protected function doConvert(): void
    {
        $logger     = $this->getContainer()->get('logger');
        $sshSession = $this->getContainer()->get('video_converter.slave0.ssh_session');
        $exec       = $sshSession->getExec();
//        $screenName = self::REMOTE_GLOBAL_PREFIX . '-trailer';
//        $screened = false;

//        try {
//            $cmd = sprintf('screen -ls | grep %s', $screenName);
//            $screened = (bool) $exec->run($cmd);
//        } catch (\RuntimeException $e) {
//            $logger->addDebug(sprintf('Result of `%s`: screen session %s not found', $cmd, escapeshellarg($screenName)));
//        }

//        if (!$screened) {
//            $cmd = sprintf('screen -S %s -dm pwd', escapeshellarg($screenName));
//            var_dump($exec->run($cmd));
//        }

        $process = 'ffmpeg -i /tmp/trailer.mp4 -vcodec libx264 -vpre libx264-medium_firstpass -s hd720 -acodec aac /tmp/trailer.720p.mp4';
        $cmd     = sprintf('screen -dmS video-converter-trailer %s', $process);
        dump($exec->run($cmd));
    }
}
