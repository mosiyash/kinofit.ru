<?php

namespace CronBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Finder\Finder;

class CronClearParsedKinopoiskDataCommand extends ContainerAwareCommand
{
    const NAME = 'cron:clear-parsed-kinopoisk-data';

    protected function configure()
    {
        $this
            ->setName(self::NAME)
            ->setDescription('Remove old JSON files contains parsed info from KinoPoisk')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $logger = $this->getContainer()->get('logger');

        $iterator = Finder::create()
            ->in($this->getContainer()->getParameter('kinopoisk.json_dir'))
            ->name('*.json')
            ->files();

        foreach ($iterator as $file) {
            if ($file->getMTime() < time() - 60*60*24*30) {
                if (!unlink($file->getPathname())) {
                    $logger->addError(sprintf('Couldn\'t remove %s', $file->getPathname()));
                } else {
                    $logger->addInfo(sprintf('File %s is removed', $file->getPathname()));
                }
            }
        }
    }
}
