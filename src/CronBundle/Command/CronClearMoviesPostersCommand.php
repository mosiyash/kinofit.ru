<?php

namespace CronBundle\Command;

use AppBundle\ContainerAwareTrait;
use KinofitBundle\Entity\File;
use KinofitBundle\Repository\FileRepository;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class CronClearMoviesPostersCommand extends ContainerAwareCommand
{
    const NAME = 'cron:clear-movies-posters';

    use ContainerAwareTrait;

    protected function configure()
    {
        $this
            ->setName(self::NAME)
            ->setDescription('Clear excess movies posters')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $logger = $this->getContainer()->get('logger');
        $em = $this->getContainer()->get('doctrine.orm.entity_manager');
        $moviesPostersFilesystem = $this->getMoviesPostersFilesystem();

        /** @var FileRepository $fileRepostory */
        $fileRepostory = $em->getRepository(File::class);

        foreach ($moviesPostersFilesystem->listContents('', true) as $fileData) {
            $logger->addDebug(sprintf('Process file: %s', json_encode($fileData, JSON_UNESCAPED_UNICODE)));

            if ($fileData['size'] === 3379) {
                $logger->addDebug(sprintf('File %s is detected as dummy KinoPoisk thumbnail', $fileData['path']));
                if (!$moviesPostersFilesystem->delete($fileData['path'])) {
                    $logger->addError(sprintf('Could\'t delete file %s', $fileData['path']));
                } else {
                    $logger->addInfo(sprintf('File %s is deleted', $fileData['path']));
                }

                continue;
            }

            $fileEntity = $fileRepostory->findByFilesystemAttributes(
                'movies_posters',
                $fileData['basename'],
                empty($fileData['dirname']) ? null : $fileData['dirname']
            );

            if ($fileEntity === null) {
                $logger->addDebug(sprintf('Entity %s not found', File::class));
                if (!$moviesPostersFilesystem->delete($fileData['path'])) {
                    $logger->addError(sprintf('Could\'t delete file %s', $fileData['path']));
                } else {
                    $logger->addInfo(sprintf('File %s is deleted', $fileData['path']));
                }
            } else {
                $logger->addDebug(sprintf('Entity %s with id %s exists', File::class, $fileEntity->getId()));
            }
        }
    }

}
