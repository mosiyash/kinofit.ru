<?php

namespace TransmissionBundle\Command;

use KinofitBundle\Entity\TransmissionQueue;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Exception\RuntimeException;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class TransmissionAddToQueueCommand extends ContainerAwareCommand
{
    const NAME               = 'transmission:add-to-queue';
    const OPTION_TORRENT_URL = 'torrent-url';

    protected function configure()
    {
        $this
            ->setName(self::NAME)
            ->setDescription('Add torrent to download queue')
            ->addOption(self::OPTION_TORRENT_URL, null, InputOption::VALUE_REQUIRED, 'URL of the torrent file')
        ;
    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     *
     * @throws RuntimeException
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $logger       = $this->getContainer()->get('logger');
        $filesystem   = $this->getContainer()->get('filesystem');
        $transmission = $this->getContainer()->get('transmission.client');
        $em           = $this->getContainer()->get('doctrine.orm.entity_manager');

        $contents = file_get_contents($input->getOption(self::OPTION_TORRENT_URL));
        if (!$contents) {
            throw new RuntimeException(sprintf('Couldn\'t get contents from file %s', $input->getOption(self::OPTION_TORRENT_URL)));
        }

        $savePath = $this->getContainer()->getParameter('transmission.save_path').'/'.basename($input->getOption(self::OPTION_TORRENT_URL));
        $filesystem->dumpFile($savePath, $contents);

        $torrentInfo = \PHP\BitTorrent\Torrent::createFromTorrentFile($savePath);
        $inQueue     = false;
        $allNames    = call_user_func(function () use ($transmission) {
            $data = [];
            foreach ($transmission->all() as $torrentInfo) {
                $data[] = $torrentInfo->getName();
            }

            return $data;
        });

        foreach ($allNames as $torrentInfoName) {
            if ($torrentInfo->getName() === $torrentInfoName) {
                $inQueue = true;
                $logger->addDebug(sprintf('File %s already in queue', $savePath));
                break;
            }
        }

        if ($inQueue) {
            return;
        }

        $transmissionQueue = new TransmissionQueue();
        $transmissionQueue->setName($torrentInfo->getName());
        $transmissionQueue->setAnnounce($torrentInfo->getAnnounce());
        $transmissionQueue->setAnnounceList($torrentInfo->getAnnounceList());
        $transmissionQueue->setComment($torrentInfo->getComment());
        $transmissionQueue->setCreatedAt($torrentInfo->getCreatedAt());
        $transmissionQueue->setCreatedBy($torrentInfo->getCreatedBy());
        $transmissionQueue->setEncodedHash($torrentInfo->getEncodedHash());
        $transmissionQueue->setExtraMeta($torrentInfo->getExtraMeta());
        $transmissionQueue->setFileList($torrentInfo->getFileList());
        $transmissionQueue->setHash($torrentInfo->getHash());
        $transmissionQueue->setInfo($torrentInfo->getInfo());
        $transmissionQueue->setSize($torrentInfo->getSize());

        $em->persist($transmissionQueue);
        $em->flush($transmissionQueue);

        $torrent = $transmission->add($savePath);
        $transmission->start($torrent, true);
    }
}
