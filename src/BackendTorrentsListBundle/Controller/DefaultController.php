<?php

namespace BackendTorrentsListBundle\Controller;

use KinofitBundle\Entity\TransmissionQueue;
use KinofitBundle\Repository\TransmissionQueueRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{
    /**
     * @Route("/backend/torrents", name="backend_torrents")
     */
    public function indexAction(Request $request)
    {
        $em        = $this->get('doctrine.orm.entity_manager');
        $paginator = $this->get('knp_paginator');

        /** @var TransmissionQueueRepository $transmissionQueueRepository */
        $transmissionQueueRepository   = $em->getRepository(TransmissionQueue::class);
        $transmissionQueueQueryBuilder = $transmissionQueueRepository->getBackendListQueryBuilder();
        $transmissionQueueQuery        = $transmissionQueueQueryBuilder->getQuery();

        $pagination = $paginator->paginate(
            $transmissionQueueQuery,
            $request->query->getInt('page', 1),
            10,
            ['wrap-queries' => true]
        );

        return $this->render('BackendTorrentsListBundle:Default:index.html.twig', [
            'pagination' => $pagination,
        ]);
    }
}
